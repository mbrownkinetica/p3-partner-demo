# Kinetica WMS Viewer

A Vue.js/Browserify SPA that uses the ArcGIS JS API to render WMS's from Kinetica.

## Project Requirements

**NodeJS Version**: 8.4.0

**Kinetica Version**: 6.1+

## Project Information

This project uses Vue.js version 2.x and was built using the Browserify template. This project utilizes the geospatial functions introduced in Kinetica 6.1 and does not provide querying capabilities for any versions prior. See these documentation resources for more information about the tools used in this demo:

#### Vue.js
http://vuejs.org

https://github.com/vuejs/vue

#### Vue CLI
https://github.com/vuejs/vue-cli

#### Vue CLI Browserify Template
https://github.com/vuejs-templates/browserify


## Pre-Setup Instructions

### Install NodeJS

Install NodeJS using brew, or by downloading the installer from the NodeJS website here: https://nodejs.org/en/. This project was built using version 8.4.0, but it may be backward compatible to 6.x. This is untested, so your results may vary.

### Install Dependencies

Navigate to your project directory and run:

``` bash
  npm install
```

## Project Commands

``` bash

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run all tests
npm test
```

## Tips

To test the viewer, you can use the public database using these endpoints:

**WMS Address:** http://demo.kinetica.com/gaiademo/proxy/wms

**Kinetica API Url:** http://demo.kinetica.com/gaiademo/proxy