// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import { sync } from 'vuex-router-sync';
import App from './App';
import router from './router';
import store from './store/store';
import Toaster from 'v-toaster';
import config from '@/config/config';
import 'v-toaster/dist/v-toaster.css';
import VueLocalStorage from 'vue-localstorage';
import VTooltip from 'v-tooltip';
import VueTabs from 'vue-nav-tabs';
import 'vue-nav-tabs/themes/vue-tabs.css';

import KinResizableDiv from '@/directives/KinResizableDiv';

// Vue Toaster
Vue.use(Toaster, {timeout: config.defaultToasterTimeout});

// Vue Local Storage
Vue.use(VueLocalStorage, { name: 'ls' });

// Vue Tooltips
Vue.use(VTooltip);

// Vue Tabs
Vue.use(VueTabs);

// Gauge Charts
Vue.use(require('vue-chartist'));

// Custom Directives
Vue.directive('kin-resizable-div', KinResizableDiv);

// Vuex Router Sync
Vue.config.productionTip = false;
sync(store, router);

/* eslint-disable no-new */
new Vue({
  store,
  el: '#app',
  router,
  data: {store},
  template: '<App/>',
  components: { App }
});
