// Modules
import * as lodash from 'lodash';

// Components
import KinWmsListEditor from '@/components/editors/KinWmsListEditor';

// Helpers
import lsHelper from '@/helpers/localStorageHelper';

////////////////////////////////
// Properties
////////////////////////////////

let props = ['show'];

////////////////////////////////
// Created
////////////////////////////////

/**
 * Fires when the component is created
 */
function mounted () {
  // Sync local settings with the store
  this.settings = lodash.cloneDeep(this.$store.getters.viewerSettings);
}

//////////////////////////////
// Data
//////////////////////////////

/**
 * Returns static data used by the component. Fires on creation.
 */
function data () {
  return {
    settings: {},
    modalTitle: 'WMS Settings'
  };
}

////////////////////////////////
// Methods
////////////////////////////////

/**
 * Handles the update of the wms address
 * @param {Event} e - The update event
 */
function updateSettings (e) {
  let self = this;
  // // Infer WMS url from api url
  lodash.set(self, 'settings.wmsUrl', self.settings.apiUrl + '/wms');
  // Commit the settings to the store. Must clone local settings
  // to avoid creating a direct pointer in the view
  this.$store.commit('UPDATE_VIEWER_SETTINGS', lodash.cloneDeep(self.settings));
  self.$emit('input', self.settings);
}

/**
 * Flushes all settings from both the view and local storage
 */
function flushSettings () {
  for (var key in this.settings) {
    this.settings[key] = null;
    lsHelper.remove(key);
  }
}

////////////////////////////////
// Component
////////////////////////////////

export default {
  components: {
    KinWmsListEditor
  },
  mounted,
  data,
  props,
  methods: {
    updateSettings,
    flushSettings
  }
};
