import * as lodash from 'lodash';
import moment from 'moment';

import kinHelper from '@/helpers/kineticaHelper';

let axios = require('axios');

let debugging = false;

//////////////////////////////
// Public Methods
//////////////////////////////

// #region

/**
 * Queries and loads all available countries from the database
 * @param {String} url - The kinetica base url including the port number
 * @returns {Promise} - The promise of the countries list
 */
async function getCountries(url) {
  let postOptions = {
    'table_name': 'country',
    'limit': -9999,
    'offset': 0,
    'encoding': 'json',
    'options': {'sort_order': 'descending', 'sort_by': 'Country_String'}
  };
  let results;

  if (debugging) {
    console.warn('Returning dev data for countries...');
    return require('@/data/devonly-countries.json');
  }

  try {
    results = await axios.post(`${url}/get/records`, postOptions);
  } catch (err) { console.error(err); return; }

  let parsed = kinHelper.parseDataStr(results.data, true);
  parsed = kinHelper.parseJsonRecords(parsed);

  // Remove bad items
  let blacklist = ['UNKNOWN'];
  let retVal = lodash.filter(parsed, item => !lodash.includes(blacklist, item.Country_String));
  return retVal;
}

/**
 * Returns a list of brands and their IDs
 * @param {String} url - The kinetica base url including the port number
 * @returns {Array<Object>} - An array of objects with text/value parameters.
 */
async function getBrands(url) {
  let postOptions = {
    'table_name': 'brand',
    'limit': -9999,
    'offset': 0,
    'encoding': 'json',
    'options': {'sort_order': 'descending', 'sort_by': 'Brand_String'}
  };
  let results;
  if (debugging) {
    console.warn('Returning dev data for brands...');
    return require('@/data/devonly-brands.json');
  } else {
    try {
      results = await axios.post(`${url}/get/records`, postOptions);
    } catch (err) { console.error(err); return; }
  }
  let parsed = kinHelper.parseDataStr(results.data, true);
  parsed = kinHelper.parseJsonRecords(parsed);
  return parsed;
}

/**
 * Loads the sample number from the database
 * @param {String} url - The kinetica base url including the port number
 * @returns {Promise} - The promise of the sample number loaded
 */
async function getSampleNumber(url, filters) {
  let expressions = [];
  let postOptions = {
    'table_name': 'sampledata3', // TODO: Remove hard-coded table name
    'column_names': ['LocationInfo_Country', 'count(*)'], // TODO: Remove hard-coded column names
    'limit': 50,
    'offset': 0,
    'encoding': 'json',
    'options': {
      'sort_order': 'ascending'
    }
  };

  if (filters.country) {
    expressions.push(`LocationInfo_Country = '${filters.country}'`);
  }
  postOptions.options.expression = expressions.join(' && ');

  let results;
  if (debugging) {
    console.warn('Returning dev data for sample number...');
    let debugVal = 1234567;
    return debugVal.toLocaleString();
  } else {
    try {
      results = await axios.post(`${url}/aggregate/groupby`, postOptions);
    } catch (err) { console.error(err); return; }
  }
  let parsed = kinHelper.parseDataStr(results.data);
  let i = parsed.column_headers.indexOf('count(*)');
  let sum = lodash.sum(parsed[`column_${i + 1}`]);
  return sum.toLocaleString();
}

/**
 * Loads table statistics
 * @returns {Promise} - The promise of the stats being loaded
 */
async function getTableStats(url, tableName) {
  // Aggregate style
  let options = {
    'table_name': tableName,
    'column_name': 'Count',
    'stats': 'count,sum',
    'options': {}
  };
  let results;
  try {
    results = await axios.post(`${url}/aggregate/statistics`, options);
    results = kinHelper.parseDataStr(results.data, true);
  } catch (err) { console.error(err); return; }
  return lodash.get(results, 'stats', {});
}

async function getViewStats(url, tableName, bounds) {
  // Group by style
  let options = {
    'table_name': tableName,
    'column_names': ['SUM(Count) as Sum', 'COUNT(Count) as TotalCount'],
    'offset': 0,
    'limit': -9999,
    'encoding': 'json',
    'options': {'expression': `Longitude >= ${bounds[0]} AND Longitude <= ${bounds[2]} AND Latitude >= ${bounds[1]} AND Latitude <= ${bounds[3]}`}
  };
  let results;
  try {
    results = await axios.post(`${url}/aggregate/groupby`, options);
  } catch (err) { console.error(err); return; }
  let dataStr = kinHelper.parseDataStr(results.data, true);
  let parsed = kinHelper.parseJsonEncodedResponseWithSchema(dataStr);
  return lodash.get(parsed, '[0]', {});
}

/**
 * Retrieves the static bar graph data from Kinetica and transforms it
 * @param {String} url - The url of Kinetica, including the port number
 * @param {Object} brandsLookup - A lookup for brands
 */
async function getBarData(url, brandsLookup) {
  console.debug(`Retrieving bar data results from ${url}...`);

  let results;
  if (debugging) {
    console.warn('Using dev data for bar chart...');
    results = require('@/data/devonly-barChartData.json');
  } else {
    let params = {
      'table_name': 'barchart',
      'offset': 0,
      'limit': -9999,
      'encoding': 'json'
    };
    let options = {
      'sort_by': 'CountryId'
    };
    results = await kinHelper.get(url, 'records', params, options);
    results = kinHelper.parseJsonRecords(results);
  }

  let transformed = _transformBarData(results, brandsLookup);
  return transformed;
}

/**
 * Gets the time series data from the results table
 * @param {String} url - The kinetica base url including the port number
 * @param {Object} filters - The filters to use in the proc
 * @param {Object} brandsLookup - The brands mapping
 * @returns {Promise<Array>} - The promise of the array of data
 */
async function getLineData(url, filters, brandsLookup, timeBinLookup) {
  let startTime = new Date();

  // Delete temporary
  console.group('getLineData');

  if (filters.brand) {
    delete filters.brand;
  }

  // Fire procs and wait till finished
  let proc1Result = await kinHelper.execProc(url, 'P3Score', [], [], filters, 50);
  if (proc1Result.status === 'error') {
    console.warn('Stopped getting line data early due to error in UDF...');
    return;
  }

  let params = {
    'table_name': 'p3score',
    'offset': 0,
    'limit': -9999,
    'encoding': 'json'
  };
  let results = await kinHelper.get(url, 'records', params, {}, {});
  let parsed = kinHelper.parseJsonRecords(results);

  // Start debug -- Loads data locally instead of remotely
  // let results = require('@/data/response-by-column.json');
  // results = kinHelper.parseDataStr(results, true);
  // let parsed = kinHelper.parseJsonEncodedResponseWithSchema(results);
  // End debug

  let lineData = _transformLineData(parsed, brandsLookup, timeBinLookup);
  let endTime = new Date();
  console.debug(`Operation took ${(endTime - startTime) / 1000} seconds`);
  console.groupEnd();
  return lineData;
}

/**
 * Retrieves the time bin table results
 * @param {String} url - The Kinetica base url including the port
 * @returns {Promise} - The promise of the time bin table results
 */
async function getTimeBins(url) {
  let result;
  if (debugging) {
    console.warn('Returning dev data for time bins...');
    return require('@/data/devonly-timebins.json');
  } else {
    try {
      let options = {
        'table_name': 'timebin',
        'offset': 0,
        'limit': -9999,
        'encoding': 'json',
        'options': {}
      };
      result = await axios.post(`${url}/get/records`, options);
    } catch (err) { console.error(err); return; }
  }
  let retVal = kinHelper.parseDataStr(result.data, true);
  retVal = kinHelper.parseJsonRecords(retVal);
  return retVal;
}

async function hasTable(url, tableName) {
  let tableResult = await axios.post(`${url}/has/table`, {'table_name': tableName, 'options': {'no_error_if_not_exists': 'true'}});
  let hasTable = kinHelper.parseDataStr(tableResult.data, true);
  if (lodash.get(hasTable, 'table_exists', false)) {
    return true;
  }
  return false;
}

async function filterWms(url, sourceTable, viewName, expression) {
  let options = {
    'table_name': sourceTable,
    'view_name': viewName,
    'expression': expression,
    'options': {'collection_name': 'wmsFilteredViews', 'ttl': '60'}
  };
  return await axios.post(`${url}/filter`, options);
}

// #endregion Public Methods

//////////////////////////////
// Private Functions
//////////////////////////////

// #region

/**
 * Transforms the bar data into something the chart can use
 * @param {Array<Object>} data - The data array to transform
 * @returns {Array<Object>} - A transformed array of objects
 */
function _transformBarData(data, brandsLookup) {
  let retVal = {};
  let countryGroups = lodash.groupBy(data, i => i.CountryId);
  lodash.forEach(countryGroups, (group, key) => {
    let brandGroup = lodash.groupBy(group, i => i.Brand);
    lodash.set(retVal, key, brandGroup);
  });
  return retVal;
}
/**
 * Takes an array of objects and produces an average
 * based on the passed property.
 * @param {Array<Object>} arr - An array of objects
 * @param {String} prop - The property by which to average
 * @returns {Number} - The average
 */
function _avgBy(arr, prop) {
  let result = 0;

  for (var i = 0; i < arr.length; i++) {
    result += arr[i][prop];
  }

  return result / arr.length;
}

/**
 * Transforms the get result into a format that the chart can display
 * @param {Array} data - An array of results from the db
 * @param {Object} brandsLookup - A lookup object of all brands
 * @returns {Array} - An array of transformed data
 */
function _transformLineData(data, brandsLookup, timeBinLookup) {
  let results = [];
  // let resultMap = {};
  let brandGroups = lodash.groupBy(data, 'Brand');
  lodash.forEach(brandGroups, (brandGroup) => {
    let brandId = brandGroup[0].Brand;
    let hasBrand = brandsLookup[brandId];

    if (!hasBrand) { return; }

    let agg = brandGroup.reduce(function(res, value) {
      if (!res[value.TimeBinAgg]) {
        res[value.TimeBinAgg] = {
          name: brandsLookup[brandId],
          date: moment(timeBinLookup[value.TimeBinAgg]),
          score: 0
        };
      }
      res[value.TimeBinAgg].score += value.ScoreValue;
      return res;
    }, {});

    lodash.forEach(agg, (row) => {
      results.push(row);
    });
  });

  let retVal = [];
  let groups = lodash.groupBy(results, 'name');
  lodash.forEach(groups, (group) => {
    retVal.push({
      name: group[0].name,
      values: lodash.sortBy(group, 'date')
    });
  });
  return retVal;
}

// #endregion Private Functions

//////////////////////////////
// Module Exports
//////////////////////////////

// #region

export default {
  hasTable,
  filterWms,
  getBarData,
  getBrands,
  getCountries,
  getLineData,
  getSampleNumber,
  getTableStats,
  getViewStats,
  getTimeBins
};

// #endregion Module Exports
