import {mapGetters} from 'vuex';
import * as lodash from 'lodash';
import PulseLoader from 'vue-spinner/src/PulseLoader.vue';
import * as wellknown from 'wellknown';
import * as moment from 'moment';
import * as uuid from 'uuid/v1';

import mapboxgl from 'mapbox-gl';

import KinGaugeDisplay from '@/components/displays/KinGaugeDisplay';
import dataAccess from './P3SignalDashboardScreen.dataAccess';
import kinHelper from '@/helpers/kineticaHelper';
import KinWebMap from '@/components/displays/KinWebMap';
import KinSegmentedBarChart from '@/components/displays/KinSegmentedBarChart';
import mbHelper from '@/helpers/mapboxHelper';
import chartHelper from '@/helpers/chartHelper';

let debugging = false;

//////////////////////////////
// Computed
//////////////////////////////

// #region

let computed = {
  barColors() {
    let retVal = [];
    if (this.categoriesModel.coverageQuality4G) { retVal.push('#98abc5'); }
    if (this.categoriesModel.dataCoverage) { retVal.push('#8a89a6'); }
    if (this.categoriesModel.dataQuality) { retVal.push('#7b6888'); }
    if (this.categoriesModel.throughputAvg) { retVal.push('#6b486b'); }
    if (this.categoriesModel.throughputMax) { retVal.push('#a05d56'); }
    if (this.categoriesModel.voiceCoverage) { retVal.push('#d0743c'); }
    if (this.categoriesModel.voiceQuality) { retVal.push('#ff8c00'); }
    return retVal;
  },
  countryChosen() { return this.filters.country !== ''; },
  apiUrl() { return this.viewerSettings.apiUrl; },
  ...mapGetters(['webmap', 'viewerSettings']),
  filtersUDF() {
    let retVal = {
      xmin: this.filters.xmin.toString(),
      xmax: this.filters.xmax.toString(),
      ymin: this.filters.ymin.toString(),
      ymax: this.filters.ymax.toString()
    };
    if (this.filters.country) {
      retVal.country = this.filters.country.toString();
    }
    if (this.filters.brand) {
      retVal.brand = this.filters.brand.toString();
    } else if (this.filters.category) {
      retVal.category = this.filters.category.toString();
    }
    return retVal;
  },
  chartFilters() {
    return {
      name: this.idsToBrands[this.filters.brand],
      category: this.categoriesToIds[this.filters.category]
    };
  },
  filtersApplied() {
    let retVal = false;
    for (var prop in this.filters) {
      if (this.filters[prop]) {
        retVal = true;
      }
    }
    return retVal;
  },
  categories() {
    let self = this;
    let categories = [];
    if (self.categoriesModel.coverageQuality4G) { categories.push('4G Quality of Coverage'); }
    if (self.categoriesModel.dataCoverage) { categories.push('Data Area of Coverage'); }
    if (self.categoriesModel.dataQuality) { categories.push('Data Quality of Coverage'); }
    if (self.categoriesModel.throughputAvg) { categories.push('Throughput Avg'); }
    if (self.categoriesModel.throughputMax) { categories.push('Throughput Max'); }
    if (self.categoriesModel.voiceCoverage) { categories.push('Voice Area of Coverage'); }
    if (self.categoriesModel.voiceQuality) { categories.push('Voice Quality of Coverage'); }
    return categories;
  }
};

// #endregion Computed

//////////////////////////////
// Created
//////////////////////////////

// #region

async function created() {
  console.debug('Creating P3 Dashboard Screen component...');
  let self = this;
  let map = await this.webmap;
  self.mapRef = map;
  return;
}

/**
 * Formats the filter value for special cases
 * @param {String} key - The filter key
 * @param {String} value - The filter value
 * @returns {String} - The transfomred value
 */
function getFilterValue(key, value) {
  let self = this;
  let retVal = '';
  switch (key) {
    case 'brand':
      retVal = self.idsToBrands[value];
      break;
    case 'country':
      retVal = self.idsToCountries[value];
      break;
    default:
      retVal = value;
  }
  return retVal;
}

/**
 * TODO: Kill this function
 */
function randomNumWithin(start, end) {
  let rand = Math.random();
  let range = end - start;
  return (rand * range);
}

// #endregion Created

//////////////////////////////
// Data
//////////////////////////////

// #region

function data() {
  return {
    categoryToColor: {
      coverageQuality4G: '#98abc5',
      dataCoverage: '#8a89a6',
      dataQuality: '#7b6888',
      throughputAvg: '#6b486b',
      throughputMax: '#a05d56',
      voiceCoverage: '#d0743c',
      voiceQuality: '#ff8c00'
    },
    countriesToBrandsToBarData: {},
    chartData: {
      labels: ['A', 'B'],
      series: [40, 60]
    },
    chartOptions: {
      donut: true,
      donutWidth: 30,
      startAngle: 270,
      total: 200,
      showLabel: false
    },
    dataTableZoom: 14,
    mapboxZoom: -1,
    showSidebar: true,
    source: null,
    // Lookups
    idsToCountries: {},
    countriesToIds: {},
    idsToBrands: {},
    brandsToIds: {},
    categoriesToIds: {},
    idsToTimeBins: {},
    timeBinsToIds: {},
    countryCoords: require('@/data/country-centers.json'),
    idsToRxLevColor: {
      0: 'AAAAAA',
      1: 'd82526',
      2: 'f26c64',
      3: 'ffdd71',
      4: '9fcd99',
      5: '309343'
    },
    // Raw Arrays
    brands: [],
    timeBins: [],
    // Select Lists
    countrySelect: [{text: 'Choose a Country', value: ''}],
    operations: {
      idle: 'Operations complete.',
      execProc: 'Scoring results. Please wait...',
      gettingCountries: 'Retrieving country codes...',
      gettingBrands: 'Retrieving brand codes...',
      gettingTimes: 'Retrieving time codes...',
      gettingTableStats: 'Retriving sample statistics...',
      gettingBarData: 'Retrieving results for bar chart...',
      error: 'An error occurred. Please try again.'
    },
    currentOperation: '',
    barChartData: [],
    barChartLoaded: false,
    countries: [{text: 'All', value: ''}],
    categoriesModel: {
      coverageQuality4G: true,
      dataCoverage: true,
      dataQuality: true,
      throughputAvg: true,
      throughputMax: true,
      voiceCoverage: true,
      voiceQuality: true
    },
    filters: {
      country: '',
      brand: '',
      category: '',
      geometryType: 'Extent',
      signalQuality: '',
      xmin: '',
      xmax: '',
      ymin: '',
      ymax: ''
    },
    layerHandler: () => { console.debug('No layer handler registered'); },
    mapRef: null,
    tableStats: {tableSum: 0, viewSum: 0, tableCount: 0, viewCount: 0},
    samples: 0,
    layerId: 'p3-dashboard-layer',
    sourceId: 'p3-dashboard-source',
    layerParams: {
      format: 'image/png',
      service: 'WMS',
      version: '1.1.1',
      request: 'GetMap',
      srs: 'EPSG:3857',
      // Kinetica rendering
      layers: 'sampledata3',
      DO_POINTS: true,
      DO_SHAPES: true,
      STYLES: 'cb_raster',
      CB_ATTR: 'MaxSignalQualityAllGenerations',
      POINTCOLORS: '',
      POINTSIZES: '3,'.repeat(6),
      USE_POINT_RENDERER: true,
      X_ATTR: 'Longitude',
      Y_ATTR: 'Latitude'
    }
  };
}

function getBrands() {
  return this.brandsArray;
}

function getColors() {
  return self.colors;
}

function getColorLookup() {
  let self = this;
  let retVal = {};
  lodash.forEach(this.colors, (color, i) => {
    retVal[self.idsToBrands[i]] = color;
  });
  return retVal;
}

function getPointColors(count) {
  let colors = [];
  for (var i = 0; i < count; i++) {
    colors.push(randomColor());
  }
  let retVal = colors.join(',');
  return retVal;
}

// #endregion Data

//////////////////////////////
// Destroyed
//////////////////////////////
function randomColor() {
  return '0123456789abcdef'.split('').map(function(v, i, a) {
    return i > 5 ? null : a[Math.floor(Math.random() * 16)];
  }).join('');
}

// #region

async function destroyed() {
  console.debug('Destroying the P3 Dashboard Screen component...');
  let self = this;

  // self.mapRef.removeControl(self.mbDraw);

  self.mapRef.off('moveend', self.renderMap);
  self.mapRef.off('zoomend', self.renderMap);
  mbHelper.removeLayer(self.mapRef, this.layerId);
  mbHelper.removeSource(self.mapRef, this.sourceId);

  return;
}

// #endregion Destroyed

//////////////////////////////
// Methods
//////////////////////////////

// #region

function getBrandsLookup(brands) {
  let retVal = {};
  lodash.forEach(brands, (brand) => {
    if (brand.Brand_String) {
      retVal[brand.Brand_ID] = brand.Brand_String;
    }
  });
  return retVal;
}

function getBrandsSelectionOptions(brands) {
  let retVal = [];
  lodash.forEach(brands, (brand) => {
    retVal.push({text: brand.Brand_String, value: brand.Brand_ID});
  });
  return retVal;
}

/**
 * Used in a view iteration loop to detect whether
 * the current object's key and value is a polygon
 * geometry.
 * @param {String} key - The object key
 * @param {String} value - The object value
 * @returns {Boolean} - Whether the current object key/value is a polygon geometry
 */
function isPolyGeom(key, value) {
  return (key === 'geometryType' && value === 'Polygon') || key !== 'geometry' && key !== 'geometryType';
}

/**
 * Returns whether the object's key and value
 * are to be displayed in a list of filters
 * @param {String} key - The filter key
 * @param {String} value - The filter's value
 * @returns {Boolean} - Whether the key is to be displayed
 */
function isDisplayedFilter(key, value) {
  let blackListed = ['geometry', 'bbox', 'xmin', 'xmax', 'ymin', 'ymax'];
  return value && !lodash.includes(blackListed, key);
}

/**
 * Returns the pretty filter name based on the
 * camel-cased key passed.
 * @param {String} key - The object key
 * @returns {String} - A pretty representation of the passed key
 */
function getFilterName(key) {
  let filterNames = {
    country: 'Country',
    brand: 'Brand',
    category: 'P3 Score Category',
    geometryType: 'Geometry'
  };

  return filterNames[key];
}

/**
 * Removes a specific filter from our list based on the key
 * @param {String} key - The filter key
 */
function removeFilter(key) {
  let self = this;
  if (key === 'geometryType') {
    self.filters[key] = 'Extent';
    let all = this.mbDraw.getAll();
    lodash.forEach(all.features, (feature) => {
      self.mbDraw.delete(feature.id);
    });
  } else if (this.filters[key]) {
    this.filters[key] = '';
  }
  self.compileFilters();
}

async function barChartUpdated(d) {
  let self = this;
  self.compileFilters(d);
  self.updateViews();
}

/**
 * Resets the filters object to a neutral state
 */
function clearFilters() {
  let self = this;
  for (var prop in this.filters) {
    // Reset normal props to blank strings
    if (prop !== 'geometryType') {
      this.filters[prop] = '';
    } else {
      // Special Cases
      if (this.filters[prop] === 'Polygon') {
        // Set back to extent and clear the drawn features
        self.filters[prop] = 'Extent';
        let all = this.mbDraw.getAll();
        lodash.forEach(all.features, (feature) => {
          self.mbDraw.delete(feature.id);
        });
      }
    }
  }
}

function getOpIcon(currentOp) {
  let self = this;
  if (currentOp === self.operations.error) {
    return 'fa-exclamation-triangle';
  } else if (currentOp === self.operations.idle) {
    return 'fa-check';
  } else {
    return 'fa-refresh fa-spin';
  }
}

function getAlertClass(currentOp) {
  let self = this;
  if (currentOp === self.operations.error) {
    return 'alert-warning';
  } else if (currentOp === self.operations.idle) {
    return 'alert-success';
  } else {
    return 'alert-info';
  }
}

/**
 * The handler that updates all contained views in the dashboard
 */
async function updateViews() {
  let self = this;
  await self.updateMapView();
  self.renderMap();
  self.updateViewStats();
  self.updateTableStats();
  self.barChartData = getCountryBarData(self.filters.country, self.idsToCountries, self.countriesToBrandsToBarData);
  return;
}

// ----------------------- //
// #region Async Tasks

//
// TODO: Kill this function
//
async function testProc() {
  console.debug('Running the proc...');
  let self = this;
  self.currentOperation = self.operations.execProc;
  self.barChartLoaded = false;
  try {
    // self.currentOperation = self.operations.gettingBarData;
    // self.barChartData = await dataAccess.getBarData(self.apiUrl, self.brandsLookup);
  } catch (err) {
    console.error(err);
    self.currentOperation = self.operations.error;
    self.barChartLoaded = true;
    return;
  }
  self.barChartLoaded = true;
  self.currentOperation = self.operations.idle;
  return;
}

function prettyStat(value) {
  var suffixes = ['', 'k', 'M', 'B', 'T'];
  var suffixNum = Math.floor(('' + value).length / 3);
  var shortValue = parseFloat((suffixNum !== 0 ? (value / Math.pow(1000, suffixNum)) : value).toPrecision(2));
  return shortValue + suffixes[suffixNum];
}

/**
 * Updates the stats view
 */
async function updateViewStats() {
  let self = this;
  let map = await self.webmap;
  self.currentOperation = self.operations.gettingTableStats;

  let stats = await dataAccess.getViewStats(
    self.apiUrl,
    self.getTableName(self.filters, self.getZoom(map)),
    mbHelper.getCurrentBboxArray(map));

  self.tableStats.viewCount = lodash.get(stats, 'TotalCount', 0);
  self.tableStats.viewSum = lodash.get(stats, 'Sum', 0);
  self.currentOperation = self.operations.idle;
}

function signalUpdated(level) {
  let self = this;
  self.filters.signalQuality = level;
  self.updateViews();
}

function removeSignalFilter() {
  let self = this;
  self.filters.signalQuality = '';
  self.updateViews();
}

async function updateTableStats() {
  let self = this;
  let map = await self.webmap;
  self.currentOperation = self.operations.gettingTableStats;

  let stats = await dataAccess.getTableStats(
    self.apiUrl,
    self.getTableName(self.filters, self.getZoom(map)));
  self.tableStats.tableSum = lodash.get(stats, 'sum', 0);
  self.tableStats.tableCount = lodash.get(stats, 'count', 0);
  self.currentOperation = self.operations.idle;
  return;
}

async function countryUpdated() {
  let self = this;
  self.updateViews();
  let map = await self.webmap;
  self.filters.brand = ''; // Reset brand filter for new country
  self.filters.signalQuality = ''; // Reset signal quality
  let countryName = self.idsToCountries[self.filters.country];
  let coords = lodash.find(self.countryCoords, country => country.countryName === countryName);
  if (coords) {
    map.panTo([coords.longitude, coords.latitude], {duration: 2000});
    map.once('moveend', () => { map.zoomTo(7); });
  }
  return;
}

function removeBrandFilter () {
  let self = this;
  self.filters.brand = '';
  self.compileFilters();
  self.updateViews();
}

/**
 * Retrieves all lookups required for app startup
 * @returns {Promise} - The promise of all lookups being populated
 */
async function getLookups() {
  let self = this;

  self.currentOperation = self.operations.gettingCountries;
  self.countries = await dataAccess.getCountries(self.viewerSettings.apiUrl);
  self.countries = lodash.sortBy(self.countries, 'Country_String');
  self.idsToCountries = kinHelper.buildMap(self.countries, 'Country_ID', 'Country_String');
  self.countriesToIds = kinHelper.buildMap(self.countries, 'Country_String', 'Country_ID');
  self.countrySelect = kinHelper.buildSelectList(self.countries, 'Country_ID', 'Country_String', '-- Select a Country --');

  self.currentOperation = self.operations.gettingBrands;
  self.brands = await dataAccess.getBrands(self.viewerSettings.apiUrl);
  self.idsToBrands = kinHelper.buildMap(self.brands, 'Brand_ID', 'Brand_String');
  self.brandsToIds = kinHelper.buildMap(self.brands, 'Brand_String', 'Brand_ID');
  self.brandSelect = kinHelper.buildSelectList(self.brands, 'Brand_ID', 'Brand_String');

  self.currentOperation = self.operations.gettingTimes;
  self.timeBins = await dataAccess.getTimeBins(self.viewerSettings.apiUrl);
  self.idsToTimeBins = kinHelper.buildMap(self.timeBins, 'TimeBin_ID', 'TimeBin_String');
  self.timeBinsToIds = kinHelper.buildMap(self.timeBins, 'TimeBin_String', 'TimeBin_ID');

  return;
}

/**
 * Add a filtered view to the map by name
 * @param {String} viewName - The name of the filtered view
 */
async function updateMapView(fireRefresh) {
  // TODO: Implement this
  console.debug('Updating map view filter...');
  let self = this;
  let map = await self.webmap;
  self.mapboxZoom = map.getZoom();

  // Set boundary filters
  let bounds = mbHelper.getCurrentBboxArray(map);
  self.filters.xmin = bounds[0];
  self.filters.ymin = bounds[1];
  self.filters.xmax = bounds[2];
  self.filters.ymax = bounds[3];

  // Bind layer name to layer params
  self.layerParams.layers = self.getTableName(self.filters, self.getZoom(map));
  self.layerParams.POINTSIZES = self.getPointSizes(Math.round(map.getZoom()));

  // Check to make sure view exists
  let hasTable = await dataAccess.hasTable(self.apiUrl, self.layerParams.layers);
  // TODO: Generate filtered view
  if (!hasTable && self.layerParams.layers) {
    let sourceTable = getSourceTableName(self.filters.country, self.getZoom(map));
    let expression = [];
    if (self.filters.brand) {
      expression.push(`Brand_ID = '${self.filters.brand}'`);
    }
    if (self.filters.signalQuality) {
      expression.push(`MaxSignalQualityAllGenerations = ${self.filters.signalQuality}`);
    }
    await dataAccess.filterWms(self.apiUrl, sourceTable, self.layerParams.layers, expression.join(' AND '));
  }

  // self.$forceUpdate(); TODO: Need this?

  return;
}

async function toggleDrawer() {
  let self = this;
  let webmap = await self.webmap;
  self.showSidebar = !self.showSidebar;
  setTimeout(() => { webmap.resize(); }, 50);
  self.$store.commit('UPDATE_WEBMAP', webmap);
  return;
}

/**
 * Retrieves the noramlized zoom level from
 * the map based on the available zoom tables
 * @param {Object} map - The MapBox webmap
 * @returns {Number} - The normalized zoom number
 */
function getZoom(map) {
  let zoom = Math.round(map.getZoom());
  // let retVal = Math.round(zoom * 6 / 24) + 13;
  // 5 -14, 6-14, 7-15, 8-15, 9-16, 9.5-16, 10-17, 11-18, 12 onwards – 19. limit max zoom level in mapbox to 14
  let mbZoomToTableZoom = {
    5: 14,
    6: 14,
    7: 15,
    8: 15,
    9: 16,
    10: 17,
    11: 18,
    12: 19,
    13: 19,
    14: 19
  };
  let mappedZoom = lodash.get(mbZoomToTableZoom, zoom, 14);
  console.dir(`Using Zoom: ${mappedZoom}`);
  return mappedZoom;
}

/**
 * Returns the encoded table name based on the given criteria
 * @param {String} countryId - The ID of the selected country
 * @param {Number} zoomLevel - The current zoom level rounded to the nearest whole number
 * @param {String?} brandName - Optional. The brand name to include in the filter
 * @returns {String} - The encoded table name
 */
function getTableName(filters, zoomLevel) {
  if (!filters.country) {
    return '';
  }

  let retVal = '';
  retVal += `A${filters.country}`;
  retVal += `Z${zoomLevel}`;
  if (filters.brand) {
    retVal += `B${filters.brand}`;
  }
  if (filters.signalQuality) {
    retVal += `Q${filters.signalQuality}`;
  }
  console.debug(`Using table: ${retVal}`);
  return retVal;
}

/**
 * Produces a source table name with only the
 * country and zoom level.
 * @param {String} countryId - The country ID
 * @param {Number} zoomLevel - The zoom level
 * @returns {String} - The encoded table name
 */
function getSourceTableName(countryId, zoomLevel) {
  let retVal = '';
  retVal += `A${countryId}`;
  retVal += `Z${zoomLevel}`;
  return retVal;
}

// #endregion

/**
 * Handles a change to a filter and compiles all filters down to
 * a single source of truth object
 * @param {Object} d - The data from the emitted input event
 */
function compileFilters(d) {
  let self = this;
  let brandName = lodash.get(d, 'name', null);
  // let category = lodash.get(d, 'clickedCategory', null);
  // if (category) {
  //   // TODO: Make sure this category selection works
  //   this.filters.category = self.categoriesToIds[category];
  //   this.filters.brand = '';
  // } else
  if (brandName) {
    console.debug(`Setting brand filter to ${brandName} / ${self.brandsToIds[brandName]}`);
    // TODO: Make sure this brand selection actually works.
    this.filters.brand = self.brandsToIds[brandName];
    this.filters.category = '';
  }
}

// ----------------------- //
// #region Handlers

/**
 * Handles a draw.create event in the map. Should fire
 * after a polygon has been created in the map.
 * @param {Event} e - The create event containing the new feature
 */
function handleDrawCreate(e) {
  console.debug('Handling draw create...', e);
  let self = this;
  self.mbDraw.set({
    type: 'FeatureCollection',
    features: e.features
  });

  self.filters.geometryType = 'Polygon';
  if (e.features.length >= 1) {
    self.filters.geometry = wellknown.stringify(e.features[0]);
  }
}

/**
 * Handles a draw.delete event from the map. Should fire
 * after any drawn polygon has been removed from the map.
 * @param {Event} e - The delete event
 */
function handleDrawDelete(e) {
  console.debug('Handling draw delete...', e);
  let self = this;
  self.filters.geometryType = 'Extent';
}

/**
 * Handles a draw.update event in the map. Should fire
 * after any drawn polygon is either moved or coords change.
 * @param {Event} e - The update event
 */
function handleDrawUpdate(e) {
  // TODO: Set the new polygon's coords as a filter.
  let self = this;
  console.debug('Handing draw update...', e);
  let coords = lodash.get(e, 'features[0]', {});
  let wkt = wellknown.stringify(coords);
  self.filters.geometry = wkt;
}

// #endregion Handlers
// #endregion Methods

//////////////////////////////
// Mounted
//////////////////////////////

// #region

async function mounted() {
  console.debug('P3 Dashboard Screen component mounted...');
  let self = this;
  self.initializeDashboard();
  return;
}

/**
 * Initializes the dashboard and a listener for any config chagnes
 */
async function initializeDashboard() {
  let self = this;
  let map = await self.webmap;

  // Add zoom control to map
  map.addControl(new mapboxgl.NavigationControl());

  // TODO: Allow drawing when bounding box drawing is available
  // map.addControl(self.mbDraw, 'top-left');// Only allow one feature to be created at a time
  map.on('draw.create', self.handleDrawCreate.bind(self));
  map.on('draw.update', self.handleDrawUpdate.bind(self));
  map.on('draw.delete', self.handleDrawDelete.bind(self));

  // Set a watcher to update our layer list any time the API url changes
  this.$store.watch((state) => state.viewerSettings.apiUrl, async (newVal, oldVal) => {
    await this.$store.dispatch('initWebmap');
    self.initializeDashboard();
  });

  // Get and cache lookups
  await self.getLookups();

  // Update Table Stats
  await self.updateTableStats();
  await self.updateViewStats();

  // Load bar chart data
  self.countriesToBrandsToBarData = await dataAccess.getBarData(self.apiUrl, self.idsToBrands);
  self.barChartLoaded = true;

  // Set current status back to idle
  self.currentOperation = self.operations.idle;

  self.setupWebmap();

  self.updateMapView();

  return;

  // TODO: Remove this, it's for debugging purposes only
  // self.mbDraw.changeMode('BOUNDS');
}

/**
 * Gets the custom point sizes based on the passed zoom level
 * @param {Number} - The current mapbox zoom level
 * @returns {String} - The pointsizes formatted for the URL
 */
function getPointSizes(zoom) {
  let retVal = '3,';
  if (zoom === 5 || zoom === 6) {
    retVal = '1,';
  }
  console.debug(`Setting point size to: ${retVal}`);
  return retVal.repeat(6);
}

/**
 * Retrieves and transforms the bar data from the lookup
 * based on the passed country ID.
 * @param {Number} countryId - The currently shown country id
 * @param {Object} lookup - The country to brands to data lookup
 * @returns {Array} - An array of bar chart data formatted for consumption
 */
function getCountryBarData(countryId, countryLookup, dataLookup) {
  let countryBrands = lodash.get(dataLookup, countryId, null);
  if (!countryBrands) {
    return [];
  }
  let retVal = [];
  lodash.forEach(countryBrands, (brand, key) => {
    var result = brand.reduce(function(map, obj) {
      map[obj['KPI'].replace(' - ', ' ')] = Math.round(obj['ScoreValue']);
      return map;
    }, {});
    result.name = lodash.get(brand, '[0].Brand', 'ERROR');
    retVal.push(result);
  });
  return retVal;
}

async function setupWebmap() {
  let self = this;
  let webmap = await self.webmap;

  self.layerParams.bbox = mbHelper.getCurrentBbox(webmap);

  // Add source
  let sourceDef = {
    type: 'image',
    url: mbHelper.buildUrl(self.apiUrl + '/wms', self.layerParams),
    coordinates: mbHelper.getCoordsFromBounds(webmap.getBounds())
  };
  self.source = mbHelper.addSourceDeprecated(webmap, 'p3-demo-source', sourceDef);

  // Add Layer
  let layerDef = {
    id: 'p3-demo-layer',
    type: 'raster',
    source: 'p3-demo-source'
  };
  mbHelper.addLayer(webmap, layerDef);

  // Setup re-render function
  let dbounced = lodash.debounce(self.renderMap, 100);

  console.debug(`Binding to ${self.layerParams.layers}...`);

  self.layerParams.POINTSHAPES = 'circle,'.repeat(6);
  self.layerParams.CB_VALS = '0:1,1:2,2:3,3:4,4:5,5:6';
  self.layerParams.POINTCOLORS = encodeURIComponent(lodash.values(self.idsToRxLevColor).join(','));
  self.layerParams.POINTSIZES = self.getPointSizes(Math.round(webmap.getZoom()));

  webmap.on('moveend', dbounced)
    .on('zoomend', dbounced);

  return;
}

function renderMap(map, newFn) {
  let self = this;
  let mappedZoom = self.getZoom(self.mapRef);
  self.layerParams.layers = self.getTableName(self.filters, mappedZoom);
  self.layerParams.POINTSIZES = self.getPointSizes(Math.round(self.mapRef.getZoom()));
  mbHelper.setSourceParams.bind(self, self.mapRef, self.apiUrl + '/wms', 'p3-demo-source', 'p3-demo-layer', self.layerParams)();
  self.updateViewStats();
}

// #endregion Mounted

//////////////////////////////
// Exported Module
//////////////////////////////

// #region

export default {
  components: {
    KinGaugeDisplay,
    PulseLoader,
    KinSegmentedBarChart,
    KinWebMap
  },
  computed,
  created,
  data,
  destroyed,
  methods: {
    signalUpdated,
    barChartUpdated,
    getBrands,
    getColorLookup,
    getColors,
    clearFilters,
    compileFilters,
    countryUpdated,
    getAlertClass,
    getBrandsLookup,
    getBrandsSelectionOptions,
    getLookups,
    getPointSizes,
    getOpIcon,
    getFilterName,
    getFilterValue,
    getSourceTableName,
    getTableName,
    getZoom,
    handleDrawCreate,
    handleDrawDelete,
    handleDrawUpdate,
    initializeDashboard,
    isDisplayedFilter,
    includes: lodash.includes,
    isPolyGeom,
    prettyStat,
    randomNumWithin,
    removeBrandFilter,
    removeSignalFilter,
    renderMap,
    removeFilter,
    setupWebmap,
    testProc,
    toggleDrawer,
    titleCase: chartHelper.titleCase,
    updateMapView,
    updateViewStats,
    updateTableStats,
    updateViews
  },
  mounted
};
// #endregion Exported Module
