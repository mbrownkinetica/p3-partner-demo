import KinTopNav from '@/components/displays/KinTopNav';
import KinWebMap from '@/components/displays/KinWebMap';
import KinResizableDiv from '@/directives/KinResizableDiv';
import config from '@/config/config';

//////////////////////////////
// Created
//////////////////////////////

function created () {
  // initHelper.initViewerSettings();
}

//////////////////////////////
// Computed
//////////////////////////////

let computed = {
  activeSections() { return config.activeSections; },
  hasLastContext () { return this.$store.getters.lastContext !== null; },
  sidebarLeftOpen () { return this.$store.getters.sidebarLeftOpen; },
  sidebarRightOpen () { return this.$store.getters.sidebarRightOpen; }
};

//////////////////////////////
// Data
//////////////////////////////

function data() {
  return {
    appTitle: config.appTitle,
    settings: {},
    showSettingsModal: false
  };
}

//////////////////////////////
// Methods
//////////////////////////////

/**
 * Toggles whether the sidebar is open
 * @param {string} direction - Either 'right' or 'left'
 */
function toggleSidebarOpen (direction) {
  if (direction === 'left') {
    this.$store.commit('TOGGLE_SIDEBAR_LEFT_OPEN');
  } else if (direction === 'right') {
    this.$store.commit('TOGGLE_SIDEBAR_RIGHT_OPEN');
  }
}

/**
 * Opens a sidebar
 * @param {String} direction - Either left or right
 * @param {String} tool - The currently active tool
 */
function openSidebar (direction, tool) {
  if (direction === 'left') {
    this.$store.commit('UPDATE_SIDEBAR_LEFT_OPEN', true);
  } else if (direction === 'right') {
    this.activeTool = tool;
    this.$store.commit('UPDATE_SIDEBAR_RIGHT_OPEN', true);
  }
}

/**
 * Toggles the settings modal on/off
 */
function toggleSettingsModal () {
  this.showSettingsModal = !this.showSettingsModal;
}

//////////////////////////////
// Mounted
//////////////////////////////

function mounted() {
  let self = this;
  self.settings = self.$store.getters.viewerSettings;
}

//////////////////////////////
// Exported Module
//////////////////////////////

export default {
  components: {
    KinTopNav,
    KinResizableDiv,
    KinWebMap
  },
  computed,
  created,
  data,
  methods: {
    openSidebar,
    toggleSettingsModal,
    toggleSidebarOpen
  },
  mounted
};