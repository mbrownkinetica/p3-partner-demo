import KinFilterEditor from '@/components/editors/KinFilterEditor';
import EventBus from '@/helpers/eventBus';

const axios = require('axios');
const lodash = require('lodash');

//////////////////////////////
// Data
//////////////////////////////

function data () {
  return {
    tableName: '',
    viewName: '',
    filterValue: '',
    filterExpression: '',
    ttl: '2',
    fields: []
  };
}

//////////////////////////////
// Methods
//////////////////////////////

/**
 * Sends a request to Kinetica to generate a view
 */
function generateView () {
  let url = this.$store.getters.viewerSettings.apiUrl + '/filter';
  let options = {
    table_name: this.tableName,
    view_name: this.viewName,
    expression: this.filterExpression,
    options: {
      ttl: this.ttl.toString()
    }
  };

  // Send the request
  axios.post(url, options)
    .then((response) => {
      Object.assign(this.$data, this.$options.data());
      EventBus.$emit('KinFilterScreen-clearFilters');
    });
}

/**
 * Fetches the columns related to the selected table name
 */
function getColumns () {
  console.debug('Getting columns...');
  let self = this;
  let apiUrl = this.$store.getters.viewerSettings.apiUrl;
  if (!apiUrl) {
    return;
  }
  let url = apiUrl + '/show/table';
  let options = {
    table_name: this.tableName,
    options: {}
  };

  axios.post(url, options)
    .then((response) => {
      try {
        let data = window.JSON.parse(response.data.data_str);
        self.fields = window.JSON.parse(data.type_schemas[0]).fields;
      } catch (e) {
        console.error(e);
      }
    });
}

function updateFilter (filt) {
  this.filterExpression = filt;
}

/**
 * Ensures all reuqired fields are present and returns a boolean indicating
 * the form's validity.
 * @returns {Boolean} - Whether the form is valid
 */
function validateForm () {
  let requiredFields = [
    this.tableName,
    this.filterExpression,
    this.viewName,
    this.ttl
  ];

  let missingFields = lodash.every(requiredFields, (field) => {
    return field;
  });

  if (missingFields) {
    return false;
  }

  return true;
}

//////////////////////////////
// Component
//////////////////////////////

export default {
  components: {
    KinFilterEditor
  },
  data,
  methods: {
    generateView,
    getColumns,
    updateFilter,
    validateForm
  }
};
