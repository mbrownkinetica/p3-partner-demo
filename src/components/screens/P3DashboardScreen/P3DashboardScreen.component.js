import {mapGetters} from 'vuex';
import * as lodash from 'lodash';
import PulseLoader from 'vue-spinner/src/PulseLoader.vue';
import * as wellknown from 'wellknown';
import * as moment from 'moment';
import * as uuid from 'uuid/v1';

// import * as tilebelt from '@mapbox/tilebelt';

import * as MapBoxDraw from '@mapbox/mapbox-gl-draw/dist/mapbox-gl-draw';
// import * as DrawBoundsMode from './DrawBoundsMode.js';
let DrawBoundsMode = require('./DrawBoundsMode');
import DrawBoundsControl from './DrawBoundsControl';

import dataAccess from './P3DashboardScreen.dataAccess';
import KinWebMap from '@/components/displays/KinWebMap';
import KinLineChart from '@/components/displays/KinLineChart';
import KinSegmentedBarChart from '@/components/displays/KinSegmentedBarChart';
import mbHelper from '@/helpers/mapboxHelper';
import chartHelper from '@/helpers/chartHelper';

let axios = require('axios');

//////////////////////////////
// Computed
//////////////////////////////

// #region

let computed = {
  apiUrl() { return this.viewerSettings.apiUrl; },
  ...mapGetters(['webmap', 'viewerSettings']),
  filtersUDF() {
    let retVal = {
      xmin: this.filters.xmin.toString(),
      xmax: this.filters.xmax.toString(),
      ymin: this.filters.ymin.toString(),
      ymax: this.filters.ymax.toString()
    };
    if (this.filters.country) {
      retVal.country = this.filters.country.toString();
    }
    if (this.filters.brand) {
      retVal.brand = this.filters.brand.toString();
    } else if (this.filters.category) {
      retVal.category = this.filters.category.toString();
    }
    return retVal;
  },
  chartFilters() {
    return {
      name: this.brandsLookup[this.filters.brand],
      category: this.metricLookup[this.filters.category]
    };
  },
  filtersApplied() {
    let retVal = false;
    for (var prop in this.filters) {
      if (this.filters[prop]) {
        retVal = true;
      }
    }
    return retVal;
  },
  categories() {
    let self = this;
    let categories = [];
    if (self.categoriesModel.coverage) { categories.push('coverage'); }
    if (self.categoriesModel.speed) { categories.push('speed'); }
    return categories;
  }
};

// #endregion Computed

//////////////////////////////
// Created
//////////////////////////////

// #region

async function created() {
  console.debug('Creating P3 Dashboard Screen component...');
  let self = this;
  let map = await this.webmap;
  self.mapRef = map;
}

/**
 * Formats the filter value for special cases
 * @param {String} key - The filter key
 * @param {String} value - The filter value
 * @returns {String} - The transfomred value
 */
function getFilterValue(key, value) {
  let self = this;
  let retVal = '';
  switch (key) {
    case 'brand':
      retVal = lodash.get(lodash.find(self.brands, brand => brand.value === value), 'text', 'Error');
      break;
    case 'country':
      retVal = lodash.get(lodash.find(self.countries, country => country.value === value), 'text', 'Error');
      break;
    default:
      retVal = value;
  }
  return retVal;
}

/**
 * TODO: Kill this function
 */
function randomNumWithin(start, end) {
  let rand = Math.random();
  let range = end - start;
  return (rand * range);
}

// #endregion Created

//////////////////////////////
// Data
//////////////////////////////

// #region

function data() {
  var modes = MapBoxDraw.modes;
  modes.BOUNDS = DrawBoundsMode;

  return {
    source: null,
    brands: [],
    brandsArray: [],
    brandsLookup: {},
    dimensionType: 'brand',
    operations: {
      idle: 'Operations complete.',
      execProc: 'Scoring results. Please wait...',
      gettingCountries: 'Retrieving country codes...',
      gettingBrands: 'Retrieving brand codes...',
      gettingTimes: 'Retrieving time codes...',
      gettingTableStats: 'Retriving sample statistics...',
      gettingBarData: 'Retrieving results for bar chart...',
      error: 'An error occurred. Please try again.'
    },
    currentOperation: '',
    metricLookup: {
      30: 'coverage',
      20: 'speed'
    },
    categoryIds: {
      coverage: 30,
      speed: 20
    },
    tile: '',
    barChartData: [],
    chartsLoaded: false,
    barChartLoaded: false,
    countries: [{text: 'All', value: ''}],
    categoriesModel: {coverage: true, speed: false},
    filters: {
      country: '',
      brand: '',
      category: '',
      geometryType: 'Extent',
      xmin: '',
      xmax: '',
      ymin: '',
      ymax: ''
    },
    layerHandler: () => { console.debug('No layer handler registered'); },
    layerId: 'p3-dashboard-layer',
    lineChartData: [],
    lineChartLoaded: false,
    mapRef: null,
    mbDraw: new MapBoxDraw({
      modes: modes
    }),
    samples: 0,
    scores: [],
    selectedCountry: '',
    selectedData: {
      score: {name: '', category: ''}
    },
    sourceTable: 'sampledata3',
    sourceId: 'p3-dashboard-source',
    layerParams: {
      format: 'image/png',
      service: 'WMS',
      version: '1.1.1',
      request: 'GetMap',
      srs: 'EPSG:3857',
      layers: 'sampledata3',
      // Kinetica rendering
      DO_POINTS: true,
      DO_SHAPES: true,
      STYLES: 'cb_raster',
      CB_ATTR: 'Brand',
      POINTCOLORS: '',
      POINTSIZES: '1',
      USE_POINT_RENDERER: true,
      X_ATTR: 'Longitude',
      Y_ATTR: 'Latitude'
    }
  };
}

function getBrands() {
  return this.brandsArray;
}

function getColors() {
  return self.colors;
}

function getColorLookup() {
  let self = this;
  let retVal = {};
  lodash.forEach(this.colors, (color, i) => {
    retVal[self.brandsLookup[i]] = color;
  });
  return retVal;
}

function getPointColors(count) {
  let colors = [];
  for (var i = 0; i < count; i++) {
    colors.push(randomColor());
  }
  let retVal = colors.join(',');
  return retVal;
}

// #endregion Data

//////////////////////////////
// Destroyed
//////////////////////////////
function randomColor() {
  return '0123456789abcdef'.split('').map(function(v, i, a) {
    return i > 5 ? null : a[Math.floor(Math.random() * 16)];
  }).join('');
}

// #region

async function destroyed() {
  console.debug('Destroying the P3 Dashboard Screen component...');
  let self = this;

  // self.mapRef.removeControl(self.mbDraw);

  self.mapRef.off('moveend', self.layerHandler);
  self.mapRef.off('zoomend', self.layerHandler);
  mbHelper.removeLayer(self.mapRef, this.layerId);
  mbHelper.removeSource(self.mapRef, this.sourceId);
}

// #endregion Destroyed

//////////////////////////////
// Methods
//////////////////////////////

// #region

function getBrandsLookup(brands) {
  let retVal = {};
  lodash.forEach(brands, (brand) => {
    if (brand.Brand_String) {
      retVal[brand.Brand_ID] = brand.Brand_String;
    }
  });
  return retVal;
}

function getBrandsSelectionOptions(brands) {
  let retVal = [];
  lodash.forEach(brands, (brand) => {
    retVal.push({text: brand.Brand_String, value: brand.Brand_ID});
  });
  return retVal;
}

/**
 * Used in a view iteration loop to detect whether
 * the current object's key and value is a polygon
 * geometry.
 * @param {String} key - The object key
 * @param {String} value - The object value
 * @returns {Boolean} - Whether the current object key/value is a polygon geometry
 */
function isPolyGeom(key, value) {
  return (key === 'geometryType' && value === 'Polygon') || key !== 'geometry' && key !== 'geometryType';
}

/**
 * Returns whether the object's key and value
 * are to be displayed in a list of filters
 * @param {String} key - The filter key
 * @param {String} value - The filter's value
 * @returns {Boolean} - Whether the key is to be displayed
 */
function isDisplayedFilter(key, value) {
  let blackListed = ['geometry', 'bbox', 'xmin', 'xmax', 'ymin', 'ymax'];
  return value && !lodash.includes(blackListed, key);
}

/**
 * Returns the pretty filter name based on the
 * camel-cased key passed.
 * @param {String} key - The object key
 * @returns {String} - A pretty representation of the passed key
 */
function getFilterName(key) {
  let filterNames = {
    country: 'Country',
    brand: 'Brand',
    category: 'P3 Score Category',
    geometryType: 'Geometry'
  };

  return filterNames[key];
}

/**
 * Removes a specific filter from our list based on the key
 * @param {String} key - The filter key
 */
function removeFilter(key) {
  let self = this;
  if (key === 'geometryType') {
    self.filters[key] = 'Extent';
    let all = this.mbDraw.getAll();
    lodash.forEach(all.features, (feature) => {
      self.mbDraw.delete(feature.id);
    });
  } else if (this.filters[key]) {
    this.filters[key] = '';
  }
  self.compileFilters();
}

/**
 * Resets the filters object to a neutral state
 */
function clearFilters() {
  let self = this;
  for (var prop in this.filters) {
    // Reset normal props to blank strings
    if (prop !== 'geometryType') {
      this.filters[prop] = '';
    } else {
      // Special Cases
      if (this.filters[prop] === 'Polygon') {
        // Set back to extent and clear the drawn features
        self.filters[prop] = 'Extent';
        let all = this.mbDraw.getAll();
        lodash.forEach(all.features, (feature) => {
          self.mbDraw.delete(feature.id);
        });
      }
    }
  }
}

function getOpIcon(currentOp) {
  let self = this;
  if (currentOp === self.operations.error) {
    return 'fa-exclamation-triangle';
  } else if (currentOp === self.operations.idle) {
    return 'fa-check';
  } else {
    return 'fa-refresh fa-spin';
  }
}

function getAlertClass(currentOp) {
  let self = this;
  if (currentOp === self.operations.error) {
    return 'alert-warning';
  } else if (currentOp === self.operations.idle) {
    return 'alert-success';
  } else {
    return 'alert-info';
  }
}

/**
 * The handler that updates all contained views in the dashboard
 */
function updateViews() {
  let self = this;
  self.updateMapView();
  // self.updateStats();
  // self.updateCharts();
}

// ----------------------- //
// #region Async Tasks

//
// TODO: Kill this function
//
async function testProc() {
  let self = this;
  self.chartsLoaded = true;
  self.currentOperation = self.operations.execProc;
  self.lineChartLoaded = false;
  self.barChartLoaded = false;
  try {
    self.lineChartData = await dataAccess.getLineData(self.apiUrl, self.filtersUDF, self.brandsLookup, self.timeBinLookup);
    // self.barChartData = self.derriveBarData(self.lineChartData);
    self.currentOperation = self.operations.gettingBarData;
    self.barChartData = await dataAccess.getBarData(self.apiUrl, self.brandsLookup);
  } catch (err) {
    console.error(err);
    self.currentOperation = self.operations.error;
    self.lineChartLoaded = true;
    return;
  }
  self.lineChartLoaded = true;
  self.barChartLoaded = true;
  self.currentOperation = self.operations.idle;
  // dataAccess.clearTempTables(self.apiUrl);
}

// function derriveBarData(lineData) {
//   let self = this;
//   let retVal = [];
//   for (var i = 0; i < lineData.length; i++) {
//     retVal.push({
//       name: lineData[i].name,
//       coverage: self.avgBy(lineData[i].values, 'score')
//     });
//   }
//   debugger;
//   return retVal;
// }

// function avgBy(array, property) {
//   let result = 0;
//   let count = 0;
//   for (var i = 0; i < array.length; i++) {
//     count += 1;
//     result += array[i][property];
//   }
//   return result / count;
// }

/**
 * The main function that updates the bar and line chart data.
 */
async function updateCharts() {
  let self = this;
  // self.getLineData();
}

/**
 * Updates the stats view
 */
async function updateStats() {
  let self = this;
  self.currentOperation = self.operations.gettingTableStats;
  self.samples = await dataAccess.getSampleNumber(self.viewerSettings.apiUrl, ''); // TODO: Plug in expression?
}

/**
 * Retrieves all lookups required for app startup
 * @returns {Promise} - The promise of all lookups being populated
 */
async function getLookups() {
  let self = this;
  self.currentOperation = self.operations.gettingCountries;
  self.countries = await dataAccess.getCountries(self.viewerSettings.apiUrl);
  self.currentOperation = self.operations.gettingBrands;
  let brands = await dataAccess.getBrands(self.viewerSettings.apiUrl);
  self.brandsArray = brands;
  self.brands = self.getBrandsSelectionOptions(brands);
  self.brandsLookup = self.getBrandsLookup(brands);
  self.currentOperation = self.operations.gettingTimes;
  self.timeBinLookup = await self.getTimeBinLookup(self.viewerSettings.apiUrl);
}

/**
 * Retrieves the time bin table and generates a lookup from its ids and strings.
 * @param {String} url - The Kinetica base url including the port number
 * @returns {Object} - A lookup object of time bins
 */
async function getTimeBinLookup(url) {
  let times = await dataAccess.getTimeBinTable(url);
  let lookup = {};
  lodash.forEach(times, (time) => {
    lookup[time.Timebin_ID] = time.Timebin_String;
  });
  return lookup;
}

/**
 * Add a filtered view to the map by name
 * @param {String} viewName - The name of the filtered view
 */
async function updateMapView(viewName) {
  // TODO: Implement this
  console.debug('Updating map view filter...');
  let self = this;
  let map = await self.webmap;
  let bounds = mbHelper.getCurrentBboxArray(map);
  self.filters.xmin = bounds[0];
  self.filters.ymin = bounds[1];
  self.filters.xmax = bounds[2];
  self.filters.ymax = bounds[3];
  self.$forceUpdate();
}

// #endregion

/**
 * Handles a change to a filter and compiles all filters down to
 * a single source of truth object
 * @param {Object} d - The data from the emitted input event
 */
async function compileFilters(d) {
  let self = this;
  let webmap = await self.webmap;
  let name = lodash.get(d, 'name', null);
  let category = lodash.get(d, 'clickedCategory', null);
  console.dir({category, name});
  if (category) {
    // TODO: Make sure this category selection works
    this.filters.category = self.categoryIds[category];
    this.filters.brand = '';
  } else if (name) {
    // TODO: Make sure this brand selection actually works.
    this.filters.brand = lodash.get(lodash.find(self.brands, brand => brand.text === name), 'value', '');
    this.filters.category = '';

    let filteredView = 'sampledata3-' + name;
    let hasTable = await dataAccess.hasTable(self.apiUrl, filteredView);
    if (!hasTable) {
      await dataAccess.filterWms(self.apiUrl, filteredView, `Brand = '${name}'`);
    }
    self.layerParams.layers = filteredView;
  } else {
    self.layerParams.layers = 'sampledata3';
  }
  mbHelper.setSourceParams.bind(this, webmap, self.apiUrl + '/wms', 'p3-demo-source', 'p3-demo-layer', self.layerParams)();
  this.filters.country = lodash.get(this, 'filters.country', '');
  this.updateViews();
}

// ----------------------- //
// #region Handlers

/**
 * Handles a draw.create event in the map. Should fire
 * after a polygon has been created in the map.
 * @param {Event} e - The create event containing the new feature
 */
function handleDrawCreate(e) {
  console.debug('Handling draw create...', e);
  let self = this;
  self.mbDraw.set({
    type: 'FeatureCollection',
    features: e.features
  });

  self.filters.geometryType = 'Polygon';
  if (e.features.length >= 1) {
    self.filters.geometry = wellknown.stringify(e.features[0]);
  }
}

/**
 * Handles a draw.delete event from the map. Should fire
 * after any drawn polygon has been removed from the map.
 * @param {Event} e - The delete event
 */
function handleDrawDelete(e) {
  console.debug('Handling draw delete...', e);
  let self = this;
  self.filters.geometryType = 'Extent';
}

/**
 * Handles a draw.update event in the map. Should fire
 * after any drawn polygon is either moved or coords change.
 * @param {Event} e - The update event
 */
function handleDrawUpdate(e) {
  // TODO: Set the new polygon's coords as a filter.
  let self = this;
  console.debug('Handing draw update...', e);
  let coords = lodash.get(e, 'features[0]', {});
  let wkt = wellknown.stringify(coords);
  self.filters.geometry = wkt;
}

// #endregion Handlers
// #endregion Methods

//////////////////////////////
// Mounted
//////////////////////////////

// #region

async function mounted() {
  console.debug('P3 Dashboard Screen component mounted...');
  let self = this;
  let map = await self.webmap;

  // Set pan/zoom handlers on the map
  self.layerHandler = self.updateViews.bind(self);
  map.on('moveend', self.layerHandler);
  map.on('zoomend', self.layerHandler);

  // TODO: Allow drawing when bounding box drawing is available
  // map.addControl(self.mbDraw, 'top-left');// Only allow one feature to be created at a time
  map.on('draw.create', self.handleDrawCreate.bind(self));
  map.on('draw.update', self.handleDrawUpdate.bind(self));
  map.on('draw.delete', self.handleDrawDelete.bind(self));

  // Get and cache lookups
  await self.getLookups();

  // Update Table Stats
  await self.updateStats();

  // Set current status back to idle
  self.currentOperation = self.operations.idle;

  self.colors = getPointColors(lodash.values(self.brandsLookup).length);
  self.colorMap = lodash.values(self.brandsLookup).map((brand, i) => {
    if (brand) {
      return {brand: brand, color: self.colors.split(',')[i]};
    }
  });
  self.layerParams.POINTCOLORS = self.colors;
  self.layerParams.CB_VALS = encodeURIComponent(lodash.values(self.brandsLookup).join(','));
  self.layerParams.POINTSHAPES = 'circle,'.repeat(lodash.values(self.brandsLookup).length);

  self.setupWebmap();

  self.updateMapView();

  // TODO: Remove this, it's for debugging purposes only
  // self.mbDraw.changeMode('BOUNDS');
}

async function setupWebmap() {
  let self = this;
  let webmap = await self.webmap;

  self.layerParams.bbox = mbHelper.getCurrentBbox(webmap);

  // Add source
  let sourceDef = {
    type: 'image',
    url: mbHelper.buildUrl(self.apiUrl + '/wms', self.layerParams),
    coordinates: mbHelper.getCoordsFromBounds(webmap.getBounds())
  };
  self.source = mbHelper.addSourceDeprecated(webmap, 'p3-demo-source', sourceDef);

  // Add Layer
  let layerDef = {
    id: 'p3-demo-layer',
    type: 'raster',
    source: 'p3-demo-source'
  };
  mbHelper.addLayer(webmap, layerDef);

  // Setup re-render function
  let renderDb = lodash.debounce(
    mbHelper.setSourceParams.bind(this, webmap, self.apiUrl + '/wms', 'p3-demo-source', 'p3-demo-layer', self.layerParams),
    100);
  renderDb();

  webmap.on('moveend', renderDb)
    .on('zoomend', renderDb);
}

// #endregion Mounted

//////////////////////////////
// Exported Module
//////////////////////////////

// #region

export default {
  components: {
    KinLineChart,
    PulseLoader,
    KinSegmentedBarChart,
    KinWebMap
  },
  computed,
  created,
  data,
  destroyed,
  methods: {
    getBrands,
    getColorLookup,
    getColors,
    clearFilters,
    compileFilters,
    getAlertClass,
    getTimeBinLookup,
    getBrandsLookup,
    getBrandsSelectionOptions,
    getLookups,
    getOpIcon,
    getFilterName,
    getFilterValue,
    handleDrawCreate,
    handleDrawDelete,
    handleDrawUpdate,
    isDisplayedFilter,
    includes: lodash.includes,
    isPolyGeom,
    randomNumWithin,
    removeFilter,
    setupWebmap,
    testProc,
    titleCase: chartHelper.titleCase,
    updateCharts,
    updateMapView,
    updateStats,
    updateViews
  },
  mounted
};
// #endregion Exported Module
