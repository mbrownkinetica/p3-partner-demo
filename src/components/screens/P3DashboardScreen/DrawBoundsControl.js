// Control implemented as ES6 class
export default class DrawBoundsControl {
  onAdd(map) {
    this._map = map;
    this._container = document.createElement('div');
    this._container.className = '';
    this._container.innerHTML = '<div class="mapboxgl-ctrl-group mapboxgl-ctrl"><button @click="drawBounds" class="mapbox-gl-draw_ctrl-draw-btn mapbox-gl-draw_trash" title="Delete"></button></div>';
    return this._container;
  }

  onRemove() {
    this._container.parentNode.removeChild(this._container);
    this._map = undefined;
  }
}
