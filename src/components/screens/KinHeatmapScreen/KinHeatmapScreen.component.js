import * as lodash from 'lodash';
import * as uuid from 'uuid/v1';
import { mapGetters } from 'vuex';

import config from '@/config/config';
import kineticaHelper from '@/helpers/kineticaHelper';
import KinSortableLayerListEditor from '@/components/editors/KinSortableLayerListEditor';
import mbHelper from '@/helpers/mapboxHelper';
import EventBus from '@/helpers/eventBus';

//////////////////////////////
// Computed
//////////////////////////////

let computed = {
  ...mapGetters([
    'webmap',
    'tables',
    'viewerSettings'
  ]),
  orderedLayers () { return lodash.orderBy(this.rasterLayers, 'order'); }
};

//////////////////////////////
// Data
//////////////////////////////

function data () {
  return {
    layerCounter: 1,
    sourceIdBase: 'heatmap-source-id',
    layerIdBase: 'heatmap-layer-id',
    rasterLayers: [],
    layerFixture: {
      id: '',
      optionsOpen: false,
      order: -1,
      sourceId: 'heatmap-source-id-',
      layerId: 'heatmap-layer-id-',
      selectionType: 'select',
      latLonWkt: 'latlon',
      params: {
        // Standard WMS Parameters
        format: 'image/png',
        service: 'WMS',
        version: '1.1.1',
        request: 'GetMap',
        srs: 'EPSG:3857',
        layers: self.heatmapLayer, // TODO: Make this variable
        // Kinetica Rendering Parameters
        STYLES: 'heatmap',
        BLUR_RADIUS: 5,
        X_ATTR: config.defaultXAttr,
        Y_ATTR: config.defaultYAttr
      }
    }
  };
}

//////////////////////////////
// Created
//////////////////////////////

function created () {
  console.debug('Creating heatmap view...');
  let self = this;

  // Update the layer list with the current API url
  self.updateLayerList();

  EventBus.$on('KinStatsScreen-UpdatedFilter', this.updateFilter);

  // Set a watcher to update our layer list any time the API url changes
  this.$store.watch((state) => {
    return state.viewerSettings.apiUrl;
  },
    self.updateLayerList.bind(self));
}

//////////////////////////////
// Destroyed
//////////////////////////////

async function destroyed () {
  console.debug('Destroying heatmap view...');
  let self = this;
  let map = await self.webmap;

  // Destroy all layers and sources
  lodash.forEach(this.rasterLayers, (layer, index) => {
    map.off('moveend', layer.handler);
    map.off('zoomend', layer.handler);
    mbHelper.removeLayer(map, layer.layerId);
    mbHelper.removeSource(map, layer.sourceId);
  });

  // Destroy our local layer registry
  this.rasterLayers = [];
}

//////////////////////////////
// Methods
//////////////////////////////

/**
 * Retreives available table names and then
 * populates the table names in the store.
 */
function updateLayerList () {
  let self = this;
  let apiUrl = self.viewerSettings.apiUrl;
  if (!apiUrl) {
    return;
  }

  // Get layer list
  kineticaHelper.getTables(apiUrl).then((tableNames) => {
    self.$store.commit('UPDATE_TABLES', tableNames);
  });
}

/**
 * Handles a refresh and reloads the map
 */
function handleRefresh () {
  let self = this;
  self.reloadMap.bind(self)();
}

/**
 * Reloads the webmap and resets the source params for each layer
 */
function reloadMap () {
  let self = this;

  // Reload the source params for each layer
  lodash.forEach(self.rasterLayers, (layer, index) => {
    layer.handler();
  });
}

/**
 * Creates a new layer and injects it into the rasterLayers variable
 */
async function addLayer () {
  console.debug('Adding heatmap layer...');
  let self = this;
  let map = await self.webmap;

  let newLayer = lodash.cloneDeep(self.layerFixture);
  var coords = mbHelper.getCoordsFromBounds(map.getBounds());
  let layerId = uuid();

  // Create the layer and add it to our local registry
  newLayer.name = `Layer ${self.layerCounter}`;
  newLayer.id = layerId;
  newLayer.layerId += layerId;
  newLayer.sourceId += layerId;
  newLayer.order = self.rasterLayers.length; // Place it at the end
  self.layerCounter += 1;

  // Set drag/zoom handlers
  // Note: You have to debounce the source loading
  // due to the zoomend handler firing repeatedly on scroll wheel zooms
  let efficientFn = lodash.debounce(
    mbHelper.setSourceParams.bind(this, map, self.viewerSettings.wmsUrl, newLayer.sourceId, newLayer.layerId, newLayer.params),
  100);
  newLayer.handler = efficientFn;

  // Add the layer to our local array
  self.rasterLayers.push(newLayer);

  mbHelper.addSource(map, newLayer.sourceId, {
    type: 'image',
    url: mbHelper.buildUrl(self.viewerSettings.wmsUrl, newLayer.params),
    coordinates: coords
  });

  // Adds on top of all basemap map layers
  let layerArr = map.getStyle().layers;
  let lastLayerId = layerArr.length - 1;
  mbHelper.addLayer(map, layerId, {
    id: newLayer.layerId,
    type: 'raster',
    source: newLayer.sourceId
  }, lastLayerId);

  // Set drag/zoom handlers
  map.on('moveend', newLayer.handler)
    .on('zoomend', newLayer.handler);
}

/**
 * Reorders the layer
 * @param {Object} layer - The layer to move
 * @param {String} direction - Either up/down, the direction to move. Up is closer to 0.
 */
function reorderLayer (layer, direction) {
  console.debug(`Reordering layer ${window.JSON.stringify(layer)} ${direction}`);
  let self = this;
  let map = self.webmap;
  let nextIndex = (direction === 'up') ? layer.order - 1 : layer.order + 1;

  if (nextIndex < 0 || nextIndex >= self.rasterLayers.length) {
    return;
  }

  let swapLayer = lodash.find(self.rasterLayers, (rasterLayer) => {
    return rasterLayer.order === nextIndex;
  });

  // Move the layer in the map
  let layerMove = map.moveLayer.bind(map, layer.layerId, swapLayer.layerId);

  // Don't pass a swap layer if we're at the end of the list
  if (nextIndex === self.rasterLayers.length - 1) {
    layerMove = map.moveLayer.bind(map, layer.layerId);
  }

  // Move the layer
  layerMove();

  // Update our local layer registery
  let temp = lodash.clone(swapLayer.order);
  swapLayer.order = layer.order;
  layer.order = temp;

  // Repaint the layers
  self.reloadMap.bind(self)();
}

/**
 * Removes a layer from the rasterLayers array
 * @param {Object} layer - The layer to be deleted
 */
async function removeLayer (layer) {
  console.debug(`Removing layer ${window.JSON.stringify(layer)}`);
  let self = this;
  let map = await self.webmap;

  // Get the index of the layer in our local registry
  let index = lodash.findIndex(self.rasterLayers, (rasterLayer) => {
    return rasterLayer.id === layer.id;
  });

  // If index doesn't exist, log the error.
  if (index === -1) {
    console.error('Could not find layer to remove with id: ' + layer.id);
    return;
  }

  // Remove the layer from the map
  mbHelper.removeLayer(map, layer.layerId);

  // Finally, remove it from our local registry
  self.rasterLayers.splice(index, 1);
}

/**
 * Opens an option panel for the passed layer
 * @param {Object} layer - The layer for which to open the options panel
 */
function openOptions (layer) {
  layer.optionsOpen = !layer.optionsOpen;
}

//////////////////////////////
// Component
//////////////////////////////

export default {
  components: {
    KinSortableLayerListEditor
  },
  computed,
  created,
  data,
  destroyed,
  methods: {
    addLayer,
    handleRefresh,
    openOptions,
    reloadMap,
    reorderLayer,
    removeLayer,
    updateLayerList
  }
};
