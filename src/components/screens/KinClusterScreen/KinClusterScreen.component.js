import PulseLoader from 'vue-spinner/src/PulseLoader.vue';
import {featureCollection} from '@turf/helpers';

import KinColorPicker from '@/components/editors/KinColorPicker';
import KinClusterLayerEditor from '@/components/editors/KinClusterLayerEditor';
import mbHelper from '@/helpers/mapboxHelper';
import clusterHelper from '@/helpers/clusterHelper';
import config from '@/config/config';

//////////////////////////////
// Computed
//////////////////////////////

let computed = {
  queryUrl () { return this.$store.getters.viewerSettings.apiUrl + '/aggregate/groupby'; }
};

//////////////////////////////
// Created
//////////////////////////////

function created () {
  console.debug('Creating KinClusterScreen componenet...');

  // Init data source after webmap has loaded
  this.$store.getters.webmap.then((webmap) => {
    console.debug('Initializing cluster data source...');
    this.initDataSource(webmap);
  });
}

//////////////////////////////
// Destroyed
//////////////////////////////

function destroyed () {
  let self = this;
  this.$store.getters.webmap.then((map) => {
    console.debug('Destroying cluster screen...');

    mbHelper.removeLayer(map, self.layerId);
    mbHelper.removeLayer(map, self.labelsLayerId);
    mbHelper.removeSource(map, self.sourceId);
  });
}

//////////////////////////////
// Data
//////////////////////////////

function data () {
  return {
    minColor: '#FF0000',
    maxColor: '#00FF00',
    xCol: config.defaultXAttr,
    yCol: config.defaultYAttr,
    clusterPrecision: 8,
    clusterRadius: 40,
    features: [],
    useBbox: false,
    loading: false,
    loadingMessage: 'Loading. Please wait...',
    labelColor: '#242446',
    labelHaloColor: '#FFFFFF',
    labelsLayerId: 'cluster-labels-id',
    layerId: 'cluster-layer-id',
    minSize: 1,
    maxSize: 20,
    sourceId: 'cluster-source-id',
    selectedTable: 'yellow_taxi_2015_bo'
  };
}

//////////////////////////////
// Methods
//////////////////////////////

/**
 * Loads cluster data into the map from the URL based on the zoom scale
 * @param {Object} map - The map object
 * @param {String} url - The Kinetica endpoint url
 * @param {Integer} precision - The clustering precision (should be 1 - 10, where 10 is least clustered)
 */
function loadData (map, url, precision) {
  let self = this;
  self.loading = true;
  return clusterHelper.queryFeatures.bind(this)(url, this.clusterPrecision, this.xCol, this.yCol, this.useBbox)
    .then((data) => {
      let features = mbHelper.dynamicSchemaToGeoJson(data.data_str, self.xCol, self.yCol);
      self.loading = false;
      return features;
    }).catch(() => {
      self.loading = false;
    });
}

/**
 * Initializes the geojson datasource and layer
 * @param {Object} map - The map object
 */
function initDataSource (map) {
  let self = this;

  let cluster = clusterHelper.initCluster(self.features, self.clusterRadius, self.clusterMaxZoom);
  let clusterSourceDef = clusterHelper.getClusterSourceDef(self.sourceId, cluster, map.getZoom());
  let clusterLayerDef = clusterHelper.getClusterLayerDef(self.layerId, self.sourceId);
  let labelsLayerDef = clusterHelper.getLabelsLayerDef(self.labelsLayerId, self.sourceId, self.labelColor, self.labelHaloColor);

  // Add cluster source and layers to map gracefully
  mbHelper.addSource(map, self.sourceId, clusterSourceDef);
  mbHelper.addLayer(map, self.layerId, clusterLayerDef);
  clusterHelper.addLayer(map, self.labelsLayerId, labelsLayerDef, self.labelColor, self.labelHaloColor);

  // Bind the updateClusters function to the zoomend and dragend events
  let updateFunc = self.updateClusters.bind(self, map, self.sourceId, self.layerId, cluster, self.clusterRadius, self.minSize, self.maxSize, self.minColor, self.maxColor);
  map.on('zoomend', updateFunc)
    .on('dragend', updateFunc);
}

/**
 * Updates the cluster layer and themes the points
 * @param {Object} map - The map object
 * @param {Object} cluster - The cluster object
 * @param {Numberpack} zoom - The zoom level
 */
function updateClusters (map, sourceId, layerId, cluster, clusterRadius, minSize, maxSize, minColor, maxColor) {
  let self = this;
  cluster.options.radius = parseInt(clusterRadius);
  let worldBounds = [-180.0000, -90.0000, 180.0000, 90.0000];
  let clusterData = featureCollection(cluster.getClusters(worldBounds, Math.floor(map.getZoom())));
  let minMax = clusterHelper.getMinMax(map, cluster, 'cluster_qty');

  let layer = map.getLayer(layerId);
  if (!layer) {
    return;
  }

  // Update the layer's paint properties based on our current view
  let radiusPaintProps = clusterHelper.getRadiusPaintProperties(minMax, self.minSize, self.maxSize);
  map.setPaintProperty(self.layerId, 'circle-radius', radiusPaintProps);

  let colorPaintProperties = clusterHelper.getColorPaintProperties(minMax, self.minColor, self.maxColor);
  map.setPaintProperty(self.layerId, 'circle-color', colorPaintProperties);

  let source = map.getSource(sourceId);
  if (source) {
    source.setData(clusterData).load();
  }
}

/**
 * Runs a new cluster query and loads the data onto the map
 */
function runCluster () {
  let self = this;
  let map = this.$store.getters.webmap;
  this.loadData(map, this.queryUrl, this.clusterPrecision)
    .then((features) => {
      self.cluster.load(features);
      updateClusters.bind(self)(map, self.cluster, self.clusterRadius, self.worldBounds, map.getZoom());
    });
}

//////////////////////////////////
// Component
//////////////////////////////////

export default {
  components: {
    KinClusterLayerEditor,
    KinColorPicker,
    PulseLoader
  },
  computed,
  created,
  data,
  destroyed,
  methods: {
    initDataSource,
    loadData,
    runCluster,
    updateClusters
  },
  props: []
};
