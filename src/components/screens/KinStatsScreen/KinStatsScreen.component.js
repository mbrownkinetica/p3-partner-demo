import KinVMultiBarChart from '@/components/displays/KinVMultiBarChart_Deprecated';
import KinLineChart from '@/components/displays/KinLineChart';
import KinModalDisplay from '@/components/displays/KinModalDisplay';
import kinHelper from '@/helpers/kineticaHelper';
import mbHelper from '@/helpers/mapboxHelper';
import KinChartOptionsModal from '@/components/modals/KinChartOptionsModal';

import { mapGetters } from 'vuex';
import PulseLoader from 'vue-spinner/src/PulseLoader';
import * as lodash from 'lodash';

const axios = require('axios'); // TODO: Figure out how to import this instead

//////////////////////////////
// Data
//////////////////////////////

function data() {
  return {
    chartData: [
    ],
    chartTitle: '',
    status: '',
    expression: '',
    lastD: null,
    layerId: 'chart-wms-layer',
    layerParams: {
      format: 'image/png',
      service: 'WMS',
      version: '1.1.1',
      request: 'GetMap',
      srs: 'EPSG:3857',
      layers: '',
      STYLES: 'heatmap',
      BLUR_RADIUS: 5,
      DOPOINTS: true,
      DOSHAPES: true,
      DOTRACKS: true,
      POINTSHAPES: 'CIRCLE',
      SHAPELINEWIDTHS: 2,
      SHAPELINECOLORS: '291262',
      SHAPEFILLCOLORS: 'c2b7dc',
      TRACKLINEWIDTHS: 2,
      TRACKLINECOLORS: '00FF00',
      TRACKMARKERSIZES: 2,
      TRACKMARKERCOLORS: '0000FF',
      TRACKMARKERSHAPES: 'CIRCLE',
      TRACKHEADSIZES: 2,
      TRACKHEADCOLORS: 'FF0000',
      TRACKHEADSHAPES: 'DIAMOND',
      X_ATTR: 'LocationLongitude', // TODO: Make this variable
      Y_ATTR: 'LocationLatitude' // TODO: Make this variable
    },
    loading: false,
    showOptionsModal: false,
    sourceId: 'chart-wms-source',
    tableName: 'sample',
    xAttr: 'value',
    yAttr: 'name'
  };
};

//////////////////////////////
// Computed
//////////////////////////////

let computed = {
  ...mapGetters([
    'viewerSettings',
    'webmap'
  ])
};

//////////////////////////////
// Created
//////////////////////////////

async function created() {
  console.debug('Creating stats screen...');
  let self = this;
  let map = await self.webmap;

  // Load the data into the chart
  loadData.bind(self)();

  self.dbSetSourceParams = lodash.debounce(
    mbHelper.setSourceParams.bind(self, map, self.viewerSettings.wmsUrl, self.sourceId, self.layerId, self.layerParams),
  100);

  self.addWmsLayerToMap(self.tableName); // TODO: Remove hard-coding of layer name
}

//////////////////////////////
// Destroyed
//////////////////////////////

async function destroyed() {
  let self = this;
  let map = await self.webmap;

  map.off('moveend', self.dbSetSourceParams);
  map.off('zoomend', self.dbSetSourceParams);
  mbHelper.removeLayer(map, self.layerId);
  mbHelper.removeSource(map, self.sourceId);
}

//////////////////////////////
// Methods
//////////////////////////////

async function addWmsLayerToMap(viewName) {
  console.debug('Adding wms layer to map...');
  let self = this;
  let map = await self.webmap;
  var coords = mbHelper.getCoordsFromBounds(map.getBounds());

  self.layerParams.layers = viewName;

  mbHelper.addSource(map, self.sourceId, {
    type: 'image',
    url: mbHelper.buildUrl(self.viewerSettings.wmsUrl, self.layerParams),
    coordinates: coords
  });

  // Add teh layer to the map
  let layerArr = map.getStyle().layers;
  let lastLayerId = layerArr.length - 1;
  mbHelper.addLayer(map, self.layerId, {
    id: self.layerId,
    type: 'raster',
    source: self.sourceId
  }, lastLayerId);

  // Set drag/zoom handlers
  map.on('moveend', self.dbSetSourceParams)
    .on('zoomend', self.dbSetSourceParams);

    // Call this once manually so the layer appears without initial pan/zoom
  self.dbSetSourceParams();
}

/**
 * Loads the data for the bar chart and
 * assigns it to the chartData var
 * @returns {Promise} - The promise of the data to be loaded
 */
async function loadData() {
  let self = this;
  // TODO: Load the real data
  // let postOptions = {
  //   'table_name': self.tableName, // TODO: Remove hard-coded table name
  //   'column_names': ['OperatorBrand', 'NetworkGeneration', 'count(*)'], // TODO: Remove hard-coded column names
  //   'limit': 50,
  //   'offset': 0,
  //   'encoding': 'json',
  //   'options': {'sort_order': 'descending'}
  // };

  // let results;
  // try {
  //   results = await axios.post(`${self.viewerSettings.apiUrl}/aggregate/groupby`, postOptions);
  // } catch (err) { console.error(err); return; }

  // self.chartData = self.transformData(results);
  self.chartData = [
    {
      name: 'Test 1',
      coverage: 20,
      speed: 30,
      origin: 40
    },
    {
      name: 'Test 2',
      coverage: 10,
      speed: 50,
      origin: 90
    }
  ];
}

/**
 * Fires off a new projection and adds the WMS of its output to the map
 * @param {Object} d - The data sent from the D3 click
 */
async function updateFilter(d) {
  let self = this;
  let map = await self.webmap;

  // Don't re-run query unless you have to
  if (lodash.eq(d, self.lastD)) {
    return;
  }

  // Set reference to last filter
  self.lastD = d;

  // Clear status when bar is not defined
  self.status = '';
  self.where = '';
  self.expression = '';
  self.loading = true;

  // If no filter is set, add the table's wms to the map
  if (!d) {
    self.layerParams.layers = self.tableName;
    mbHelper.setSourceParams(map, self.viewerSettings.wmsUrl, self.sourceId, self.layerId, self.layerParams);
    return;
  }

  self.status = `Filtering data where <code>${self.expression}</code>`;
  let viewName = self.getViewName.bind(self)(d);
  self.expression = `${self.yAttr} = ${d[self.yAttr]}`;

  // See if view exits. If not, create it.
  let viewExists = await self.filteredViewExists(viewName);
  if (!viewExists) {
    try {
      await self.createFilteredView(d);
    } catch (err) { console.error(err); return; }
  }

  self.layerParams.layers = viewName;
  mbHelper.setSourceParams(map, self.viewerSettings.wmsUrl, self.sourceId, self.layerId, self.layerParams);
}

/**
 * Checks to see if a filter exists for the passed filter object
 * @param {Object} d - The filter criteria object
 * @returns {Promise} - The promise of a boolean value as to whether the filter exists
 */
async function filteredViewExists(viewName) {
  let self = this;

  console.debug(`Checking if filter exists for ${viewName}`);
  let options = {
    'table_name': viewName,
    'options': {
      'no_error_if_not_exists': 'true'
    }
  };

  let results;
  try {
    results = await axios.post(`${self.viewerSettings.apiUrl}/show/table`, options);
  } catch (err) { console.error(err); return; }

  let response = kinHelper.parseData(results.data.data_str, true);
  console.debug(`Show table returned ${window.JSON.stringify(results)}`);

  if (response.table_names.length === 0) {
    return false;
  }

  return true;
}

/**
 * Returns the view name for a given filter criteria
 * @param {Object} d - The filter criteria
 * @returns {String} - The view name for the given filter criteria
 */
function getViewName(d) {
  let self = this;
  let viewName = d[self.yAttr].toLowerCase().split(' ').join('_');
  return `webmap_viewer_${viewName}`;
}

/**
 * Creates a projection for the given filter. D cannot be null or undefined.
 * @param {Object} d - An object with one key and one value to build a filter with.
 * @returns {Promise} - The promise of the created projection
 */
async function createFilteredView(d) {
  let self = this;
  let criteria = d[self.yAttr].split(' '); // TODO: Refactor this. It's janky.
  let operatorBrand = criteria[0];
  let networkGeneration = criteria[1];

  let postOptions = {
    'table_name': self.tableName,
    'view_name': self.getViewName.bind(self)(d),
    'expression': `OperatorBrand == '${operatorBrand}' and NetworkGeneration = '${networkGeneration}' `,
    'options': {}
  };

  // Create the filtered view based on the current filter criteria
  return axios.post(`${self.viewerSettings.apiUrl}/filter`, postOptions)
    .catch((err) => { console.error(err); return; });
}

/**
 * Transforms the result of the Kinetica query to something
 * that can be inserted into the bar chart (2-dimensional).
 * @param {Object} result - The result object
 * @returns {Object} - The resultant data structure transformed for bar chart consumption
 */
function transformData(result) {
  let parsed = kinHelper.parseData(result.data.data_str);

  // Transform disperate columns into cohesive json object
  let transformed = lodash.map(parsed.column_1, (d, i) => {
    // Disallow spaces at the end of the name
    let nameArr = [];
    nameArr.push(d);
    nameArr.push(lodash.get(parsed, `column_2[${i}]`, undefined));

    return {
      name: nameArr.filter((d) => { return d; }).join(' '),
      value: lodash.get(parsed, `column_3[${i}]`, 0)
    };
  });

  // Sort by name
  transformed = lodash.sortBy(transformed, (d) => { return d.name; });

  return transformed;
}

/**
 * Opens the chart options modal
 */
function openOptionsModal() {
  console.debug('Opening chart options modal...');
  this.showOptionsModal = true;
}

/**
 * Fires when the x attribute has been
 * updated by the settings editor
 */
function updateXAttr(newVal) {
  this.xAttr = newVal;
}

/**
 * Fires when the y attribute has been
 * udated by the settings editor
 */
function updateYAttr(newVal) {
  this.yAttr = newVal;
}

/**
 * Fires when the table name has been updated
 * @param {String} newVal - The new table name
 */
function updateTableName(newVal) {
  this.tableName = newVal;
}

/**
 * Fires when the blur radius has been updated
 * @param {Number} newVal - The new blur radius value
 */
function updateBlurRadius(newVal) {
  lodash.set(this, 'layerParams.BLUR_RADIUS', newVal);
  this.dbSetSourceParams();
}

/**
 * Fires when the color map has been updated
 * @param {String} newVal - The new color map
 */
function updateColorMap(newVal) {
  lodash.set(this, 'layerParams.COLORMAP', newVal);
  this.dbSetSourceParams();
}

/**
 * Handles a chart title update event
 * @param {String} newVal - The new value
 */
function updateChartTitle(newVal) {
  this.chartTitle = newVal;
}

//////////////////////////////
// Module Exports
//////////////////////////////

export default {
  components: {
    KinChartOptionsModal,
    KinLineChart,
    KinModalDisplay,
    KinVMultiBarChart,
    PulseLoader
  },
  computed,
  created,
  data,
  destroyed,
  methods: {
    addWmsLayerToMap,
    createFilteredView,
    filteredViewExists,
    getViewName,
    loadData,
    openOptionsModal,
    transformData,
    updateFilter,
    updateYAttr,
    updateXAttr,
    updateTableName,
    updateBlurRadius,
    updateColorMap,
    updateChartTitle
  }
};
