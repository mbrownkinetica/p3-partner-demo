import * as d3 from 'd3';
import * as lodash from 'lodash';

//////////////////////////////
// Test Data
//////////////////////////////

let testData = [
  {letter: 'A', frequency: 0.08167},
  {letter: 'B', frequency: 0.01492},
  {letter: 'C', frequency: 0.02780},
  {letter: 'D', frequency: 0.04253},
  {letter: 'E', frequency: 0.12702},
  {letter: 'F', frequency: 0.02288},
  {letter: 'G', frequency: 0.02022},
  {letter: 'H', frequency: 0.06094},
  {letter: 'I', frequency: 0.06973},
  {letter: 'J', frequency: 0.00153},
  {letter: 'K', frequency: 0.00747},
  {letter: 'L', frequency: 0.04025},
  {letter: 'M', frequency: 0.02517},
  {letter: 'N', frequency: 0.06749},
  {letter: 'O', frequency: 0.07507},
  {letter: 'P', frequency: 0.01929},
  {letter: 'Q', frequency: 0.00098},
  {letter: 'R', frequency: 0.05987},
  {letter: 'S', frequency: 0.06333},
  {letter: 'T', frequency: 0.09056},
  {letter: 'U', frequency: 0.02758},
  {letter: 'V', frequency: 0.01037},
  {letter: 'W', frequency: 0.02465},
  {letter: 'X', frequency: 0.00150},
  {letter: 'Y', frequency: 0.01971},
  {letter: 'Z', frequency: 0.00074}
];

//////////////////////////////
// Props
//////////////////////////////

let props = {
  barClickCb: {
    type: Function,
    default: () => { console.debug('No barClickCb specified. Throwing default function.'); }
  },
  chartData: {
    type: Array,
    default: () => {
      return testData;
    }
  },
  height: {
    type: String,
    default: () => { return 500; }
  },
  width: {
    type: String,
    default: () => { return 500; }
  },
  xAttr: {
    type: String,
    default: () => { return 'x'; }
  },
  yAttr: {
    type: String,
    default: () => { return 'y'; }
  }
};

//////////////////////////////
// Computed
//////////////////////////////

let computed = {
  calcWidth() { return this.width - this.margin.left - this.margin.right; },
  calcHeight() { return this.height - this.margin.top - this.margin.bottom; }
};

//////////////////////////////
// Data
//////////////////////////////

function data() {
  return {
    margin: {top: 20, right: 20, bottom: 30, left: 40},
    lastFilter: null
  };
}

//////////////////////////////
// Mounted
//////////////////////////////

function mounted () {
  this.buildChart();
}

//////////////////////////////
// Methods
//////////////////////////////

/**
 * Retrieves the scales for the bar chart based on the height
 * passed in the props
 * @returns {Object} - An object containing the x and y scale functions
 */
function getScales() {
  let self = this;

  var x = d3.scaleBand()
    .range([0, self.calcWidth])
    .padding(0.1);

  var y = d3.scaleLinear()
    .range([self.calcHeight, 0]);

  // Scale the range of the data in the domains
  x.domain(self.chartData.map(function(d) { return d[self.xAttr]; }));
  y.domain([0, d3.max(self.chartData, function(d) { return d[self.yAttr]; })]);

  return {x, y};
}

/**
 * Returns the css name for bars based on the existing attribute value
 * @param {String} attr - The existing x attribute value
 * @returns {String} - The built css name (without the leading period).
 */
function getCssName(attr) {
  return `bar-${attr.toLowerCase()}`;
}

/**
 * Sets the on hover/click handlers for the chart
 * @param {Object} svg - The D3 SVG object
 */
function setHandlers(container) {
  let self = this;

  // Fire callback on bar click, and pass the bars data
  container.selectAll('.bar').on('click', (datum, index, nodeList) => {
    if (lodash.isEqual(self.lastFilter, datum)) {
      return;
    }

    container.selectAll('.bar')
    .classed('highlighted', false)
    .classed('unhighlighted', true);

    // Highlight only selected bar
    d3.select(nodeList[index])
    .classed('highlighted', true)
    .classed('unhighlighted', false);

    // Fire passed callback
    self.lastFilter = datum;
    self.barClickCb(datum);
  });

  // Display tooltip on bar hover
  let divTooltip = container.select('.kin-bar-chart-tooltip');
  container.selectAll('.bar').on('mousemove', (d) => {
    let self = this;
    divTooltip.style('left', d3.event.pageX + 10 + 'px');
    divTooltip.style('top', d3.event.pageY - 25 + 'px');
    divTooltip.style('display', 'inline-block');
    var x = d3.event.pageX;
    var y = d3.event.pageY;
    var elements = document.querySelectorAll(':hover');
    var l = elements.length - 1;
    var elementData = elements[l].__data__;
    divTooltip.html(`<b>${elementData[self.xAttr]}</b><br>${elementData[self.yAttr]}`);
  });

  container.selectAll('.bar').on('mouseout', (d) => {
    divTooltip.style('display', 'none');
  });

  // Click off of filtering mode
  container.selectAll('.bar-bg').on('click', () => {
    if (!self.lastFilter) {
      return;
    }
    container.selectAll('.bar')
      .classed('highlighted', false)
      .classed('unhighlighted', false);
    self.lastFilter = null;
    self.barClickCb(null);
  });
}

/**
 * Calculates the chart based on the data property and
 * builds it on the SVG in the view.
 */
function buildChart() {
  let self = this;
  let scales = self.getScales();
  let container = d3.select(self.$el);
  let svg = container.select('.kin-bar-chart');

  // Add Group for the histogram rects
  svg.select('g')
    .attr('transform', 'translate(' + self.margin.left + ',' + self.margin.top + ')')
    .selectAll('rect')
    .data(self.chartData)
    .attr('class', (d) => { return `bar ${self.getCssName(d[self.xAttr])}`; })
    .attr('x', (d) => { return scales.x(d[self.xAttr]); })
    .attr('width', scales.x.bandwidth())
    .attr('y', (d) => { return scales.y(d[self.yAttr]); })
    .attr('height', (d) => { return self.calcHeight - scales.y(d[self.yAttr]); });

  // add the x Axis
  svg.append('g')
    .attr('transform', `translate(${self.margin.left}, ${self.calcHeight + self.margin.top})`)
    .call(d3.axisBottom(scales.x));

  // add the y Axis
  svg.append('g')
    .attr('transform', `translate(${self.margin.left}, ${self.margin.top})`)
    .call(d3.axisLeft(scales.y));

  // Set hover/click handlers
  self.setHandlers(container);

  // Load up the legend
  var options = d3.keys(self.chartData[0]).filter(function(key) { return key !== 'label'; });
  var legend = svg.selectAll('.legend')
    .data(options.slice())
    .enter().append('g')
    .attr('class', 'legend')
    .attr('transform', (d, i) => { return `translate(0,${i * 20}`; });

  legend.append('rect')
    .attr('class', 'bar')
    .attr('y', 0)
    .attr('x', self.calcWidth - 18)
    .attr('width', 18)
    .attr('height', 18);

  legend.append('text')
    .attr('x', self.calcWidth - 24)
    .attr('y', 9)
    .attr('dy', '.35em')
    .style('text-anchor', 'end')
    .text((d) => { return d; });
}

//////////////////////////////
// Module Exports
//////////////////////////////

export default {
  computed,
  data,
  methods: {
    buildChart,
    getCssName,
    getScales,
    setHandlers
  },
  mounted,
  props
};
