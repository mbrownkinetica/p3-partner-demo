// Components
import config from '@/config/config';
import KinSettingsModal from '@/components/modals/KinSettingsModal';
import KinModalDisplay from '@/components/displays/KinModalDisplay';

//////////////////////////////////
// Hooks
//////////////////////////////////

/**
 * The data hook
 */
function data () {
  return {
    appTitle: config.appTitle,
    identifyCoords: {x: 0, y: 0},
    identifyVisible: false,
    layers: [],
    isMounted: true,
    queryResults: {},
    settings: {},
    showModal: false
  };
}

//////////////////////////////
// Computed
//////////////////////////////

let computed = {
  sidebarOpen: {
    get: function () {
      return this.$store.getters.sidebarOpen;
    },
    set: function (newVal) {
      return this.$store.commit('UPDATE_SIDEBAR_VISIBILITY', newVal);
    }
  },
  webmap: {
    get: function () {
      return this.$store.getters.webmap;
    },
    set: function (newVal) {
      this.$store.commit('UPDATE_WEBMAP', newVal);
    }
  }
};

//////////////////////////////
// Mounted
//////////////////////////////

/**
 * The mounted hook
 */
function mounted () {
  let self = this;
  // Sync settings
  self.settings = self.$store.getters.viewerSettings;

  // Initialize the demo
  self.initWebMap();
}

//////////////////////////////
// Methods
//////////////////////////////

/**
 * Initializes the webmap object
 * @param {String} wmsUrl - The wms url
 */
function initWebMap (wmsUrl) {
  console.debug('Initializing webmap...');
  this.$store.dispatch('initWebmap');
}

/**
 * Refreshes the webmap layers after the wms url ahs been updated
 */
function refreshWebMapLayers (url) {
  console.debug('Refreshing webmaps layers with new url...');
}

//////////////////////////////////
// Component
//////////////////////////////////

export default {
  components: {
    KinModalDisplay,
    KinSettingsModal
  },
  computed,
  data,
  methods: {
    initWebMap,
    refreshWebMapLayers
  },
  mounted,
  props: []
};
