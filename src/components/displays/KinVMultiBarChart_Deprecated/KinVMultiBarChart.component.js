/**
 * Usage Instructios
 *
 * Data
 * ---------------------
 * Data should be an array of objects. A name property must be on the object
 * that dictates its label. A total property must exist on the object that
 * equals the sum of the categories. All other properties can be shown in the
 * bar graph by passing their property names into the categories property on
 * the bar graph directive.
 *
 * Presentation
 * ---------------------
 * Bar colors can be passed via the barColors property on the directive. If
 * more than one is present, it will map the bar colors to the categories
 * respectively. Otherwise, if only one color is passed, that is used for all
 * bar colors (creating a solid colored bar). Text colors can also be set
 * this way for each category respectively on the textColor property passed
 * to the directive.
 *
 * The width of the chart is automagically set based on the parent container's ref name,
 * which can be passed into the directive via the containerRef property on the directive.
 */
import * as d3 from 'd3';
import * as lodash from 'lodash';
import helper from '@/helpers/chartHelper';

//////////////////////////////
// Demo Data
//////////////////////////////

let demoData = [
  {name: 'A', x: 100, y: 200, total: 300},
  {name: 'B', x: 300, y: 400, total: 700},
  {name: 'C', x: 200, y: 100, total: 300}
];

//////////////////////////////
// Props
//////////////////////////////

let props = {
  barClickCb: {
    type: Function,
    default: () => { console.debug('No barClickCb specified. Throwing default function.'); }
  },
  barColors: {
    type: Array,
    default: () => {
      return [
        '#d1cbe2',
        '#b1a5d2',
        '#a187dc'
      ];
    }
  },
  categories: {
    type: Array,
    default: () => { return ['x', 'y']; }
  },
  chartData: {
    type: Array,
    default: () => { return demoData; }
  },
  containerRef: {
    type: String,
    default: () => { return 'widgetBody'; }
  },
  textColors: {
    type: Array,
    default: () => { return ['#FFFFFF']; }
  },
  tickFormat: {
    type: String,
    default: 's'
  },
  tickCount: {
    type: Number,
    default: 5
  },
  yAttr: {
    type: String,
    default: () => { return 'name'; }
  },
  xTitle: {type: String},
  yTitle: {type: String}
};

//////////////////////////////
// Computed
//////////////////////////////

let computed = {
  calcWidth() {
    if (this.calculatedWidth) {
      return this.calculatedWidth;
    }
    return 0;
  },
  calcHeight() {
    let barHeights = this.barHeight * this.chartData.length;
    let barPaddings = this.barPadding * this.chartData.length;
    let totalHeight = barHeights + barPaddings + this.margin.top + this.margin.bottom + this.titlePadding;
    return totalHeight;
  },
  margin() {
    let self = this;
    let leftMargin = 20;
    let margins = {top: 20, right: 20, bottom: 20, left: leftMargin};
    let longestTotal = lodash.maxBy(self.chartData, (d) => { return d['total']; });
    if (longestTotal) {
      margins.right = this.getTextWidth(longestTotal['total'], '8.5pt arial') + 10;
    }
    let longestObj = lodash.maxBy(self.chartData, (d) => {
      if (!d[self.yAttr]) {
        console.warn('Y attribute does not match data object.');
        return;
      }
      return d[self.yAttr].length;
    });
    if (longestObj) {
      leftMargin = this.getTextWidth(longestObj[self.yAttr], '8.5pt arial') + 10;
      margins.left = leftMargin;
    }
    return margins;
  },
  labelOffset() { return 40; },
  paddingLeft() { return (this.yTitle) ? this.margin.left + this.titlePadding : this.margin.left; },
  paddingTop() { return (this.xTitle) ? this.margin.top + this.titlePadding : this.margin.top; }
};

//////////////////////////////
// Data
//////////////////////////////

function data() {
  return {
    barHeight: 10,
    barPadding: 10,
    calculatedWidth: null,
    titlePadding: 40,
    lastFilter: 'first-load'
  };
}

//////////////////////////////
// Mounted
//////////////////////////////

function mounted() {
  let dbResizeHandler = lodash.debounce(() => {
    this.clearAxis();
    this.storeWidth();
    this.updateAxis();
    this.buildChart();
  }, 500);
  window.addEventListener('resize', dbResizeHandler);
  dbResizeHandler();
}

//////////////////////////////
// Updated
//////////////////////////////

function updated () {
  this.updateAxis();
  this.buildChart();
}

//////////////////////////////
// Methods
//////////////////////////////

/**
 * Stores the width of the parent container after
 * the component has finished loading. Required
 * for automagic width sizing of this component
 */
function storeWidth() {
  let rawWidth, rawPaddingLeft, rawPaddingRight;
  try {
    let style = window.getComputedStyle(this.$parent.$refs[this.containerRef]);
    rawWidth = style.getPropertyValue('width');
    rawPaddingLeft = style.paddingLeft;
    rawPaddingRight = style.paddingRight;
  } catch (e) {
    rawWidth = this.$parent.$refs[self.containerRef].currentStyle.width;
    rawPaddingLeft = this.$parent.$refs[self.containerRef].currentStyle.paddingLeft;
    rawPaddingRight = this.$parent.$refs[self.containerRef].currentStyle.paddingRight;
  }
  this.calculatedWidth = helper.removePx(rawWidth) -
    helper.removePx(rawPaddingLeft) -
    helper.removePx(rawPaddingRight);
}

/**
 * Retrieves the scales for the bar chart based on the height
 * passed in the props
 * @returns {Object} - An object containing the x and y scale functions
 */
function getScales() {
  let self = this;

  let xScale = d3.scaleLinear()
    .range([0, self.calcWidth - self.margin.left - self.margin.right]);

  let yScale = d3.scaleBand()
    .range([0, self.calcHeight - self.margin.top - self.margin.bottom])
    .padding(0.2);

  // Scale the range of the data in the domains
  xScale.domain([0, d3.max(self.chartData, function(d) {
    let total = 0;
    lodash.forEach(self.categories, (c) => {
      total += d[c];
    });
    return total;
  })]);
  yScale.domain(self.chartData.map(function(d) { return d['name']; }));

  return {x: xScale, y: yScale};
}

/**
 * Returns the css name for bars based on the existing attribute value
 * @param {String} attr - The existing x attribute value
 * @returns {String} - The built css name (without the leading period).
 */
function getCssName(attr) {
  return `bar-${attr.toLowerCase().split(' ').join('-')}`;
}

/**
 * Sets the on hover/click handlers for the chart
 * @param {Object} svg - The D3 SVG object
 */
function setHandlers(container) {
  let self = this;

  // Fire callback on bar click, and pass the bars data
  container.selectAll('.bar').on('click', (datum, index, nodeList) => {
    if (lodash.isEqual(self.lastFilter, datum)) {
      return;
    }

    container.selectAll('.bar')
    .classed('highlighted', false)
    .classed('unhighlighted', true);

    // Highlight only selected bar
    d3.select(nodeList[index])
    .classed('highlighted', true)
    .classed('unhighlighted', false);

    // Fire passed callback
    self.lastFilter = datum;
    self.barClickCb(datum);
  });

  // Display tooltip on bar hover
  let divTooltip = container.select('.kin-v-bar-chart-tooltip');
  container.selectAll('.bar').on('mousemove', (d) => {
    let self = this;
    divTooltip.style('left', d3.event.pageX + 10 + 'px');
    divTooltip.style('top', d3.event.pageY - 25 + 'px');
    divTooltip.style('display', 'inline-block');
    var x = d3.event.pageX;
    var y = d3.event.pageY;
    var elements = document.querySelectorAll(':hover');
    var l = elements.length - 1;
    var elementData = elements[l].__data__;
    var output = [];
    var total = 0;
    lodash.forEach(self.categories, (c) => {
      output.push(`<b>${helper.titleCase(c)}</b>: ${elementData[c].toLocaleString()}`);
      total += elementData[c];
    });
    output.push(`<b>Total</b>: ${total.toLocaleString()}`);
    divTooltip.html(output.join('<br/>'));
  });

  container.selectAll('.bar').on('mouseout', (d) => {
    divTooltip.style('display', 'none');
  });

  // Click off of filtering mode
  container.selectAll('.bar-bg').on('click', () => {
    if (!self.lastFilter) {
      return;
    }
    container.selectAll('.bar')
      .classed('highlighted', false)
      .classed('unhighlighted', false);
    self.lastFilter = null;
    self.barClickCb(null);
  });
}

/**
 * Uses canvas.measureText to compute and return the width of the given text of given font in pixels.
 * @param {String} text The text to be rendered.
 * @param {String} font The css font descriptor that text is to be rendered with (e.g. 'bold 14px verdana').
 * @see https://stackoverflow.com/questions/118241/calculate-text-width-with-javascript/21015393#21015393
 */
function getTextWidth(text, font) {
  // re-use canvas object for better performance
  var canvas = getTextWidth.canvas || (getTextWidth.canvas = document.createElement('canvas'));
  var context = canvas.getContext('2d');
  context.font = font;
  var metrics = context.measureText(text);
  return metrics.width;
}

/**
 * Clears existing axis from the svg
 */
function clearAxis() {
  let self = this;
  let container = d3.select(self.$el);
  let svg = container.select('.kin-v-bar-chart');
  svg.selectAll('.x-axis').remove();
  svg.selectAll('.y-axis').remove();
  svg.selectAll('.axis-titles').remove();
}

/**
 * Redraws the two axis based on the chart data
 */
function updateAxis() {
  let self = this;

  // Clear the old axis on update
  self.clearAxis();

  // Build new axis
  let container = d3.select(self.$el);
  let svg = container.select('.kin-v-bar-chart');
  let scales = self.getScales();

  // add the x Axis
  svg.append('g')
    .classed('x-axis', true)
    .call(d3.axisTop(scales.x).ticks(self.tickCount, self.tickFormat))
    .attr('transform', `translate(${self.paddingLeft}, ${self.paddingTop})`);

  svg.append('g')
    .classed('y-axis', true)
    .call(d3.axisLeft(scales.y))
    .attr('transform', `translate(${self.paddingLeft}, ${self.paddingTop})`);

  // Add titles to the chart
  let titles = svg.append('g')
    .classed('axis-titles', true);

  if (self.xTitle) {
    titles.append('svg:text')
    .text(self.xTitle)
    .attr('x', () => { return (self.calcWidth / 2) - (self.getTextWidth(self.xTitle, '8.5pt arial') / 2); })
    .attr('y', () => { return self.titlePadding / 2; });
  }

  if (self.yTitle) {
    titles.append('svg:text')
    .text(self.yTitle)
    .attr('x', () => { return self.titlePadding; })
    .attr('y', () => { return (self.calcHeight / 2); })
    .attr('transform', 'rotate(-90,' + (self.titlePadding / 2) + ',' + (self.calcHeight / 2) + ')');
  }
}

/**
 * Calculates the chart based on the data property and
 * builds it on the SVG in the view.
 */
function buildChart() {
  let self = this;
  let scales = self.getScales();
  let container = d3.select(self.$el);
  let svg = container.select('.kin-v-bar-chart');

  // Pad the bar chart
  svg.select('.bars')
  .attr('transform', 'translate(' + (self.paddingLeft) + ',' + (self.paddingTop) + ')');

  // Bind chart data to bar groups
  svg.selectAll('.bar')
    .data(self.chartData);

  // Add histogram rects
  for (var i = 0; i < this.categories.length; i++) {
    let cat = this.categories[i];
    let prevCats = lodash.filter(this.categories, (c, t) => {
      return t < i;
    });

    svg.selectAll('.bar')
      .data(self.chartData);

    svg.selectAll('.' + self.getCssName(cat))
      .data(self.chartData)
      .attr('id', (d, i) => { return `${self.getCssName(cat + '-' + i)}`; })
      .attr('x', (d) => {
        let total = 0;
        lodash.forEach(prevCats, (c) => {
          total += scales.x(d[c]);
        });
        return total;
      })
      .attr('fill', (d, ind) => {
        if (this.barColors.length > 1) {
          return this.barColors[i];
        }
        return this.barColors[0];
      })
      .attr('y', (d, i) => { return scales.y(d['name']); })
      .attr('height', (d, i) => {
        return scales.y.bandwidth();
      })
      .attr('width', function(d, i) {
        if (!d[cat]) {
          console.warn('X attribute does not match data object.');
          return 0;
        }
        return scales.x(d[cat]);
      });

    // Add titles to each bar
    svg.selectAll('.bar-title-' + i)
      .data(self.chartData)
      .text(() => {
        return helper.titleCase(cat);
      })
      .style('visibility', (d) => {
        // Hide bar title if the bar is too thin to hold it
        let textWidth = self.getTextWidth(cat);
        let barLength = scales.x(d[cat]) - 10;
        if (textWidth > barLength) {
          return 'hidden';
        }
        return 'visible';
      })
      .attr('id', (d, i) => { return `${self.getCssName('title-' + cat + '-' + i)}`; })
      .attr('x', (d) => {
        let total = 0;
        lodash.forEach(prevCats, (c) => {
          total += scales.x(d[c]);
        });
        return total + 5;
      })
      .attr('y', (d, i) => {
        return scales.y(d['name']) +
          self.barHeight +
          8;
      });
  }

  // Add bar values
  svg.selectAll('.bars .bar .bar-value')
    .data(self.chartData)
    .text((d) => {
      return d['total'].toLocaleString();
    })
    .attr('style', 'font-size: 11px;')
    .attr('x', (d) => {
      return self.calcWidth;
    })
    .attr('y', (d, i) => {
      if (!d[self.yAttr]) {
        console.warn('Y attribute does not match data object.');
        return 0;
      }
      return scales.y(d[self.yAttr]) + // Bar Spacing
        (scales.y.bandwidth() / 3) * 2 + // Center within bar
        5; // Arbirary number for line-height
    })
    .attr('transform', `translate(-${self.margin.left}, 0)`);

  // Set hover/click handlers
  self.setHandlers(container);
}

//////////////////////////////
// Module Exports
//////////////////////////////

export default {
  computed,
  data,
  methods: {
    buildChart,
    clearAxis,
    getTextWidth,
    getCssName,
    getScales,
    setHandlers,
    storeWidth,
    updateAxis
  },
  updated,
  mounted,
  props,
  watch: {
    xTitle: function(newVal, oldVal) { // watch it
      this.updateAxis();
      this.buildChart();
    }
  }
};
