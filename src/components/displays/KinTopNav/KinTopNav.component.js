import {mapGetters} from 'vuex';

import config from '@/config/config';
import KinModalDisplay from '@/components/displays/KinModalDisplay';
import KinSettingsModal from '@/components/modals/KinSettingsModal';
import initHelper from '@/helpers/initHelper';

//////////////////////////////
// Computed
//////////////////////////////

let computed = {
  ...mapGetters['viewerSettings']
};

//////////////////////////////
// Created
//////////////////////////////

function created() {
  initHelper.initViewerSettings();
}

//////////////////////////////
// Data
//////////////////////////////

function data() {
  return {
    activesections: config.activesections,
    settings: {},
    showSettingsModal: false
  };
}

//////////////////////////////
// Props
//////////////////////////////

let props = {
  activeSections: {
    type: Object,
    default: {}
  },
  appTitle: {
    type: String,
    default: ''
  }
};

//////////////////////////////
// Methods
//////////////////////////////

/**
 * Handles a toggle event for the settings modal
 */
function toggleSettingsModal() {
  this.showSettingsModal = !this.showSettingsModal;
  this.$emit('settingsModalToggled', this.showSettingsModal);
}

/**
 * Binds the newly updated settings value to our internal settings property
 * @param {Object} newVal - The new value of the settings object
 */
function updateSettings (newVal) {
  console.debug('Updating settings object...');
  this.settings = newVal;
}

//////////////////////////////
// Mounted
//////////////////////////////

function mounted() {
  let self = this;
  self.settings = self.viewerSettings;
}

//////////////////////////////
// Exported Module
//////////////////////////////

export default {
  components: {
    KinModalDisplay,
    KinSettingsModal
  },
  computed,
  created,
  data,
  methods: {
    toggleSettingsModal,
    updateSettings
  },
  mounted,
  props
};
