/**
 * Events
 * -------------------
 * @fires barClicked - Emitted when a bar segment is clicked
 * @fires bgClicked - Emitted when the bg behind the bars is clicked (often to reset the view)
 * @fires titleClicked - Emitted when a bar title is clicked (Often to select an entire bar, rather than a segment)
 */
import * as d3 from 'd3';
import * as lodash from 'lodash';
import helper from '@/helpers/chartHelper';

//////////////////////////////
// #region Demo Data
//////////////////////////////

let demoData = [
  {name: 'San Francisco', x: 200, y: 300},
  {name: 'Austin', x: 300, y: 400}
];

// #endregion

//////////////////////////////
// Computed
//////////////////////////////

let computed = {
  calcHeight(d) {
    let barHeights = this.barHeight * this.localData.length;
    let totalHeight = barHeights + this.margin.top + this.margin.bottom;
    return totalHeight;
  },
  margin() {
    let self = this;
    let margins = self.defaultMargin;

    // Account for label size
    let longestLabel = lodash.maxBy(self.localData, (d) => { return d.name.length; });
    if (longestLabel) {
      margins.left = helper.getTextWidth(longestLabel.name, self.fontSize + 'px arial') + 20;
    }

    // Account for total label size
    let longestTotal = lodash.maxBy(self.localData, (d) => { return d['total']; });
    if (longestTotal) {
      margins.right = helper.getTextWidth(longestTotal['total'], self.fontSize + 'px arial') + 40;
    }
    return margins;
  }
};

//////////////////////////////
// Created
//////////////////////////////

function created() {
  let self = this;
  if (self.useDemoData) {
    self.localData = lodash.cloneDeep(demoData);
  } else {
    self.localData = lodash.cloneDeep(self.chartData);
  }
}

//////////////////////////////
// Data
//////////////////////////////

function data() {
  return {
    calcWidth: 0,
    barHeight: 60,
    localData: []
  };
}

//////////////////////////////
// Methods
//////////////////////////////

// ----------------------- //
// #region Drawing

function addRemoveRects() {
  let self = this;
  let container = d3.select(self.$el);
  let svg = container.select('.kin-segmented-bar-chart');

  svg.selectAll('.bar-rect-group')
    .remove();

  let rectCount = svg.selectAll('.bar-rect').size();
  let dataGroups = self.localData.length;
  let rectGroups = rectCount / self.categories.length;
  let dataRects = self.localData.length * self.categories.length;
  let rectDiff = dataRects - rectCount;
  let groupDiff = dataGroups - rectGroups;
  let scales = self.getScales();

  // Do not add/remove if no disparity is found
  if (rectDiff === 0 && groupDiff === 0) {
    return;
  }

  console.dir('Rect disparity...');

  let stacked = d3.stack().keys(self.categories)(self.localData);

  // Transform the stack to include categories on segment
  lodash.forEach(stacked, (arr, i) => {
    lodash.forEach(arr, (d, i1) => {
      d[2] = self.categories[i];
    });
  });

  if (groupDiff > 0) {
    // Add rect groups
    console.debug(`Adding ${groupDiff} rect groups...`);
    svg.select('.bar-rects')
      .selectAll('.bar-rect-group')
      .data(stacked)
      .enter().append('g')
      .classed('bar-rect-group', true)
      .attr('fill', function(d) { return scales.z(d.key); });
      // .attr('transform', `translate(${self.margin.left}, ${self.margin.top})`);
  } else if (groupDiff < 0) {
    // Eliminate Rect Groups
    console.debug(`Eliminating ${groupDiff} excess rect groups...`);
    svg.select('.bar-rects')
      .selectAll('.bar-rect-group')
      .data(stacked)
      .exit()
      .remove();
  }

  // Add Rects
  if (rectDiff > 0) {
    console.debug(`Adding ${rectDiff} rects to groups...`);
    svg.selectAll('.bar-rect-group')
      .data(stacked)
      .selectAll('.bar-rect')
      .data((d) => { return d; })
      .enter()
      .append('rect')
        .classed('bar-rect', true);

    svg.selectAll('.bar-rect-group')
        .data(stacked)
        .selectAll('.rect-label')
        .data((d) => { return d; })
        .enter()
        .append('text')
          .classed('rect-label', true)
          .data((d) => { return d; });
  } else if (rectDiff < 0) {
    // Eliminate rects
    console.debug(`Eliminating ${rectDiff} rect groups...`);
    svg.selectAll('.bar-rect-group')
      .data(stacked)
      .selectAll('.bar-rect')
      .data((d) => { return d; })
      .exit()
      .remove();

    svg.selectAll('.bar-rect-group')
      .data(stacked)
      .selectAll('.rect-label')
      .data((d) => { return d; })
      .exit()
      .remove();
  }

  svg.select('.bar-rects')
    .attr('transform', `translate(${self.margin.left}, ${self.margin.top})`);
}

/**
 * Clears the axis of this chart
 * @param {Element} svg - The svg element of this chart
 */
function clearAxis(svg) {
  let clearables = ['.x-axis', '.y-axis', '.total-labels'];
  lodash.forEach(clearables, (d) => { svg.select(d).remove(); });
}

/**
 * Builds the chart axis including the total labels
 */
function updateAxis() {
  let self = this;

  if (self.localData.length === 0) {
    return;
  }

  // Build new axis
  let container = d3.select(self.$el);
  let svg = container.select('.kin-segmented-bar-chart');

  let scales = self.getScales();

  // Update the x-axis
  svg.select('.x-axis')
    // .transition()
    // .duration(2000)
    .call(d3.axisTop(scales.x).ticks(self.tickCount, self.tickFormat));

  // Update the Y-axis
  svg.select('.y-axis')
    // .transition()
    // .duration(2000)
    .call(d3.axisLeft(scales.y));

  svg.selectAll('.total-label')
    .remove();

  svg.selectAll('.total-label')
    .data(self.localData)
    .enter()
      .append('text')
      .classed('total-label', true)
      .text((d) => {
        if (d.total > 0) {
          return d.total;
        }
        return 'N/A';
      })
      .style('font-size', self.fontSize + 'px')
      .attr('y', (d) => {
        return scales.y(d.name) +
          self.margin.top + scales.y.bandwidth() / 2 + 5;
      })
      .attr('x', (d) => {
        return self.calcWidth - 30;
      });
}

/**
 * Clears and builds a new bar chart
 */
function buildChart() {
  let self = this;

  if (self.localData.length === 0) {
    return;
  }

  let container = d3.select(self.$el);
  let svg = container.select('.kin-segmented-bar-chart');
  let stacked = d3.stack().keys(self.categories)(self.localData);

  // Transform the stack to include categories on segment
  lodash.forEach(stacked, (arr, i) => {
    lodash.forEach(arr, (d, i1) => {
      d[2] = self.categories[i];
    });
  });

  let scales = self.getScales();

  // Bar rectangles
  let g = svg
    .selectAll('.bar-rect-group')
    .data(stacked)
    .selectAll('.bar-rect')
    .data((d, i) => { return d; })
    .attr('y', (d) => { return scales.y(d.data.name); })
    .attr('x', (d) => {
      return scales.x(d[0]) + 1;
    })
      .transition()
      .duration(1000)
      .attr('width', function(d) {
        return scales.x(d[1]) - scales.x(d[0]);
      })
      .attr('height', scales.y.bandwidth());

  // Bar labels
  // svg.selectAll('.bar-rect-group')
  //   .data(stacked)
  //   .selectAll('.rect-label')
  //   .data((d, i) => { return d; })
  //     .text((d) => {
        // // Only add a label if it can fit inside the bar
        // let textWidth = helper.getTextWidth(helper.titleCase(d[2])) + 15;
        // let barWidth = scales.x(d[1]) - scales.x(d[0]);
        // if (barWidth < textWidth) {
        //   return '';
        // }
        // return helper.titleCase(d[2]);
        // return '';
      // })
      // .style('fill', 'white')
      // .style('font-size', self.fontSize + 'px')
      // .attr('y', (d) => { return scales.y(d.data.name); })
      // .attr('transform', `translate(10, ${scales.y.bandwidth() / 2 + 4})`)
      // .attr('x', (d) => { return scales.x(d[0]); });

  // Set default highlighted state based on prop
  let selectedName = lodash.get(self, 'selectedData.name', null);
  let selectedCat = lodash.get(self, 'selectedData.category', null);
  let highlightables = ['.tick text', '.rect-label', '.bar-rect'];
  lodash.forEach(highlightables, (h) => {
    svg.selectAll(h)
      .classed('highlighted', false)
      .classed('faded', false);
  });

  if (!selectedName && !selectedCat) {
    lodash.forEach(highlightables, (className) => {
      svg.selectAll(className)
      .classed('highlighted', true);
    });
  } else {
    lodash.forEach(highlightables, (className) => {
      svg.selectAll(className)
        .classed('faded', true)
        .attr('class', (d, i, node) => {
          let dataName = lodash.get(d, 'data.name', '');
          let dataCat = d[2];
          // let condition = (selectedCat) ? dataName === selectedName && dataCat === selectedCat : dataName === selectedName;
          let altCondition = (dataCat === selectedCat) || (dataName === selectedName && !selectedCat);
          return helper.addHighlight(node[i], altCondition);
        });
    });
  }

  // Highlight label
  svg.selectAll('.tick text')
    .classed('highlighted', (d) => { return d === selectedName; })
    .classed('bold', (d) => { return d === selectedName; });

  // Display tooltip on bar hover
  let divTooltip = container.select('.bar-chart-tooltip');
  container.selectAll('.bar-rect, .rect-label').on('mousemove', (d) => {
    let oWidth = divTooltip.node().offsetWidth;
    divTooltip.style('left', d3.event.layerX + 20 - oWidth + 'px');
    divTooltip.style('top', d3.event.layerY + 35 + 'px');
    divTooltip.style('display', 'inline-block');
    divTooltip.html(`<b>${d.data.name} ${helper.titleCase(d[2])}</b>: ${d[1] - d[0]}`);
  });

  container.selectAll('.bar-rect').on('mouseout', (d) => {
    divTooltip.style('display', 'none');
  });

  svg.select('.chart-bg')
    .attr('height', self.calcHeight)
    .attr('width', self.calcWidth);

  self.setupHandlers(svg);
}

// #endregion

// ----------------------- //
// #region Handlers

/**
 * Registers the click/hover handlers for this chart
 * @param {Element} svg - The svg element for the chart
 */
function setupHandlers(svg) {
  let self = this;
  // Bar title clicks
  svg.selectAll('.tick text').on('click', self.handleLabelClick.bind(this, svg));
  svg.select('.chart-bg').on('click', self.handleBgClick.bind(this, svg));

  // Bar rectangle clicks
  svg.selectAll('.bar-rect').on('click', self.handleRectClick.bind(this, svg));
  svg.selectAll('.rect-label').on('click', self.handleRectClick.bind(this, svg));
}

/**
 * Handles a background click to reset the filter
 * @event bgClicked - Emits bgClicked
 */
function handleBgClick(svg) {
  // let self = this;

  // svg.selectAll('.bar-rect')
  //   .classed('faded', false)
  //   .classed('highlighted', false);

  // console.debug('Emitting bgClicked...');
  // // Emit the bgClicked event to trigger a reset
  // self.$emit('bgClicked');
  // self.$emit('input', null);
}

/**
 * Handles a label click
 * @param {Object} d - The data for that label
 * @event titleClicked - Emits titleClicked
 */
function handleLabelClick(svg, d) {
  let self = this;
  let key = lodash.findKey(self.localData, (k) => { return k.name === d; });
  let retVal = {
    name: d,
    data: self.localData[key]
  };

  console.debug('Emitting titleClicked...', retVal);
  self.$emit('titleClicked', retVal);
  self.$emit('input', retVal);
}

/**
 * Handles a rectangle click for this chart
 * @param {Object} d- The data for that rectangle
 * @event barClicked - Emits barClicked
 */
function handleRectClick(svg, d) {
  let self = this;
  let category = lodash.findKey(d.data, (k) => { return k === d[1] - d[0]; });
  let retVal = {
    clickedCategory: d[2],
    clickedValue: d.data[category],
    name: d.data.name,
    data: d.data
  };

  console.debug('Emitting barClicked...', retVal);
  self.$emit('barClicked', retVal);
  self.$emit('input', retVal);
}

// #endregion

/**
 * Returns the scales for each axis
 */
function getScales() {
  let self = this;

  self.localData = self.transformData(self.localData);

  let x = d3.scaleLinear()
    .range([0, self.calcWidth - self.margin.right - self.margin.left]);

  let y = d3.scaleBand()
    .range([0, self.calcHeight - self.margin.top - self.margin.bottom])
    .padding(0.2);

  let z = d3.scaleOrdinal()
    .range(self.barColors);

  // Scale the range of the data in the domains
  let max = d3.max(self.localData, (d) => { return d.total; });
  x.domain([ 0, max ]);
  y.domain(self.localData.map(function(d) { return d['name']; }));
  z.domain(self.categories);

  return {x, y, z};
}

/**
 * Transforms the data and adds totals to it
 */
function transformData(data) {
  let self = this;

  // Add up totals based on categories
  let transformed = data.map((d) => {
    d.total = 0;
    for (var i = 0; i < self.categories.length; i++) {
      d.total += d[self.categories[i]];
    }
    return d;
  });

  return transformed;
}

/**
 * Stores the width of the parent container after
 * the component has finished loading. Required
 * for automagic width sizing of this component
 */
function storeWidth() {
  let rawWidth, rawPaddingLeft, rawPaddingRight;
  try {
    let style = window.getComputedStyle(this.$parent.$refs[this.containerRef]);
    rawWidth = style.getPropertyValue('width');
    rawPaddingLeft = style.paddingLeft;
    rawPaddingRight = style.paddingRight;
  } catch (e) {
    rawWidth = this.$parent.$refs[self.containerRef].currentStyle.width;
    rawPaddingLeft = this.$parent.$refs[self.containerRef].currentStyle.paddingLeft;
    rawPaddingRight = this.$parent.$refs[self.containerRef].currentStyle.paddingRight;
  }
  this.calcWidth = helper.removePx(rawWidth) -
    helper.removePx(rawPaddingLeft) -
    helper.removePx(rawPaddingRight) - 15;
}

/**
 * Handles an update to the selected data
 */
function updateHandler() {
  console.debug('Detecting update in segmented bar chart...');
  let self = this;
  let container = d3.select(self.$el);
  let svg = container.select('.kin-segmented-bar-chart');
  self.localData = lodash.cloneDeep(self.chartData);

  self.clearAxis.bind(this)(svg);
  self.setupChart.bind(this)();

  self.storeWidth.bind(self)();
  self.updateAxis.bind(self)();
  self.addRemoveRects.bind(self)();
  self.buildChart.bind(self)();
}

/**
 * Runs once when the chart is created. Adds the static container groups
 * to the chart, that are manipulated when updated.
 */
function setupChart() {
  let self = this;
  let container = d3.select(self.$el);
  let svg = container.select('.kin-segmented-bar-chart');
  let scales = self.getScales();

  svg.selectAll('.bar-rect-group')
    .exit()
    .remove();

  // Add a clickable background for reset clicks
  svg.append('g')
    .classed('bg-container', true)
    .append('rect')
    .classed('chart-bg', true)
    .style('fill', 'transparent')
    .attr('x', 0)
    .attr('y', 0);

  svg.append('g')
    .classed('bar-rects', true);

    // add the x Axis
  svg.append('g')
  .classed('x-axis', true)
  .call(d3.axisTop(scales.x).ticks(self.tickCount, self.tickFormat))
  .attr('transform', `translate(${self.margin.left}, ${self.margin.top})`)
  .attr('font-size', self.fontSize + 'px');

  svg.append('g')
    .classed('y-axis', true)
    .call(d3.axisLeft(scales.y))
    .attr('transform', `translate(${self.margin.left}, ${self.margin.top})`)
    .attr('font-size', self.fontSize + 'px');

  // Add total labels
  svg.append('g')
    .classed('total-labels', true)
    .selectAll('.total-label')
    .data(self.localData)
    .enter()
      .append('text')
      .classed('total-label', true);
}

//////////////////////////////
// Mounted
//////////////////////////////

function mounted() {
  let self = this;

  self.setupChart.bind(this)();

  // Set the window resize handler
  let dbResizeHandler = lodash.debounce(() => {
    self.storeWidth.bind(self)();
    self.updateAxis.bind(self)();
    self.addRemoveRects.bind(self)();
    self.buildChart.bind(self)();
  }, 500);
  window.addEventListener('resize', dbResizeHandler);

  // Call it once upon mounting
  dbResizeHandler();
}

//////////////////////////////
// Props
//////////////////////////////

let props = {
  barColors: {
    type: Array,
    default: () => {
      return [
        '#98abc5',
        '#8a89a6',
        '#7b6888',
        '#6b486b',
        '#a05d56',
        '#d0743c',
        '#ff8c00'
      ];
    }
  },
  categories: {
    type: Array,
    default: () => { return ['x', 'y']; }
  },
  chartData: {
    type: Array,
    default: () => { return []; }
  },
  containerRef: {
    type: String,
    default: () => { return 'widgetBody'; }
  },
  fontSize: {
    type: Number,
    default: () => { return 18; }
  },
  defaultMargin: {
    type: Object,
    default: () => { return {top: 20, right: 20, bottom: 20, left: 20}; }
  },
  tickFormat: {
    type: String,
    default: 's'
  },
  tickCount: {
    type: Number,
    default: 10
  },
  selectedData: {
    type: Object,
    default: {}
  },
  useDemoData: {
    type: Boolean,
    default: () => { return false; }
  }
  // xTitle: {type: String},
  // yTitle: {type: String}
};

//////////////////////////////
// Exported Module
//////////////////////////////

export default {
  computed,
  created,
  data,
  methods: {
    addRemoveRects,
    buildChart,
    clearAxis,
    getScales,
    handleBgClick,
    handleLabelClick,
    handleRectClick,
    setupChart,
    setupHandlers,
    storeWidth,
    transformData,
    updateAxis,
    updateHandler
  },
  mounted,
  props,
  watch: {
    categories: {
      deep: true,
      handler: updateHandler
    },
    chartData: {
      deep: true,
      handler: updateHandler
    },
    'selectedData.category': updateHandler,
    'selectedData.name': updateHandler
  }
};
