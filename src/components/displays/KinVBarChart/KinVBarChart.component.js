import * as d3 from 'd3';
import * as lodash from 'lodash';

//////////////////////////////
// Test Data
//////////////////////////////

let testData = [
  {letter: 'A', frequency: 0.08167},
  {letter: 'B', frequency: 0.01492}
];

//////////////////////////////
// Props
//////////////////////////////

let props = {
  barClickCb: {
    type: Function,
    default: () => { console.debug('No barClickCb specified. Throwing default function.'); }
  },
  chartData: {
    type: Array
  },
  tickFormat: {
    type: String,
    default: 's'
  },
  tickCount: {
    type: Number,
    default: 5
  },
  height: {
    type: String,
    default: () => { return 500; }
  },
  width: {
    type: String,
    default: () => { return 500; }
  },
  xAttr: {
    type: String,
    default: () => { return 'x'; }
  },
  yAttr: {
    type: String,
    default: () => { return 'y'; }
  },
  xTitle: {type: String},
  yTitle: {type: String}
};

//////////////////////////////
// Computed
//////////////////////////////

let computed = {
  calcWidth() { return this.width - this.margin.left - this.margin.right; },
  calcHeight() { return this.height - this.margin.top - this.margin.bottom - this.titlePadding; },
  margin() {
    let self = this;
    let leftMargin = 20;
    let margins = {top: 20, right: 20, bottom: 20, left: leftMargin};
    let longestObj = lodash.maxBy(self.chartData, (d) => {
      if (!d[self.yAttr]) {
        console.warn('Y attribute does not match data object.');
        return;
      }
      return d[self.yAttr].length;
    });
    if (longestObj) {
      // leftMargin = this.getTextWidth(longestObj[self.yAttr], '8.5pt arial') + 10;
      // margins.left = leftMargin;
    }
    return margins;
  },
  labelOffset() { return 40; },
  paddingLeft() { return (this.yTitle) ? this.margin.left + this.titlePadding : this.margin.left; },
  paddingTop() { return (this.xTitle) ? this.margin.top + this.titlePadding : this.margin.top; }
};

//////////////////////////////
// Data
//////////////////////////////

function data() {
  return {
    titlePadding: 40,
    lastFilter: 'first-load'
  };
}

//////////////////////////////
// Mounted
//////////////////////////////

function mounted() {
  this.updateAxis();
  this.buildChart();
}

//////////////////////////////
// Updated
//////////////////////////////

function updated () {
  this.updateAxis();
  this.buildChart();
}

//////////////////////////////
// Methods
//////////////////////////////

/**
 * Retrieves the scales for the bar chart based on the height
 * passed in the props
 * @returns {Object} - An object containing the x and y scale functions
 */
function getScales() {
  let self = this;

  let xScale = d3.scaleLinear()
    .range([0, self.calcWidth]);

  let yScale = d3.scaleBand()
    .range([0, self.calcHeight])
    .padding(0.1);

  // Scale the range of the data in the domains
  xScale.domain([0, d3.max(self.chartData, function(d) { return d[self.xAttr]; })]);
  yScale.domain(self.chartData.map(function(d) { return d[self.yAttr]; }));

  return {x: xScale, y: yScale};
}

/**
 * Returns the css name for bars based on the existing attribute value
 * @param {String} attr - The existing x attribute value
 * @returns {String} - The built css name (without the leading period).
 */
function getCssName(attr) {
  return `bar-${attr.toLowerCase().split(' ').join('-')}`;
}

/**
 * Sets the on hover/click handlers for the chart
 * @param {Object} svg - The D3 SVG object
 */
function setHandlers(container) {
  let self = this;

  // Fire callback on bar click, and pass the bars data
  container.selectAll('.bar').on('click', (datum, index, nodeList) => {
    if (lodash.isEqual(self.lastFilter, datum)) {
      return;
    }

    container.selectAll('.bar')
    .classed('highlighted', false)
    .classed('unhighlighted', true);

    // Highlight only selected bar
    d3.select(nodeList[index])
    .classed('highlighted', true)
    .classed('unhighlighted', false);

    // Fire passed callback
    self.lastFilter = datum;
    self.barClickCb(datum);
  });

  // Display tooltip on bar hover
  let divTooltip = container.select('.kin-v-bar-chart-tooltip');
  container.selectAll('.bar').on('mousemove', (d) => {
    let self = this;
    divTooltip.style('left', d3.event.pageX + 10 + 'px');
    divTooltip.style('top', d3.event.pageY - 25 + 'px');
    divTooltip.style('display', 'inline-block');
    var x = d3.event.pageX;
    var y = d3.event.pageY;
    var elements = document.querySelectorAll(':hover');
    var l = elements.length - 1;
    var elementData = elements[l].__data__;
    divTooltip.html(`<b>${elementData[self.xAttr].toLocaleString()}</b><br>${elementData[self.yAttr]}`);
  });

  container.selectAll('.bar').on('mouseout', (d) => {
    divTooltip.style('display', 'none');
  });

  // Click off of filtering mode
  container.selectAll('.bar-bg').on('click', () => {
    if (!self.lastFilter) {
      return;
    }
    container.selectAll('.bar')
      .classed('highlighted', false)
      .classed('unhighlighted', false);
    self.lastFilter = null;
    self.barClickCb(null);
  });
}

/**
 * Uses canvas.measureText to compute and return the width of the given text of given font in pixels.
 * @param {String} text The text to be rendered.
 * @param {String} font The css font descriptor that text is to be rendered with (e.g. 'bold 14px verdana').
 * @see https://stackoverflow.com/questions/118241/calculate-text-width-with-javascript/21015393#21015393
 */
function getTextWidth(text, font) {
  // re-use canvas object for better performance
  var canvas = getTextWidth.canvas || (getTextWidth.canvas = document.createElement('canvas'));
  var context = canvas.getContext('2d');
  context.font = font;
  var metrics = context.measureText(text);
  return metrics.width;
}

/**
 * Clears existing axis from the svg
 */
function clearAxis() {
  let self = this;
  let container = d3.select(self.$el);
  let svg = container.select('.kin-v-bar-chart');
  svg.selectAll('.x-axis').remove();
  svg.selectAll('.y-axis').remove();
  svg.selectAll('.axis-titles').remove();
}

function updateAxis() {
  let self = this;

  // Clear the old axis on update
  self.clearAxis();

  // Build new axis
  let container = d3.select(self.$el);
  let svg = container.select('.kin-v-bar-chart');
  let scales = self.getScales();

  // add the y Axis
  // svg.append('g')
  // .classed('x-axis', true)
  // .call(d3.axisLeft(scales.y))
  // .attr('transform', `translate(${self.paddingLeft}, ${self.paddingTop})`);

  // add the x Axis
  svg.append('g')
    .classed('x-axis', true)
    .call(d3.axisTop(scales.x).ticks(self.tickCount, self.tickFormat))
    .attr('transform', `translate(${self.paddingLeft}, ${self.paddingTop})`);

  // Add titles to the chart
  let titles = svg.append('g')
    .classed('axis-titles', true);

  if (self.xTitle) {
    titles.append('svg:text')
    .text(self.xTitle)
    .attr('x', () => { return (self.width / 2) - (self.getTextWidth(self.xTitle, '8.5pt arial') / 2); })
    .attr('y', () => { return self.titlePadding / 2; });
  }

  if (self.yTitle) {
    titles.append('svg:text')
    .text(self.yTitle)
    .attr('x', () => { return self.titlePadding; })
    .attr('y', () => { return (self.height / 2); })
    .attr('transform', 'rotate(-90,' + (self.titlePadding / 2) + ',' + (self.height / 2) + ')');
  }
}

/**
 * Calculates the chart based on the data property and
 * builds it on the SVG in the view.
 */
function buildChart() {
  let self = this;
  let scales = self.getScales();
  let container = d3.select(self.$el);
  let svg = container.select('.kin-v-bar-chart');

  // Pad the bar chart
  svg.select('.bars')
  .attr('transform', 'translate(' + (self.paddingLeft) + ',' + (self.paddingTop) + ')');

  // Bind chart data to bar groups
  svg.selectAll('.bar')
    .data(self.chartData);

  // Add histogram rects
  svg.selectAll('.bars .bar .bar-rect')
    .data(self.chartData)
    .attr('id', (d) => { return `${self.getCssName(d[self.yAttr])}`; })
    .attr('x', (d) => { return 0; })
    .attr('y', (d, i) => { return scales.y(d[self.yAttr]); })
    .attr('height', scales.y.bandwidth())
    .attr('width', function(d, i) {
      if (!d[self.xAttr]) {
        console.warn('X attribute does not match data object.');
        return 0;
      }
      return scales.x(d[self.xAttr]);
    });

  // Add bar titles
  svg.selectAll('.bars .bar .bar-title')
    .data(self.chartData)
    .text((d) => {
      if (!d[self.yAttr]) {
        console.warn('Y attribute does not match data object.');
        return '';
      }
      return d[self.yAttr];
    })
    .attr('style', 'font-weight: bold')
    .attr('x', (d) => { return 5; })
    .attr('y', (d, i) => {
      if (!d[self.yAttr]) {
        console.warn('Y attribute does not match data object.');
        return 0;
      }
      return scales.y(d[self.yAttr]) + // Bar Spacing
        scales.y.bandwidth() / 3 + // Center within bar
        5; // Arbirary number for line-height
    });

  // Add bar values
  svg.selectAll('.bars .bar .bar-value')
    .data(self.chartData)
    .text((d) => {
      if (!d[self.xAttr]) {
        console.warn('X attribute does not match data object.');
        return '';
      }
      return d[self.xAttr].toLocaleString();
    })
    .attr('style', 'font-size: 11px;')
    .attr('x', (d) => { return 5; })
    .attr('y', (d, i) => {
      if (!d[self.yAttr]) {
        console.warn('Y attribute does not match data object.');
        return 0;
      }
      return scales.y(d[self.yAttr]) + // Bar Spacing
        (scales.y.bandwidth() / 3) * 2 + // Center within bar
        5; // Arbirary number for line-height
    });

  // Set hover/click handlers
  self.setHandlers(container);

  // Load up the legend
  // var options = d3.keys(self.chartData[0]).filter(function(key) { return key !== 'label'; });
  // var legend = svg.selectAll('.legend')
  //   .data(options.slice())
  //   .enter().append('g')
  //   .attr('class', 'legend')
  //   .attr('transform', (d, i) => { return 'translate(0,' + i * 20 + ')'; });

  // legend.append('rect')
  //     .attr('class', 'bar')
  //     .attr('y', 0)
  //     .attr('x', self.calcWidth - 18)
  //     .attr('width', 18)
  //     .attr('height', 18);

  // legend.append('text')
  //     .attr('x', self.calcWidth - 24)
  //     .attr('y', 9)
  //     .attr('dy', '.35em')
  //     .style('text-anchor', 'end')
  //     .text((d) => { return d; });
}

//////////////////////////////
// Module Exports
//////////////////////////////

export default {
  computed,
  data,
  methods: {
    buildChart,
    clearAxis,
    getTextWidth,
    getCssName,
    getScales,
    setHandlers,
    updateAxis
  },
  updated,
  mounted,
  props,
  watch: {
    xTitle: function(newVal, oldVal) { // watch it
      this.updateAxis();
      this.buildChart();
    }
  }
};
