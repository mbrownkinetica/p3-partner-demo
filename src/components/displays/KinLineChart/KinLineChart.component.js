/**
 * Events
 * ---------------------
 * @fires hitDotClicked - Fires when a hit dot has been clicked and includes the relevant data about the element.
 */
import * as d3 from 'd3';
import * as lodash from 'lodash';
import moment from 'moment';

import helper from '@/helpers/chartHelper';

//////////////////////////////
// #region Demo Data
//////////////////////////////

let demoData = [
  {
    name: 'San Francisco',
    values: [{
      name: 'San Francisco',
      temperature: 78.5,
      date: moment('2011-05-05').toISOString()
    },
    {
      name: 'San Francisco',
      temperature: 65.2,
      date: moment('2011-05-06').toISOString()
    },
    {
      name: 'San Francisco',
      temperature: 71.2,
      date: moment('2011-05-07').toISOString()
    }
    ]
  },
  {
    name: 'Austin',
    values: [{
      name: 'Austin',
      temperature: 30.5,
      date: moment('2011-05-05').toISOString()
    },
    {
      name: 'Austin',
      temperature: 35.1,
      date: moment('2011-05-06').toISOString()
    },
    {
      name: 'Austin',
      temperature: 48.1,
      date: moment('2011-05-07').toISOString()
    }]
  }
];

// #endregion

//////////////////////////////
// #region Computed
//////////////////////////////

let computed = {
  calcWidth() {
    if (this.calculatedWidth) {
      return this.calculatedWidth;
    }
    return 0;
  },
  calcHeight() {
    if (this.calculatedHeight) {
      return this.calculatedHeight;
    }
    return 0;
  },
  margin() {
    return {top: 20, right: 50, bottom: 20, left: 30};
  },
  widthNoMargin() {
    return 900;
  }
};

// #endregion

//////////////////////////////
// #region Created
//////////////////////////////

function created() {
  let self = this;
  if (self.useDemoData) {
    self.localData = lodash.cloneDeep(demoData);
  } else {
    self.localData = lodash.cloneDeep(self.chartData);
  }
}

// #endregion

//////////////////////////////
// #region Data
//////////////////////////////

function data() {
  return {
    calculatedWidth: null,
    calculatedHeight: null,
    localData: []
  };
}

// #endregion

//////////////////////////////
// #region Destroyed
//////////////////////////////

function destroyed() {
  let self = this;
  self.clearChart.bind(this)();
}

// #endregion

//////////////////////////////
// #region Mounted
//////////////////////////////

function mounted() {
  let self = this;
  let dbResizeHandler = lodash.debounce(() => {
    self.storeWidth.bind(this)();
    self.storeHeight.bind(this)();
    self.clearChart.bind(this)();
    self.buildChart.bind(this)();
  }, 500);
  window.addEventListener('resize', dbResizeHandler);
  dbResizeHandler();
}

// #endregion

//////////////////////////////
// #region Updated
//////////////////////////////

function updated() {
  let self = this;
  self.clearChart.bind(this)();
  self.buildChart.bind(this)();
}

// #endregion

//////////////////////////////
// #region Methods
//////////////////////////////

// #region Helpers

/**
 * Removes the px from the end of a style string
 * @param {String} str - The style string
 * @return {String} - The parsed number without the px
 */
function removePx(str) {
  return parseFloat(str.substr(0, str.length - 2));
}

/**
 * Stores the width of the parent container after
 * the component has finished loading. Required
 * for automagic width sizing of this component
 */
function storeWidth() {
  let self = this;
  let rawWidth, rawPaddingLeft, rawPaddingRight;
  try {
    let style = window.getComputedStyle(self.$parent.$refs[self.containerRef]);
    rawWidth = removePx(style.getPropertyValue('width'));
    rawPaddingLeft = style.paddingLeft;
    rawPaddingRight = style.paddingRight;
  } catch (e) {
    rawWidth = self.$parent.$refs[self.containerRef].currentStyle.width;
    rawPaddingLeft = self.$parent.$refs[self.containerRef].currentStyle.paddingLeft;
    rawPaddingRight = self.$parent.$refs[self.containerRef].currentStyle.paddingRight;
  }
  self.calculatedWidth = (rawWidth * 0.50) - removePx(rawPaddingLeft) - removePx(rawPaddingRight);
}

function storeHeight() {
  let self = this;
  let rawHeight, rawPaddingTop, rawPaddingBottom;
  try {
    let style = window.getComputedStyle(self.$parent.$refs[self.containerRef]);
    rawHeight = style.getPropertyValue('height');
    rawPaddingTop = style.paddingTop;
    rawPaddingBottom = style.paddingBottom;
  } catch (e) {
    rawHeight = self.$parent.$refs[self.containerRef].currentStyle.height;
    rawPaddingTop = self.$parent.$refs[self.containerRef].currentStyle.paddingTop;
    rawPaddingBottom = self.$parent.$refs[self.containerRef].currentStyle.paddingBottom;
  }
  self.calculatedHeight = Math.floor(removePx(rawHeight) - removePx(rawPaddingTop) - removePx(rawPaddingBottom)) - 5;
}

// #endregion

// #region Drawing

/**
 * Clears the SVG
 */
function clearChart() {
  let container = d3.select('.kin-line-chart-container');
  let svg = d3.select('svg.kin-line-chart');
  svg.selectAll('g').remove();
  container.select('.info')
    .select('ul')
    .remove();
}

function getScales(svg, height, width, groups) {
  let self = this;
  let x = d3.scaleTime().range([0, self.calcWidth - self.margin.left - self.margin.right]);
  let y = d3.scaleLinear().range([self.calcHeight - self.margin.top - self.margin.bottom, 0]);
  let z = d3.scaleOrdinal(d3.schemeCategory10);

  x.domain([
    d3.min(groups, function(c) { return d3.min(c.values, function(d) { return d3.isoParse(d[self.xAttr]); }); }),
    d3.max(groups, function(c) { return d3.max(c.values, function(d) { return d3.isoParse(d[self.xAttr]); }); })
  ]);

  y.domain([
    d3.min(groups, function(c) { return d3.min(c.values, function(d) { return d[self.yAttr]; }); }),
    d3.max(groups, function(c) { return d3.max(c.values, function(d) { return d[self.yAttr]; }); })
  ]);
  z.domain(groups.map(function(c) { return c.name; }));
  if (self.colors) {
    let colorArray = [];
    lodash.forEach(groups, (group) => {
      let color = lodash.find(self.colors, color => color.brand === group.name);
      colorArray.push('#' + color.color);
    });
    z.range(colorArray);
  }

  return {x, y, z};
}

/**
 * Builds the chart on the SVG element
 */
function buildChart() {
  let self = this;
  self.localData = lodash.cloneDeep(self.chartData);
  if (self.localData.length === 0) {
    return;
  }
  let container = d3.select('.kin-line-chart-container');
  let svg = d3.select('svg.kin-line-chart');
  let g = svg.append('g').attr('transform', 'translate(' + self.margin.left + ',' + self.margin.top + ')');

  svg.attr('height', self.calcHeight)
    .attr('width', self.calcWidth - self.margin.left);

  let scales = self.getScales(svg, self.calcHeight, self.calcWidth, self.localData);

  // Add Chart Background
  g.append('rect')
    .classed('chart-bg', true)
    .style('fill', 'transparent')
    .attr('x', 0)
    .attr('y', 0)
    .attr('height', self.calcHeight)
    .attr('width', self.calcWidth);

  // X-Axis
  g.append('g')
    .attr('class', 'axis axis-x')
    .attr('transform', 'translate(0,' + (self.calcHeight - self.margin.bottom - self.margin.top) + ')')
    .call(d3.axisBottom(scales.x).tickFormat(d3.timeFormat('%m/%y')));

  // Y-Axis
  g.append('g')
      .attr('class', 'axis axis-y')
      .call(d3.axisLeft(scales.y))
    .append('text')
      .attr('transform', 'rotate(-90)')
      .attr('y', 6)
      .attr('dy', '0.71em')
      .attr('fill', '#757575')
      .text(self.yAttr);

  let line = d3.line()
    .x((d) => { return scales.x(d3.isoParse(d['date'])); })
    .y((d) => { return scales.y(d[self.yAttr]); });

  // Build groups
  var group = g.selectAll('.line-group')
    .data(self.localData)
    .enter()
    .append('g')
    .classed('line-group', true);

  // Create Line Interpolations
  group.append('path')
    .attr('class', 'line')
    .style('fill', 'transparent')
    .style('stroke', (d) => { return scales.z(d.name); })
    .style('stroke-width', 2)
    // .transition().duration(2000)
    .attr('d', (d) => { return line(d.values); });

  // Create Data Points
  group.selectAll('.dot')
    .data((d) => { return d.values; })
    .enter()
    .append('circle')
    .classed('dot', true)
    .attr('cx', (d) => { return scales.x(d3.isoParse(d[self.xAttr])); })
    .attr('cy', (d) => { return scales.y(d[self.yAttr]); })
    .attr('r', (d) => { return 3; })
    .attr('fill', (d) => { return scales.z(d.name); });

  // // Add hit dots that are larger than the actual dots
  group.selectAll('.hit-dot')
    .data((d) => { return d.values; })
    .enter()
    .append('circle')
    .attr('class', 'hit-dot')
    .attr('cx', (d) => { return scales.x(d3.isoParse(d[self.xAttr])); })
    .attr('cy', (d) => { return scales.y(d[self.yAttr]); })
    .attr('r', (d) => { return 5; })
    .attr('fill', 'transparent');

  // Line Labels
  // group.append('text')
  //   .datum(function(d) { return {id: d.name, value: d.values[d.values.length - 1]}; })
  //   .attr('transform', function(d) {
  //     return `translate(${scales.x(d3.isoParse(d.value[self.xAttr]))},${scales.y(d.value[self.yAttr])})`;
  //   })
  //   .attr('x', -5)
  //   .attr('y', 5)
  //   .attr('dy', '0.35em')
  //   .style('font', '10px sans-serif')
  //   .style('text-anchor', 'end')
  //   .text(function(d) { return d.name; });

  // Alt Legend
  var legend = d3.select('.legend');
  legend.style('max-height', self.calculatedHeight - self.margin.bottom + 'px');
  legend.style('overflow', 'auto');
  let test = legend.selectAll('li')
    .data(self.localData)
    .enter()
    .append('li')
      // .attr('class', 'clearfix legend-item');
      .classed('legendary-item', true)
      .classed('clearfix', true);
  test
    .append('div')
    .classed('legend-box', true)
    .style('background-color', (d) => { return scales.z(d['name']); });
  test
    .append('div')
    .classed('legend-label', true)
    .text((d) => { return d.name; });

  d3.select('.info')
    // .remove('li')
    .append('ul')
      .classed('info-list', true)
      .style('max-height', self.calculatedHeight - 19 + 'px')
      .append('li')
        .classed('default-message', true)
        .text('Select a brand to see its score over time.');

  // Legend Boxes
  // let rectWidth = 30;
  // let rectHeight = 15;
  // let textPadding = 2;
  // svg.append('g')
  //   .classed('legend', true)
  //   .selectAll('.legend-box')
  //   .data(self.localData)
  //   .enter()
  //     .append('rect')
  //     .classed('legend-rect', true)
  //     .style('fill', (d) => { return scales.z(d.name); })
  //     .attr('rx', 4)
  //     .attr('ry', 4)
  //     .attr('x', (d, i) => {
  //       let lastObj = self.localData[i - 1];
  //       let textWidth = 0;
  //       if (lastObj) {
  //         textWidth = helper.getTextWidth(lastObj.name, 'arial 10px');
  //       }
  //       return (rectWidth * i) + textWidth;
  //     })
  //     .attr('y', (d) => {
  //       return self.calcHeight - rectHeight;
  //     })
  //     .attr('width', rectWidth)
  //     .attr('height', rectHeight);

  // svg.select('.legend')
  //   .selectAll('.legend-label')
  //   .data(self.localData)
  //   .enter()
  //   .append('text')
  //   .classed('legend-label', true)
  //   .style('font-size', '10px')
  //   .text((d) => { return d.name; })
  //   .attr('y', (d) => { return self.calcHeight; })
  //   .attr('x', (d, i) => {
  //     let lastObj = self.localData[i - 1];
  //     let textWidth = 0;
  //     if (lastObj) {
  //       textWidth = helper.getTextWidth(lastObj.name, 'arial 10px');
  //     }
  //     return (rectWidth * i) + textWidth + rectWidth + textPadding;
  //   });

  // Add highlights if applicable
  // Set default highlighted state based on prop
  let selectedName = lodash.get(self, 'selectedData.name', null);
  let highlightables = ['.dot', '.line', '.legendary-item'];

  lodash.forEach(highlightables, (className) => {
    container.selectAll(className)
      .classed('faded', false)
      .classed('highlighted', false);
  });

  if (selectedName) {
    lodash.forEach(highlightables, (className) => {
      container.selectAll(className)
        .classed('faded', true)
        .classed('highlighted', false)
        .attr('class', (d, i, node) => {
          let dataName = lodash.get(d, 'name', '');
          let condition = dataName === selectedName;
          return helper.addHighlight(node[i], condition);
        });
    });

    let selection = lodash.find(self.localData, d => d.name === selectedName);
    container.select('.info-list')
        .selectAll('li')
        .remove();
    if (selection) {
      container.select('.info-list')
        .selectAll('li')
        .data(selection.values).enter()
        .append('li')
          .html((d) => {
            console.dir(d);
            return `<b>Date</b>: ${d['date'].format('DD/MM/YYYY')}<br/><b>Score</b>: ${Math.round(d[self.yAttr])}`;
          });
    }
  }

  self.setupHandlers();
}

// #endregion

// #region Handlers

function updateHandler() {
  console.debug('Detected update in line chart data...');
  this.clearChart();
  this.buildChart();

  // let self = this;
  // let svg = d3.select('svg.kin-line-chart');
  // self.localData = lodash.cloneDeep(self.chartData);
  // let line = d3.line()
  //   .x((d) => { return scales.x(d3.isoParse(d['date'])); })
  //   .y((d) => { return scales.y(d[self.yAttr]); });

  // let scales = self.getScales(svg, self.calcHeight, self.calcWidth, self.localData);

  // // Update Axis
  // // X-Axis
  // svg.select('.axis-x')
  //   // .transition().duration(2000)
  //   .call(d3.axisBottom(scales.x));

  // // Y-Axis
  // svg.select('.axis-y')
  //   // .transition().duration(2000)
  //   .call(d3.axisLeft(scales.y));

  // // Update lines
  // let group = svg.selectAll('.line-group')
  //   .data(self.localData);

  // svg.selectAll('.line')
  //   // .transition().duration(2000)
  //   .attr('d', (d, i, node) => {
  //     return line(self.localData[i].values);
  //   });

  // // Update dots
  // svg.selectAll('.line-group')
  //   .selectAll('.dot')
  //   .data((d) => {
  //     return d.values;
  //   })
  //   // .transition().duration(2000)
  //   .attr('cy', (d) => { return scales.y(d[self.yAttr]); });

  // // Update Hit Dots
  // group.selectAll('.hit-dot')
  //   .data((d) => { return d.values; })
  //   .attr('cy', (d) => { return scales.y(d[self.yAttr]); });

  // // Add highlights if applicable
  // // Set default highlighted state based on prop
  // let selectedName = lodash.get(self, 'selectedData.name', null);
  // let highlightables = ['.dot', '.line', '.legend-rect', '.legend-label'];
  // lodash.forEach(highlightables, (className) => {
  //   svg.selectAll(className)
  //     .classed('highlighted', false);
  // });

  // if (selectedName) {
  //   lodash.forEach(highlightables, (className) => {
  //     svg.selectAll(className)
  //       .classed('faded', true)
  //       .attr('class', (d, i, node) => {
  //         let dataName = lodash.get(d, 'name', '');
  //         let condition = dataName === selectedName;
  //         return helper.addHighlight(node[i], condition);
  //       });
  //   });
  // } else {
  //   lodash.forEach(highlightables, (className) => {
  //     svg.selectAll(className)
  //       .classed('faded', false)
  //       .classed('highlighted', false);
  //   });
  // }
}

/**
 * Sets up the event handlers used by the line chart
 * @param {Object} svg - The D3 svg object
 */
function setupHandlers() {
  let self = this;
  let container = d3.select('.kin-line-chart-container');
  let svg = container.select('svg');
  let divTooltip = container.select('.kin-line-chart-tooltip');

  // svg.selectAll('.hit-dot')
  //   .on('mousemove', self.handleHitDotMouseMove.bind(self, divTooltip));
  svg.selectAll('.hit-dot')
    .on('mouseout', handleHitDotMouseOut.bind(self, divTooltip));
  svg.selectAll('.hit-dot')
    .on('click', self.handleHitDotClick.bind(self, svg));
  svg.select('.chart-bg')
    .on('click', self.handleChartBgClick.bind(self, svg));
  container.selectAll('.legendary-item')
    .on('click', self.handleLegendRectClick.bind(self, container));
}

/**
 * Handles a legend click and emits a legendClick event
 * @param {Element} svg - The d3 svg element
 * @param {Object} d - The data for the clicked element
 * @event legendClicked - The legendClicked event
 */
function handleLegendRectClick(svg, d) {
  let self = this;
  // self.clearDotsAndLinesSelection(svg);
  // self.selectDotsAndLines.bind(self)(svg, d);
  console.debug('Emitting legendClicked...', d);
  self.$emit('legendClicked', d);
  self.$emit('input', d);
}

/**
 * Handles a background click on this chart
 * @param {Element} svg - The d3 svg element
 * @event bgClicked - Fires a bgClicked event
 */
function handleChartBgClick(svg) {
  let self = this;

  // self.clearDotsAndLinesSelection(svg);

  console.debug('Emitting bgClicked...');
  self.$emit('bgClicked');
  self.$emit('input', null);
}

/**
 * Handles the hit dot click event
 * @param {Element} svg - The svg element
 * @param {Object} d - The data object for the clicked element
 * @event hitDotClicked - Fires the hitDotClicked event
 */
function handleHitDotClick(svg, d) {
  let self = this;

  // self.clearDotsAndLinesSelection(svg);
  // self.selectDotsAndLines(svg, d);

  console.debug('Emitting hitDotClicked...', d);
  self.$emit('hitDotClicked', d);
  self.$emit('input', d);
}

/**
 * Handles the mouse move event over a hit dot
 * @param {Element} divTooltip - The tooltip div element
 * @param {Object} d - The data object for the hovered element
 */
function handleHitDotMouseMove(divTooltip, d) {
  let self = this;
  let oWidth = divTooltip.node().offsetWidth;
  divTooltip.style('left', d3.event.layerX - oWidth + 20 + 'px');
  divTooltip.style('top', d3.event.layerY + 20 + 'px');
  divTooltip.style('display', 'block');
  var elements = document.querySelectorAll(':hover');
  var l = elements.length - 1;
  var elementData = elements[l].__data__;
  let keyVals = [];
  keyVals.push(`<b>${self.xAttr}</b>: ${d3.isoParse(elementData[self.xAttr]).toLocaleString()}`);
  keyVals.push(`<b>${self.yAttr}</b>: ${elementData[self.yAttr].toLocaleString()}`);
  divTooltip.html(keyVals.join('<br/>'));
}

/**
 * Handles the hit dot mouse out event
 * @param {Element} divTooltip - The tooltip div element
 * @param {Object} d - The data object for the hovered element
 */
function handleHitDotMouseOut(divTooltip, d) {
  divTooltip.style('display', 'none');
}

/**
 * Selects all dots and lines that match the id in the data object
 * @param {Element} svg - The d3 element
 * @param {Object} d - The selected data
 */
function selectDotsAndLines(svg, d) {
  let classes = ['.dot', '.line', '.legend-label', '.legend-rect'];
  lodash.forEach(classes, (className) => {
    svg.selectAll(className)
      .classed('faded', true)
      .attr('class', (t, i, node) => {
        return helper.addHighlight(node[i], t.name === d.name);
      });
  });
}

/**
 * Clears faded and highlighted modifiers from the dots and lines
 * @param {Element} svg - The d3 svg
 */
function clearDotsAndLinesSelection(svg) {
  let clearables = ['.dot', '.line', '.legend-label', '.legend-rect'];
  lodash.forEach(clearables, (clearable) => {
    svg.selectAll(clearable)
    .classed('faded', false)
    .classed('highlighted', false);
  });
}

// #endregion

// #endregion

//////////////////////////////
// #region Props
//////////////////////////////

let props = {
  chartData: {
    type: Array,
    default: () => { return []; }
  },
  containerRef: {
    type: String,
    default: () => { return 'widgetBody'; }
  },
  colors: {
    type: Array,
    default: () => { return null; }
  },
  selectedData: {
    type: Object,
    default: {name: null}
  },
  height: {
    type: Number,
    default: () => { return 500; }
  },
  useDemoData: {
    type: Boolean,
    default: () => { return false; }
  },
  xAttr: {
    type: String,
    default: 'date'
  },
  yAttr: {
    type: String,
    default: 'temperature'
  }
};

// #endregion

//////////////////////////////
// #region Exported Module
//////////////////////////////

export default {
  created,
  computed,
  data,
  destroyed,
  methods: {
    buildChart,
    clearChart,
    clearDotsAndLinesSelection,
    getScales,
    handleChartBgClick,
    handleHitDotClick,
    handleHitDotMouseMove,
    handleHitDotMouseOut,
    handleLegendRectClick,
    selectDotsAndLines,
    setupHandlers,
    storeHeight,
    storeWidth,
    updateHandler
  },
  mounted,
  props,
  updated,
  watch: {
    chartData: {
      deep: true,
      handler: updateHandler
    },
    selectedData: {
      deep: true,
      handler: updateHandler
    }
  }
};
// #endregion
