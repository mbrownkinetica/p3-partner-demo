import { Chrome } from 'vue-color';

//////////////////////////////
// Data
//////////////////////////////

function data () {
  return {
    color: this.defaultColor || '#FF0000',
    outputColor: this.defaultColor || '#FF0000',
    pickerShown: false
  };
}

//////////////////////////////
// Computed
//////////////////////////////

let computed = {
  currentHex () { return this.outputColor.hex || this.outputColor; }
};

//////////////////////////////
// Methods
//////////////////////////////

function commitColor() {
  let self = this;
  self.outputColor = self.color;
  self.pickerShown = !self.pickerShown;
  this.$emit('input', this.outputColor);
}

function revertColor() {
  let self = this;
  self.color = self.outputColor;
  self.pickerShown = !self.pickerShown;
}

//////////////////////////////
// Properties
//////////////////////////////

let props = ['defaultColor'];

//////////////////////////////
// Module Exports
//////////////////////////////

export default {
  components: {
    'color-picker': Chrome
  },
  computed,
  data,
  methods: {
    commitColor,
    revertColor
  },
  props
};
