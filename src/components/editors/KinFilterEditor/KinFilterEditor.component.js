import KinFilterRowEditor from '@/components/editors/KinFilterRowEditor';
import EventBus from '@/helpers/eventBus';

const lodash = require('lodash');
const uuid = require('uuid').v1;

//////////////////////////////
// Fixtures
//////////////////////////////

let filterFixture = {
  expression: ''
};

//////////////////////////////
// Created
//////////////////////////////

function created () {
  EventBus.$on('KinFilterScreen-clearFilters', () => {
    Object.assign(this.$data, this.$options.data());
  });
}

//////////////////////////////
// Data
//////////////////////////////

function data () {
  return {
    expressionOutput: '',
    filters: []
  };
}

//////////////////////////////
// Methods
//////////////////////////////

function addFilter () {
  let newFilter = lodash.cloneDeep(filterFixture);
  newFilter.id = uuid();
  this.filters.push(newFilter);
}

function removeFilter (instance) {
  let index = this.filters.indexOf(instance);
  this.filters.splice(index, 1);
  updateExpressionOutput.bind(this)();
}

function updateExpression (exp) {
  let filter = lodash.find(this.filters, (filter) => {
    return filter.id === exp.filterId;
  });
  if (!filter) {
    return;
  }
  filter.expression = exp.expression;
  updateExpressionOutput.bind(this)();
}

function updateExpressionOutput () {
  this.expressionOutput = '';
  lodash.forEach(this.filters, (filter, index) => {
    if (index > 0) {
      this.expressionOutput += ' AND ';
    }
    this.expressionOutput += filter.expression;
  });
  this.$emit('input', this.expressionOutput);
}

//////////////////////////////
// Props
//////////////////////////////

let props = ['fields'];

//////////////////////////////
// Component
//////////////////////////////

export default {
  created,
  data,
  components: {
    KinFilterRowEditor
  },
  methods: {
    addFilter,
    removeFilter,
    updateExpression
  },
  props
};
