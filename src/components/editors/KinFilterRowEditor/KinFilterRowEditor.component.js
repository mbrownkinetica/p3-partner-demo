//////////////////////////////
// Data
//////////////////////////////

function data () {
  return {
    expression: '',
    filterValue: '',
    operator: '',
    columnName: '',
    operators: [
      {
        name: '>',
        operator: '>'
      },
      {
        name: '<=',
        operator: '>='
      },
      {
        name: '<',
        operator: '<'
      },
      {
        name: '<=',
        operator: '<='
      },
      {
        name: '=',
        operator: '='
      },
      {
        name: '!=',
        operator: '!='
      }
    ]
  };
}

//////////////////////////////
// Methods
//////////////////////////////

function updateExpression () {
  this.expression = `${this.columnName} ${this.operator} ${this.filterValue}`;
  this.$emit('input', this);
}

//////////////////////////////
// Props
//////////////////////////////

let props = ['fields', 'filterId'];

//////////////////////////////
// Component
//////////////////////////////

export default {
  data,
  methods: {
    updateExpression
  },
  props
};
