// Modules
import * as lodash from 'lodash';
import RangeSlider from 'vue-range-slider';
import { mapGetters } from 'vuex';

// Components
import KinColorPicker from '@/components/editors/KinColorPicker';
import KinTableGeometryEditor from '@/components/editors/KinTableGeometryEditor';
import KinTableNameEditor from '@/components/editors/KinTableNameEditor';
import KinRasterSettingsEditor from '@/components/editors/KinRasterSettingsEditor';
import KinCbRasterSettingsEditor from '@/components/editors/KinCbRasterSettingsEditor';
import KinHeatmapSettingsEditor from '@/components/editors/KinHeatmapSettingsEditor';
import mbHelper from '@/helpers/mapboxHelper';

// Helpers
import lsHelper from '@/helpers/localStorageHelper';

// External CSS
import 'vue-range-slider/dist/vue-range-slider.css';

// Config
import config from '@/config/config';

//////////////////////////////
// Properties
//////////////////////////////

let props = {
  layer: {
    type: Object,
    default: () => { return null; }
  },
  filtering: {
    type: Boolean,
    default: () => { return false; }
  },
  removeLayerFn: {
    type: Function,
    default: () => { console.debug('No remove layer function registered.'); }
  },
  refreshFn: {
    type: Function,
    default: () => { console.debug('No refresh function registered.'); }
  },
  tables: {
    type: Array,
    default: () => { return []; }
  }
};

//////////////////////////////
// Fixtures
//////////////////////////////

let defaultColors = {
  hex: '#194d33',
  hsl: {
    h: 150,
    s: 0.5,
    l: 0.2,
    a: 1
  },
  hsv: {
    h: 150,
    s: 0.66,
    v: 0.30,
    a: 1
  },
  rgba: {
    r: 25,
    g: 77,
    b: 51,
    a: 1
  },
  a: 1
};

//////////////////////////////
// Data
//////////////////////////////

function data () {
  return {
    // Bind layer opacity to local property
    advancedSettingsOpen: false,
    blurRadius: lodash.get(this, 'layer.params.BLUR_RADIUS', 5),
    color: lodash.cloneDeep(defaultColors),
    layerOpacity: lodash.get(this, 'layer.opacity', 1),
    layerOpen: false,
    layerSettingsKey: 'kinetica-layer-settings',
    pointSizes: 3,
    querySettingsOpen: false,
    orderSettingsOpen: false,
    renderPoints: lodash.get(this, 'layer.params.DOPOINTS', config.layerDefaults.renderPoints),
    renderShapes: lodash.get(this, 'layer.params.DOSHAPES', config.layerDefaults.renderShapes),
    renderSymbology: lodash.get(this, 'layer.params.DOSYMBOLOGY', config.layerDefaults.renderSymbology),
    visualizationSettingsOpen: false,
    wktAttr: lodash.get(this, 'layer.wktAttr', 'wkt')
  };
}

//////////////////////////////
// Hooks
//////////////////////////////

/**
 * The created hook
 */
function created () {
  // Debounce event handlers
  let debouncedFns = [
    'updatePointColor'
  ];
  _registerDeboucedFns.bind(this)(debouncedFns);
}

/**
 * Strips out the hash from a hex color string
 * @param {String} value - The hex color
 * @returns {String} - The hex color without the hash
 */
function stripHash (value) {
  return lodash.clone(value.substring(1, value.length));
}

/**
 * Registers all debounced functions passed to it
 * @param {Array<String>} fns - A list of function names bound to "this"
 */
function _registerDeboucedFns (fns) {
  let self = this;
  lodash.forEach(fns, (fn) => {
    // TODO: Figure out why debouncing like this cuases layers to suddenly disappear
    // self[fn] = lodash.debounce(self[fn], 500)
    self[fn] = self[fn];
  });
}

//////////////////////////////
// Methods
//////////////////////////////

/**
 * Binds the internal model to the layer parameters
 */
function _updateLayerSettings () {
  // TODO: Come up with a more streamlined way of binding this stuff
  let layerSettings = lsHelper.getObject(this.layerSettingsKey, {});
  layerSettings[this.layer.id] = {
    pointColors: this.color,
    renderPoints: lodash.get(this, 'layer.params.DOPOINTS', config.layerDefaults.renderPoints),
    renderShapes: lodash.get(this, 'layer.params.DOSHAPES', config.layerDefaults.renderShapes),
    renderSymbology: lodash.get(this, 'layer.params.DOSYMBOLOGY', config.layerDefaults.renderSymbology),
    visible: lodash.get(this, 'layer.visible', config.layerDefaults.visible),
    xAttr: lodash.get(this, 'layer.params.X_ATTR', config.layerDefaults.X_ATTR),
    yAttr: lodash.get(this, 'layer.params.Y_ATTR', config.layerDefaults.Y_ATTR),
    wktAttr: lodash.get(this, 'layer.wktAttr', config.layerDefaults.wktAttr)
  };
  lsHelper.setObject(this.layerSettingsKey, layerSettings);
}

/**
 * Toggles whether the layer details are open
 */
function toggleLayer () {
  this.layerOpen = !this.layerOpen;
}

/**
 * Updates a custom layer parameter then refreshses the layer and persists the config to local storage
 * @param {Event} e - The passed event
 * @param {String} name - The name of the layer parameter to update
 */
function updateParam (name, value) {
  _updateLayerSettings.bind(this)();
  this.refreshLayer();
}

/**
 * Handles updating of the wkt column property
 */
function updateWkt (e) {
  let newVal = lodash.get(e, 'target.value', 'wkt');
  lodash.set(this, 'layer.wktAttr', newVal);
  this.refreshLayer();
  _updateLayerSettings.bind(this)();
}

/**
 * Toggles whether the layer is visible
 */
function toggleVisibility () {
  console.debug('Toggling layer visibility...');
  if (!this.layer.visible) {
    this.layer.show();
    this.visible = true;
    _updateLayerSettings.bind(this)();
  } else {
    this.visible = false;
    this.layer.hide();
    _updateLayerSettings.bind(this)();
  }
}

/**
 * Force-refreshes the layer
 */
function refreshLayer () {
  this.refreshFn();
}

/**
 * Updates point color in the layer parameters
 */
function updatePointColor (newVal) {
  // this.colors = newVal // Update model
  let hexColor = this.stripHash(newVal.hex);
  lodash.set(this, 'layer.params.POINTCOLORS', hexColor);
  lsHelper.setObjectProperty(this.layerSettingsKey, this.layer.id, 'pointColors', newVal);
  this.refreshLayer();
  _updateLayerSettings.bind(this)();
}

/**
 * Updates the point size for the raster layer
 */
function updatePointSize (newVal) {
  console.debug('Updating point sizes to ' + newVal + '...');
  lodash.set(this, 'layer.params.POINTSIZES', newVal);
  this.refreshLayer();
}

/**
 * Fires when the point shape has been updated
 * @param {String} newVal - The new point shape
 */
function updatePointShape (newVal) {
  console.debug(`Updating point shape to ${newVal}...`);
  lodash.set(this, 'layer.params.POINTSHAPES', newVal);
  this.refreshLayer();
}

/**
 * Handles a blur radius update event
 * @param {Number} newVal - The new blur radius value
 */
function updateBlurRadius(newVal) {
  lodash.set(this, 'layer.params.BLUR_RADIUS', newVal);
  this.refreshLayer();
}

/**
 * Handles a color map update event
 * @param {String} newVal - The new color map value
 */
function updateColorMap(newVal) {
  lodash.set(this, 'layer.params.COLORMAP', newVal);
  this.refreshLayer();
}

function updateUrlParams(newVal) {
  lodash.set(this, 'layer.params.pointSizes', newVal.pointSizes);
  lodash.set(this, 'layer.params.pointColors', newVal.pointColors);
  lodash.set(this, 'layer.params.pointShapes', newVal.pointShapes);
  lodash.set(this, 'layer.params.cb_attr', newVal.cbAttr);
  lodash.set(this, 'layer.params.cb_vals', newVal.ranges);
  this.refreshLayer();
}

//////////////////////////////
// Computed
//////////////////////////////

let computed = {
  ...mapGetters(['webmap']),
  layerName() {
    let fallback = lodash.get(this, 'layer.id', 'Unknown Layer');
    return lodash.get(this, 'layer.layerInfos[0].name', fallback);
  },
  isHeatmap() { return lodash.get(this, 'layer.params.STYLES', null) === 'heatmap'; },
  isRaster() { return lodash.get(this, 'layer.params.STYLES', null) === 'raster'; },
  isCbRaster() { return lodash.get(this, 'layer.params.STYLES', null) === 'cb_raster'; },
  layerStyles() {
    let retVal = [];
    retVal.push({key: 'heatmap', value: 'Heatmap'});
    retVal.push({key: 'raster', value: 'Raster'});
    retVal.push({key: 'cb_raster', value: 'CB Raster'});
    return retVal;
  }
};

//////////////////////////////
// Watchers
//////////////////////////////

/**
 * Binds layer opacity changes from the UI to the layer's internal settings
 * @param {Float} - The new layer opacity value
 */
async function layerOpacity (newVal) {
  let self = this;
  let parsedOpacity = parseFloat(newVal);
  if (parsedOpacity) {
    let map = await self.webmap;
    map.setPaintProperty(self.layer.layerId, 'raster-opacity', newVal);
  }
}

/**
 * Binds the blur radius change with the layer parameter
 * @param {Number} - The new blur radius value
 */
function blurRadius (newVal) {
  let parsedBlurRadius = parseInt(newVal);
  if (parsedBlurRadius) {
    lodash.set(this, 'layer.params.BLUR_RADIUS', parsedBlurRadius);
  }
}

function layer (newVal, oldVal) {
  console.log('Detected layer change...');
}

//////////////////////////////
// Component
//////////////////////////////

export default {
  components: {
    KinColorPicker,
    KinCbRasterSettingsEditor,
    KinHeatmapSettingsEditor,
    KinRasterSettingsEditor,
    KinTableGeometryEditor,
    KinTableNameEditor,
    RangeSlider
  },
  created,
  computed,
  data,
  methods: {
    refreshLayer,
    stripHash,
    toggleLayer,
    toggleVisibility,
    updatePointColor,
    updatePointSize,
    updatePointShape,
    updateWkt,
    updateParam,
    updateColorMap,
    updateBlurRadius,
    updateUrlParams
  },
  props,
  watch: {
    layerOpacity,
    blurRadius,
    layer
  }
};
