export default {
  appTitle: 'Signal Quality Explorer',
  apiKey: 'pk.eyJ1IjoibWJyb3dua2luZXRpY2EiLCJhIjoiY2o4eW01d28zMDhweDMybzR2a3lkZ3E0ZCJ9.gpZZylcYNzX9NeIeklG69A',
  mapStyle: 'mapbox://styles/mapbox/dark-v9',
  mapDiv: 'web-map',
  initialExtent: null, // TODO: Add a way to set initial extent
  defaultPointQueryRadius: 0.02,
  defaultRecordLimit: 10,
  defaultToasterTimeout: 5000,
  defaultXAttr: 'LocationLongitude',
  defaultYAttr: 'LocationLatitude',
  activeSections: {
    dashboard: false,
    chartFilter: false,
    clustering: true,
    filterBuilder: true,
    map: false,
    raster: true,
    p3ScoreDashboard: false,
    p3SignalDashboard: false,
    p3SignalDensityDashboard: false
  },
  layerDefaults: {
    pointColors: '#FF000',
    pointSizes: 3,
    pointShapes: 'circle',
    renderPoints: true,
    renderShapes: true,
    renderSymbology: true,
    xAttr: 'x',
    yAttr: 'y',
    wktAttr: 'wkt'
  }
};
