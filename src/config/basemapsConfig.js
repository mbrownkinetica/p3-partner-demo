export const basemapConfigs = [
  {
    id: 'dark_gray_canvas',
    title: 'Dark Gray',
    thumbnailUrl: 'http://www.arcgis.com/sharing/rest/content/items/1970c1995b8f44749f4b9b6e81b5ba45/info/thumbnail/ago_downloaded.png',
    url: 'https://services.arcgisonline.com/arcgis/rest/services/Canvas/World_Dark_Gray_Base/MapServer',
    subLayers: []
  },
  {
    id: 'light_gray_canvas',
    title: 'Light Gray',
    thumbnailUrl: 'http://www.arcgis.com/sharing/rest/content/items/8b3b470883a744aeb60e5fff0a319ce7/info/thumbnail/light_gray_canvas.jpg',
    url: 'https://services.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Base/MapServer',
    subLayers: []
  },
  {
    id: 'topographic',
    title: 'Topographic',
    thumbnailUrl: 'https://www.arcgis.com/sharing/rest/content/items/a1dc28de08e6447c8d14085fa15012e1/info/thumbnail/ago_downloaded.png',
    url: 'https://services.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer',
    subLayers: []
  },
  {
    id: 'street',
    title: 'Street',
    thumbnailUrl: 'https://www.arcgis.com/sharing/rest/content/items/3b93337983e9436f8db950e38a8629af/info/thumbnail/ago_downloaded.jpg',
    url: 'https://services.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer',
    subLayers: []
  },
  {
    id: 'imagery',
    title: 'World Imagery',
    thumbnailUrl: 'https://www.arcgis.com/sharing/rest/content/items/10df2279f9684e4a9f6a7f08febac2a9/info/thumbnail/ago_downloaded.jpg',
    url: 'https://services.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer',
    subLayers: []
  },
  {
    id: 'imagery_with_labels',
    title: 'World Imagery with Labels',
    thumbnailUrl: 'https://www.arcgis.com/sharing/rest/content/items/10df2279f9684e4a9f6a7f08febac2a9/info/thumbnail/ago_downloaded.jpg',
    url: 'https://services.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer',
    subLayers: [
      {
        id: 'world_boundaries_and_places',
        url: 'https://services.arcgisonline.com/ArcGIS/rest/services/Reference/World_Boundaries_and_Places/MapServer'
      },
      {
        id: 'world_transportation',
        url: 'https://services.arcgisonline.com/ArcGIS/rest/services/Reference/World_Transportation/MapServer'
      }
    ]
  },
  {
    id: 'terrain_with_labels',
    title: 'Terrain with Labels',
    thumbnailUrl: 'https://www.arcgis.com/sharing/rest/content/items/fe44cf9a739848939988addfeba473e4/info/thumbnail/Terrain_Labels.jpg',
    url: 'https://services.arcgisonline.com/ArcGIS/rest/services/World_Terrain_Base/MapServer',
    subLayers: [
      {
        id: 'world_reference_overlay',
        url: 'https://services.arcgisonline.com/ArcGIS/rest/services/Reference/World_Reference_Overlay/MapServer'
      }
    ]
  }
];
