import BbPromise from 'bluebird';
import Vue from 'vue';
import Vuex from 'vuex';
import * as lodash from 'lodash';
let axios = require('axios');

import config from '@/config/config';
import lsHelper from '@/helpers/localStorageHelper';
import mbHelper from '@/helpers/mapboxHelper';
import mapboxgl from 'mapbox-gl';

// Defer the map creation
let mapPromise = defer();

/**
 * A function to provide deferred-like functionality. Use with caution, can
 * produce anti-patterns and should be used sparingly.
 * @returns {Promise} - A deferred promise object
 */
function defer () {
  var res, rej;
  var promise = new Promise((resolve, reject) => {
    res = resolve;
    rej = reject;
  });
  return {
    resolve: res,
    reject: rej,
    promise: promise
  };
}

// All keys used for storing in local storage
let lsKeys = {
  lsViewerSettingsKey: 'kinetica-wms-viewer-settings',
  lsLayerSettings: 'kinetica-layer-settings'
};

// Normally use statements go into main.js, but this needs to be here
Vue.use(Vuex);

//////////////////////////////
// State
//////////////////////////////

const state = {
  layers: [],
  lsKeys: lsKeys,
  lastContext: null,
  sidebarLeftOpen: false,
  sidebarRightOpen: false,
  tables: [],
  viewerSettings: {apiUrl: '', wmsUrl: ''},
  webmap: mapPromise.promise
};

//////////////////////////////
// Actions
//////////////////////////////

const actions = {
  /**
   * Initializes a webmap and resolves with it. Will mutate
   * the local webmap object in the process.
   */
  initWebmap: (context) => {
    console.debug('Initializing new webmap...');
    return new BbPromise((resolve, reject) => {
      // Set interceptor on Axios to use basic auth if username/password is present.
      axios.interceptors.request.use((config) => {
        let uName = context.getters.viewerSettings.username;
        let pass = context.getters.viewerSettings.password;
        if (uName && pass) {
          config.auth = {username: uName, password: pass};
        }
        return config;
      });

      // Init webmap
      let webmap = mbHelper.initWebmap(
        mapboxgl,
        config.mapDiv,
        config.mapStyle,
        config.apiKey,
        context.getters.viewerSettings.apiUrl,
        context.getters.viewerSettings.username,
        context.getters.viewerSettings.password
      );

      // Wait for map to load styles before resolving
      webmap.on('load', () => {
        context.commit('UPDATE_WEBMAP', webmap);
        mapPromise.resolve(webmap);
        return resolve(webmap);
      });
    });
  }
};

//////////////////////////////
// Mutations
//////////////////////////////

const mutations = {
  UPDATE_TABLES (state, tables) {
    state.tables = tables;
  },
  TOGGLE_SIDEBAR_LEFT_OPEN () {
    state.sidebarLeftOpen = !state.sidebarLeftOpen;
  },
  TOGGLE_SIDEBAR_RIGHT_OPEN () {
    state.sidebarRightOpen = !state.sidebarRightOpen;
  },
  UPDATE_LAST_CONTEXT (state, context) {
    state.lastContext = context;
  },
  UPDATE_LAYERS (state, layers) {
    state.layers = layers;
  },
  UPDATE_SIDEBAR_LEFT_OPEN (state, open) {
    state.sidebarLeftOpen = open;
  },
  UPDATE_SIDEBAR_RIGHT_OPEN (state, open) {
    state.sidebarRightOpen = open;
  },
  UPDATE_TEST_PROMISE (state, value) {
    state.testPromise = value;
  },
  UPDATE_VIEWER_SETTINGS (state, settings) {
    state.viewerSettings = settings;
    lsHelper.setObject(lsKeys.lsViewerSettingsKey, settings); // Persist to local storage
  },
  UPDATE_WEBMAP (state, webmap) {
    state.webmap = webmap;
  }
};

//////////////////////////////
// Getters
//////////////////////////////

const getters = {
  layerById: function (state, layerId) {
    return lodash.find((layer) => { return layer.id === layerId; });
  },
  layers: state => state.layers,
  lastContext: state => state.lastContext,
  lsKeys: state => state.lsKeys,
  sidebarLeftOpen: state => state.sidebarLeftOpen,
  sidebarRightOpen: state => state.sidebarRightOpen,
  testPromise: state => state.testPromise,
  viewerSettings: state => state.viewerSettings,
  tables: state => state.tables,
  webmap: state => state.webmap
};

//////////////////////////////
// Store
//////////////////////////////

export default new Vuex.Store({
  getters,
  state,
  mutations,
  actions
});
