import Vue from 'vue';
import Router from 'vue-router';

import config from '@/config/config';
import KinDashboardScreen from '@/components/screens/KinDashboardScreen';
import KinMapScreen from '@/components/screens/KinMapScreen';
import KinFilterScreen from '@/components/screens/KinFilterScreen';
import KinHeatmapScreen from '@/components/screens/KinHeatmapScreen';
import KinClusterScreen from '@/components/screens/KinClusterScreen';
import KinStatsScreen from '@/components/screens/KinStatsScreen';
import P3DashboardScreen from '@/components/screens/P3DashboardScreen';
import P3SignalDashboardScreen from '@/components/screens/P3SignalDashboardScreen';
import P3SignalDensityDashboardScreen from '@/components/screens/P3SignalDensityDashboardScreen';

Vue.use(Router);

let self = this;

let appRouter = new Router({
  base: '/demo/',
  routes: [
    { path: '*', redirect: '/dashboard/p3-signal-dashboard' }, // Catch-all Redirect
    {
      path: '/dashboard',
      name: 'dashboard',
      component: KinDashboardScreen,
      children: [
        {
          path: 'p3-scoring-dashboard',
          name: 'p3-scoring-dashboard',
          component: P3DashboardScreen
        },
        {
          path: 'p3-signal-dashboard',
          name: 'p3-signal-dashboard',
          component: P3SignalDashboardScreen
        },
        {
          path: 'p3-signal-density-dashboard',
          name: 'p3-signal-density-dashboard',
          component: P3SignalDensityDashboardScreen
        }
      ]
    },
    {
      path: '/map',
      name: 'map',
      component: KinMapScreen,
      children: [
        {
          path: 'raster',
          name: 'raster',
          component: KinHeatmapScreen
        },
        {
          path: 'cluster',
          name: 'cluster',
          component: KinClusterScreen
        },
        {
          path: 'filter',
          name: 'filter',
          component: KinFilterScreen
        },
        {
          path: 'chart-filter',
          name: 'chart-filter',
          component: KinStatsScreen
        }
      ]
    }
  ],
  mode: 'history' // Used to remove hashbang
});

// Setup titles for each route. Defaults to config appTitle.
appRouter.beforeEach((to, from, next) => {
  // Set the document title based on the current route
  document.title = to.meta.title || config.appTitle;

  // Update the last context visited in the store
  self.a.app.$store.commit('UPDATE_LAST_CONTEXT', to.name);

  // Fire callback
  next();
});

//////////////////////////////
// Exported Module
//////////////////////////////

export default appRouter;
