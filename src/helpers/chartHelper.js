import * as d3 from 'd3';

/**
 * Adds a highlight class based on the truthy-ness of the condition passed
 */
function addHighlight(elem, condition) {
  let classes = d3.select(elem).attr('class').split(' ');
  if (!condition || classes.indexOf('highlighted') > -1) {
    return classes.join(' ');
  }

  classes.push('highlighted');
  return classes.join(' ');
}

/**
 * Takes a camel-cased string and returns a title case string with spaces
 * @param {String} string - the camel-cased string
 * @returns {String} - A title-cased string with spaces
 */
function titleCase(string) {
  let outputString = '';
  for (var i = 0; i < string.length; i++) {
    let reggie = new RegExp(/[A-Z]/);
    if (i === 0) {
      outputString += string[i].toUpperCase();
    } else if (string[i].match(reggie)) {
      outputString += ' ' + string[i].toUpperCase();
    } else {
      outputString += string[i];
    }
  }

  return outputString;
}

/**
 * Uses canvas.measureText to compute and return the width of the given text of given font in pixels.
 * @param {String} text The text to be rendered.
 * @param {String} font The css font descriptor that text is to be rendered with (e.g. 'bold 14px verdana').
 * @see https://stackoverflow.com/questions/118241/calculate-text-width-with-javascript/21015393#21015393
 */
function getTextWidth(text, font) {
  // re-use canvas object for better performance
  var canvas = getTextWidth.canvas || (getTextWidth.canvas = document.createElement('canvas'));
  var context = canvas.getContext('2d');
  context.font = font;
  var metrics = context.measureText(text);
  return metrics.width;
}

/**
 * Removes the 'px' at the end of a style string
 * @param {String} str - The style string ending in 'px'
 * @returns {Number} - The parsed number from the input string ending in 'px'
 */
function removePx(str) {
  return parseFloat(str.substr(0, str.length - 2));
}

/**
 * Takes a string and returns a css class-friendly string
 * @param {String}
 */
function getCssClass(str) {
  let reggie = new RegExp(/[a-zA-Z0-9\s]/);
  let className = str.replace(reggie).split(' ').join('-');
  return className;
}

//////////////////////////////
// Exported Module
//////////////////////////////

export default {
  addHighlight,
  getCssClass,
  getTextWidth,
  removePx,
  titleCase
};
