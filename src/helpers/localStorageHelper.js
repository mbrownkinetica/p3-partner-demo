import * as lodash from 'lodash';

//////////////////////////////
// Public Functions
//////////////////////////////

/**
 * Retrieves a value from local storage or falls back to the given value
 * @param {String} key - The local storage key
 * @param {Any} fallback - The fallback value
 */
function get (key, fallback) {
  try {
    let value = window.sessionStorage.getItem(key);
    if (value) {
      return value;
    }
    return lodash.cloneDeep(fallback);
  } catch (err) {
    console.warn('Local storage not found on window...');
  }
}

/**
 * Sets an item in local storage
 * @param {String} key - The storage key
 * @param {String} value - The storage value
 */
function setLs (key, value) {
  try {
    window.sessionStorage.setItem(key, value);
  } catch (err) {
    console.warn('Local storage not found on window...', err);
  }
}

/**
 * Removes a key/value store from local storage
 * @param {String} key - The key of the store to destroy
 */
function remove (key) {
  try {
    window.sessionStorage.removeItem(key);
  } catch (err) {
    console.warn('Either local storage could not be found, or your key did not match any item there.', err);
  }
}

/**
 * Attempts to deserialize an object at the given store key,
 * or returns the fallback value during failure.
 * @param {String} key - The storage key
 * @param {Any} fallback - The fallback value
 */
function getObject (key, fallback) {
  try {
    let serialized = get(key, '');
    return JSON.parse(serialized);
  } catch (err) {
    return lodash.cloneDeep(fallback);
  }
}

/**
 * Returns the property of a stored object
 * @param {String} storeKey - The store key
 * @param {String} objectKey - The object key
 * @param {String} propertyName - The property name
 * @param {Any} fallback - The fallback value
 * @returns {Any} - The value of the stored object property, or the fallback.
 */
function getObjectProperty (storeKey, objKey, propertyName, fallback) {
  try {
    let obj = getObject(storeKey, null);
    return obj[objKey][propertyName] || lodash.cloneDeep(fallback);
  } catch (err) {
    return lodash.cloneDeep(fallback);
  }
}

/**
 * Sets an object property on a stored serialized object and re-saves it.
 * @param {String} storeKey - The store key
 * @param {String} objKey - The object key
 * @param {String} propertyName - The property name
 * @param {Any} value - The value to set the property
 */
function setObjectProperty (storeKey, objKey, propertyName, value) {
  try {
    let obj = getObject(storeKey, null);
    obj[objKey][propertyName] = value;
    setLs(storeKey, window.JSON.stringify(obj));
  } catch (err) {
    console.warn(`Could not set object property in local storage: ${storeKey}->${objKey}->${propertyName}`, err);
  }
}

/**
 * Serializes and sets an object in the local storage.
 * @param {String} key - The key
 * @param {Any} value - The object value to serialize
 */
function setObject (key, value) {
  try {
    setLs(key, window.JSON.stringify(value)); // Babel has a bug. Have to use window. here
  } catch (err) {
    console.warn(`Could not save object into local storage with key: ${key}`, err);
  }
}

//////////////////////////////
// Module Exports
//////////////////////////////

export default {
  remove,
  get,
  getObject,
  getObjectProperty,
  setLs,
  setObject,
  setObjectProperty
};
