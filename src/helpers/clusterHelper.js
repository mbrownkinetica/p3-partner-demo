// External Modules
import * as axios from 'axios';
import * as lodash from 'lodash';
import supercluster from 'supercluster';
import {featureCollection} from '@turf/helpers';
// Helpers
import mbHelper from '@/helpers/mapboxHelper';

//////////////////////////////
// Public Functions
//////////////////////////////

/**
 * Initializes the cluster object
 * @param {Array<Object>} features - An array of GeoJSON Features
 * @param {*} clusterRadius - The cluster radius size
 * @param {*} clusterMaxZoom - The max zoom
 * @param {*} aggProp - The property with which to aggregate (defaults to cluster_qty)
 */
function initCluster (features, clusterRadius, clusterMaxZoom, aggProp) {
  clusterMaxZoom = clusterMaxZoom || 14;
  aggProp = aggProp || 'cluster_qty';

  // Supercluster with property aggregation
  let cluster = supercluster({
    radius: clusterRadius,
    initial: function () {
      return {
        count: 0,
        cluster_qty: 0,
        min: Infinity,
        max: -Infinity
      };
    },
    map: function (properties) {
      return {
        count: 1,
        cluster_qty: Number(properties[aggProp]),
        min: Number(properties[aggProp]),
        max: Number(properties[aggProp])
      };
    },
    reduce: function (accumulated, properties) {
      accumulated.cluster_qty += Math.round(properties.cluster_qty * 100) / 100; // Customized from example
      accumulated.cluster_qty_pretty = accumulated.cluster_qty.toLocaleString(undefined); // Customized from example
      accumulated.count += properties.count; // Came with the example
      accumulated.min = Math.round(Math.min(accumulated.min, properties.min) * 100) / 100; // Came with the example
      accumulated.max = Math.round(Math.max(accumulated.max, properties.max) * 100) / 100; // Came with the example
      accumulated.avg = Math.round(100 * accumulated.cluster_qty / accumulated.count) / 100; // Came with the example
    }
  });
  // USE SUPERCLUSTER TO CLUSTER THE GEOJSON DATA
  cluster.load(features);

  return cluster;
}

/**
 * Returns the cluster source definition object
 * @param {String} sourceId - The source id
 * @param {Object} cluster - The instantiated supercluster object
 * @param {Float} zoom - The current zoom of the map
 * @returns {Object} - The cluster source definition object
 */
function getClusterSourceDef (sourceId, cluster, zoom) {
  let worldBounds = [-180.0000, -90.0000, 180.0000, 90.0000];
  let clusterData = featureCollection(cluster.getClusters(worldBounds, Math.floor(zoom)));
  return {
    type: 'geojson',
    data: clusterData
  };
}

/**
 * Returns a cluster layer definition.
 * @param {String} layerId - The layer id
 * @param {String} sourceId - The source id
 * @returns {Object} - The cluster layer definition
 */
function getClusterLayerDef (layerId, sourceId) {
  return {
    id: layerId,
    type: 'circle',
    source: sourceId
  };
}

/**
 * Returns a layer definition for the cluster labels layer
 * @param {String} layerId - The layer id for the labels layer
 * @param {String} sourceId - The layer's source id
 * @param {String} labelColor - The label color
 * @param {String} haloColor - The label halo color
 * @returns {Object} - The labels layer definition object
 */
function getLabelsLayerDef (layerId, sourceId, labelColor, haloColor) {
  return {
    id: layerId,
    type: 'symbol',
    source: sourceId,
    layout: {
      'text-field': '{cluster_qty_pretty}',
      'text-font': ['Open Sans Semibold', 'Arial Unicode MS Bold'],
      'text-offset': [0.6, 0.0],
      'text-anchor': 'left',
      'text-size': 15
    },
    paint: {
      'text-color': labelColor,
      'text-halo-color': haloColor,
      'text-halo-width': 14
    }
  };
}

/**
 * Returns the paint properties for a cluster based on the current
 * min/max shownand the min/max size of cluster points set in the UI.
 * @param {Object} minMax - The min/max object
 * @param {Integer} minSize - The minimum size of the cluster points
 * @param {Integer} maxSize - The maximum size of the cluster points
 */
function getRadiusPaintProperties (minMax, minSize, maxSize) {
  let paintProps = {
    'type': 'exponential',
    'property': 'cluster_qty',
    'stops': [
      [minMax.min, parseInt(minSize)],
      [minMax.max, parseInt(maxSize)]
    ]
  };

  if (minMax.min === minMax.max) {
    paintProps.stops = [[minMax.max, maxSize]];
  }

  return paintProps;
}

/**
 * Returns the color paint properties for the cluster layer based
 * on the current minmax and colors set in the UI.
 * @param {Object} minMax - The minmax object
 * @param {String} minColor - The min color
 * @param {Strin} maxColor - The max color
 */
function getColorPaintProperties (minMax, minColor, maxColor) {
  return {
    'type': 'exponential',
    'property': 'cluster_qty',
    'stops': [
      [minMax.min, minColor],
      [minMax.max, maxColor]
    ]
  };
}

/**
 * Returns the min and max values for all cluster features based
 * on their sum property. Useful for finding the smallest
 * and largest clusters for automagic theming of cluster points.
 * @param {Object} clusterData - The cluster data object
 */
function getMinMax (map, cluster, aggProp) {
  let bounds = map.getBounds();
  let boundsArr = [bounds._sw.lng, bounds._sw.lat, bounds._ne.lng, bounds._ne.lat];
  let features = cluster.getClusters(boundsArr, Math.floor(map.getZoom()));

  let min = lodash.minBy(features, (feature) => {
    return feature.properties[aggProp];
  });

  let max = lodash.maxBy(features, (feature) => {
    return feature.properties[aggProp];
  });

  if (min && max) {
    return {min: min.properties[aggProp], max: max.properties[aggProp]};
  }

  return {min: 0, max: 100};
}

/**
 * Queries features from the specified URL
 * @param {String} url - The URL
 * @param {Number} precision - The precision with which to query
 * @param {String} xCol - The longitude column name
 * @param {String} yCol - The latitude column name
 * @returns {Promise} - The promise of the response from the API
 */
function queryFeatures (url, precision, xCol, yCol, useBbox) {
  let map = this.$store.getters.webmap;

  // Ensure a table has been selected before we query
  if (!this.selectedTable) {
    return;
  }

  var postOptions = {
    'table_name': this.selectedTable,
    'column_names': [
      `SUBSTRING(geohash, 1, ${precision}) as geohash_prefix`,
      'COUNT(*) as cluster_qty',
      `AVG(${xCol}) as ${xCol}`,
      `AVG(${yCol}) as ${yCol}`
    ],
    'limit': -9999,
    'offset': 0,
    'encoding': 'json',
    'options': {
      'sort_order': 'descending',
      'sort_by': 'value'
    }
  };

  if (useBbox) {
    postOptions.options.expression = `STXY_INTERSECTS(${xCol}, ${yCol}, ${mbHelper.getBoundsWkt(map.getBounds())})`;
  }

  return axios.post(url, postOptions)
    .then((response) => {
      return response.data;
    })
    .catch((err) => {
      console.error(err);
    });
}

//////////////////////////////
// Module Exports
//////////////////////////////

export default {
  getClusterSourceDef,
  getClusterLayerDef,
  getColorPaintProperties,
  getLabelsLayerDef,
  getRadiusPaintProperties,
  getMinMax,
  initCluster,
  queryFeatures
};
