import * as lodash from 'lodash';
import {toMercator} from '@turf/projection';
import {feature, point} from '@turf/helpers';
import config from '@/config/config';

import kineticaHelper from '@/helpers/kineticaHelper';

//////////////////////////////
// Public Functions
//////////////////////////////

/**
 * Initializes a mapbox webmap given the mapboxgl object and a valid api key.
 * @param {Object} mapboxgl - The MapBoxGL object (typically from window)
 * @param {String} mapDiv - The ID of the map div
 * @param {String} mapStyle - The map style.
 * @param {String} apiKey - The Mapbox API Key
 */
function initWebmap (mapboxgl, mapDiv, mapStyle, apiKey, apiUrl, username, password) {
  mapboxgl.accessToken = apiKey;
  let mapParams = {
    container: mapDiv,
    style: mapStyle,
    zoom: 4,
    center: [-3.8199625, 40.4378693],
    maxZoom: 14,
    minZoom: 3
  };

  if (apiUrl && username && password) {
    mapParams.transformRequest = (url, resourceType) => {
      let encodedCreds = btoa(`${username}:${password}`);
      if (resourceType === 'Image' && url.startsWith(apiUrl)) {
        return {
          url: url,
          headers: {'Authorization': `Basic ${encodedCreds}`}
        };
      }
    };
  }

  return new mapboxgl.Map(mapParams);
}

/**
 * Transforms a Kinetica dynamic schema response's data_string into an array of GeoJSON features.
 * @param {String} dataStr - The data string from the Kinetica query response.
 * @param {*} lonCol - The longitude column name
 * @param {*} latCol - The latitude colum name
 * @returns {Array<Object>} - The array of GeoJSON features.
 */
function dynamicSchemaToGeoJson (dataStr, lonCol, latCol) {
  var data = kineticaHelper.parseData(dataStr);
  var features = [];

  // For each feature
  for (var i = 0; i < data.column_1.length; i++) {
    var feature = {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: []
      },
      properties: {}
    };

    // Set the properties
    for (var col = 0; col < data.column_headers.length; col++) {
      var columnHeader = data.column_headers[col];
      var columnData = data[`column_${col + 1}`][i];
      feature.properties[columnHeader] = columnData;
    }

    if (lodash.get(feature, 'properties.cluster_qty', null)) {
      feature.properties.cluster_qty_pretty = feature.properties.cluster_qty.toLocaleString();
    }

    // Set the geometry
    feature.geometry.coordinates = [feature.properties[lonCol], feature.properties[latCol]];
    feature.properties.sqrt = Math.sqrt(feature.properties.cluster_qty);

    features.push(feature);
  }

  return features;
}

/**
 * Returns a bounds WKT for the passed mapbox getBounds() result.
 * @param {Object} bounds - A MapBox getBounds() result
 * @returns {String} - A boundary WKT to be used in Kinetica.
 */
function getBoundsWkt (bounds) {
  let boundaryWkt = 'POLYGON((';
  boundaryWkt += `${bounds._sw.lng} ${bounds._ne.lat},`; // A,D 4
  boundaryWkt += `${bounds._ne.lng} ${bounds._ne.lat},`; // C,D 3
  boundaryWkt += `${bounds._ne.lng} ${bounds._sw.lat},`; // C,B 2
  boundaryWkt += `${bounds._sw.lng} ${bounds._sw.lat},`; // A,B 1
  boundaryWkt += `${bounds._sw.lng} ${bounds._ne.lat}`; // A,D 4
  boundaryWkt += '))';
  return `St_GeomFromText('${boundaryWkt}')`;
}

function boundsToFeature (bounds) {
  // Get coords and make a valid coordinate array for a polygon
  let coords = getCoordsFromBounds(bounds);
  coords.push(lodash.cloneDeep(coords[0]));

  // Transform it to a feature
  var geometry = {
    'type': 'Polygon',
    'coordinates': coords
  };

  return feature(geometry);
}

/**
 * Returns a coordinate array given a MapBox lat/long bounds object
 * @param {Object} bounds - The MapBox lat/long bounds object
 * @returns {Array<Array<Number>>} - The coordinate array
 */
function getCoordsFromBounds (bounds) {
  var coords = [];
  coords.push([bounds._sw.lng, bounds._ne.lat]); // A,D 4
  coords.push([bounds._ne.lng, bounds._ne.lat]); // C,D 3
  coords.push([bounds._ne.lng, bounds._sw.lat]); // C,B 2
  coords.push([bounds._sw.lng, bounds._sw.lat]); // A,B 1
  return coords;
}

/**
 * Returns the current bounding box of the map,
 * formatted to 3857 and ready to be sent to Kinetica.
 * @param {Object} map - The map
 * @returns {String} - A bounding box string for a wms bbox param
 */
function getCurrentBbox (map) {
  var bounds = map.getBounds();
  let projMin = toMercator(point([bounds._sw.lng, bounds._sw.lat]));
  let projMax = toMercator(point([bounds._ne.lng, bounds._ne.lat]));
  let bbox = '';
  bbox += projMin.geometry.coordinates[0] + ',';
  bbox += projMin.geometry.coordinates[1] + ',';
  bbox += projMax.geometry.coordinates[0] + ',';
  bbox += projMax.geometry.coordinates[1];
  return bbox;
}

function getCurrentBboxArray(map) {
  let retVal = [];
  let bounds = map.getBounds();
  retVal.push(bounds._sw.lng);
  retVal.push(bounds._sw.lat);
  retVal.push(bounds._ne.lng);
  retVal.push(bounds._ne.lat);
  return retVal;
}

/**
 * Gracefully adds a source object to a map
 * @param {Object} map - The MapBox map object
 * @param {String} sourceId - The source id
 * @param {Object} sourceDef - The source definition
 * @returns {Object} - The source object that was added
 */
function addSourceDeprecated (map, sourceId, sourceDef) {
  let source = map.getSource(sourceId);
  if (source) {
    map.removeSource(sourceId);
  }
  map.addSource(sourceId, sourceDef);
  return map.getSource(sourceId);
}

/**
 * Gracefully adds a source object to a map
 * @param {Object} map - The MapBox map object
 * @param {String} sourceId - The source id
 * @param {Object} sourceDef - The source definition
 * @returns {Object} - The source object that was added
 */
function addSource (map, sourceDef) {
  let source = map.getSource(sourceDef.id);
  if (source) {
    map.removeSource(sourceDef.id);
  }
  map.addSource(sourceDef.id, sourceDef);
  return map.getSource(sourceDef.id);
}

/**
 * Gracefully adds a layer object to a map
 * @param {Object} map - The MapBox map object
 * @param {String} layerId - The source id
 * @param {Object} layerDef - The layer definition
 * @returns {Object} - The layer object that was added
 */
function addLayerDeprecated (map, layerId, layerDef) {
  let layer = map.getLayer(layerId);
  if (layer) {
    map.removeLayer(layerId);
  }
  map.addLayer(layerDef);
  return map.getLayer(layerId);
}

/**
 * Gracefully adds a layer object to a map
 * @param {Object} map - The MapBox map object
 * @param {String} layerId - The source id
 * @param {Object} layerDef - The layer definition
 * @returns {Object} - The layer object that was added
 */
function addLayer (map, layerDef) {
  let layer = map.getLayer(layerDef.id);
  if (layer) {
    map.removeLayer(layerDef.id);
  }
  map.addLayer(layerDef);
  return map.getLayer(layerDef.id);
}

/**
 * Removes a source gracefully from a mapbox map
 * @param {Object} map - The map object
 * @param {Strinb} sourceId - The source id
 */
function removeSource (map, sourceId) {
  let layer = map.getSource(sourceId);
  if (layer) {
    map.removeSource(sourceId);
  }
}

/**
 * Removes a layer gracefully from a map object
 * @param {Object} map - The map object
 * @param {String} layerId - The layer id to remove
 */
function removeLayer (map, layerId) {
  let layer = map.getLayer(layerId);
  if (layer) {
    map.removeLayer(layerId);
  }
}

/**
 * Returns div dimensions for the passed id
 * @returns {Object} - A dimensions object with height and width properties.
 */
function getDivDimensions (divId) {
  let div = document.getElementById(divId);
  return {height: div.clientHeight, width: div.clientWidth};
}

/**
 * Returns a URL givena base url and a params object
 * @param {String} baseUrl - The base URL
 * @param {*} params - The params object
 * @returns {String} - The final url with params concatenated
 */
function buildUrl (baseUrl, params) {
  return baseUrl + '?' + joinParams(params);
}

/**
 * Sets the soruce params on a heatmap source
 * @param {Object} map - The mapbox map
 * @param {String} wmsUrl - The WMS Url
 * @param {String} sourceId - The source ID for the heatmap
 * @param {Object} params - The url params for the wms heatmap view
 */
function setSourceParams (map, wmsUrl, sourceId, layerId, params) {
  let dimensions = getDivDimensions(config.mapDiv);

  var coords = getCoordsFromBounds(map.getBounds());
  params.bbox = getCurrentBbox(map);
  params.srs = 'EPSG:3857';
  params.height = dimensions.height;
  params.width = dimensions.width;

  let source = map.getSource(sourceId);

  lodash.set(source, 'url', buildUrl(wmsUrl, params));
  lodash.set(source, 'options.url', buildUrl(wmsUrl, params));
  lodash.set(source, 'coordinates', coords);

  if (source.load) {
    source.load();
  }
}

//////////////////////////////
// Private Functions
//////////////////////////////

/**
 * Joins object properties as URL parameters
 * @param {Object} params - The object representing the params
 * @returns {String} - A stringified version of the URL parameters
 */
function joinParams (params) {
  var retVal = '';
  for (var key in params) {
    retVal += `&${key}=${params[key]}`;
  }
  return retVal;
}

//////////////////////////////
// Module Exports
//////////////////////////////

export default {
  addLayer,
  addLayerDeprecated,
  addSource,
  addSourceDeprecated,
  boundsToFeature,
  buildUrl,
  dynamicSchemaToGeoJson,
  getCoordsFromBounds,
  getCurrentBbox,
  getCurrentBboxArray,
  getDivDimensions,
  getBoundsWkt,
  initWebmap,
  removeLayer,
  removeSource,
  setSourceParams
};
