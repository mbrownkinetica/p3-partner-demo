let axios = require('axios');
let lodash = require('lodash');
let BbPromise = require('bluebird');

//////////////////////////////
// Private Functions
//////////////////////////////

/**
 * Builds a payload based on the passed parameters and options
 * @param {Object} params - A params object
 * @param {Object} options - An options object
 */
function _buildPayload (params, options) {
  let payload = {options: options};
  lodash.assignIn(payload, params);
  return payload;
}

/**
 * Delays further execution until the sleep timer has expired
 * @param {*} time The time to sleep in milliseconds
 */
async function _sleep (time) {
  console.debug(`Sleeping for ${time}...`);
  return new BbPromise((resolve) => setTimeout(resolve, time));
}

/**
 * Checks the overall status of a particular proc ID at the specified interval.
 * @param {String} url - The url of Kinetica including the port
 * @param {String} procId - The proc id to babysit
 * @param {Number} attempts - The number of attempts
 * @param {Number} maxFailAttempts - The number of times a proc status can return a running status. Defaults to 100.
 * @returns {Promise} - The promise of the proc result at either 'running', 'complete', or 'error'
 */
async function _awaitProcResult(url, procId, attempts, waitTime, maxFailAttempts) {
  maxFailAttempts = maxFailAttempts || 100;
  attempts = attempts || 0;
  waitTime = waitTime || 100;

  // Fetch the proc status
  let procStatus;
  try {
    console.debug(`Checking status of proc with id: ${procId}`);
    let procStatusResult = await axios.post(`${url}/show/proc/status`,
      {'run_id': procId.toString(), 'options': {'clear_complete': 'true'}});
    let parsed = parseDataStr(procStatusResult.data, true);
    procStatus = parsed.overall_statuses[procId];
  } catch (err) { console.error(err); return 'error'; }

  // If the result is complete, return so
  let completeStatuses = ['error', 'complete', 'killed'];
  if (lodash.includes(completeStatuses, procStatus)) {
    console.debug('Proc completed', {procStatus});
    return procStatus;
  // Else if failed, return a failed status
  } else if (attempts >= maxFailAttempts) {
    return 'failed';
  }

  // Otherwise, re-fire this function
  console.debug(`Proc still running... Checking again. Attempt: ${attempts} of ${maxFailAttempts}`);
  attempts += 1;
  _sleep(waitTime);
  return _awaitProcResult(url, procId, attempts, waitTime, maxFailAttempts);
}

/**
 * Executes the proc and returns its proc ID.
 * @param {String} baseUrl - The url of Kinetica including the port number.
 * @param {String} procName - The name of the proc
 * @param {Array<String>} inputTableNames - An array of table names to use as input
 * @param {Array<String>} outputTableNames - An array of table names to use as output
 * @param {Object} params - Params to use in the proc
 *  Should not include the /execute/proc endpoint.
 */
async function _startProc(baseUrl, procName, inputTableNames, outputTableNames, params) {
  params = params || {};
  let procResult, procId;
  let procOptions = {
    'proc_name': procName,
    'params': params,
    'bin_params': {},
    'input_table_names': inputTableNames,
    'input_column_names': {},
    'output_table_names': outputTableNames,
    'options': {}
  };
  try {
    procResult = await axios.post(`${baseUrl}/execute/proc`, procOptions);
    procId = parseDataStr(procResult.data, true).run_id;
  } catch (err) { console.error(err); return; }
  console.debug(`Executed proc with ID: ${procId}`);
  return procId;
}

/**
 * Returns the output of a proc
 * @param {String} url - The Kinetica base URL
 * @param {String} tableName - The name of the table
 * @returns {Promise} - The promise of the returned results from the get all query.
 */
async function _getAll(url, tableName) {
  let getParams = {
    'table_name': tableName,
    'offset': 0,
    'limit': -9999,
    'encoding': 'json'
  };
  let getOptions = {};
  let outputResults = await get(url, 'records', getParams, getOptions);
  return parseDataStr(outputResults, true);
}

//////////////////////////////
// Public Functions
//////////////////////////////

/**
 * Deletes (DROPS) a collection from Kinetica
 * @param {String} url - The Kinetica base url
 * @param {String} name - The name of the collection to remove
 * @returns {Promise} - The promise of the collection being deleted
 */
async function deleteCollection(url, name) {
  let params = {
    'table_name': name,
    'authorization': '',
    'options': {
      'no_error_if_not_exists': 'true'
    }
  };
  let result = await axios.post(`${url}/clear/table`, params);
  return result.data;
}

/**
 * Retrieves records from the Kinetica database
 */
async function get(url, method, params, options) {
  let requestUrl = url + '/get/' + method;
  let payload = _buildPayload(params, options);
  console.debug('Querying database at ' + requestUrl + '...', payload);

  let response = await axios.post(requestUrl, payload);
  try {
    return window.JSON.parse(response.data.data_str);
  } catch (err) {
    console.warn('Could not find data in the query reponse', err);
    return BbPromise.reject(err);
  }
}

/**
 * Returns a first-order list of tables available in the
 * database at the provided apiUrl
 * @param {String} apiUrl - The api URL
 */
async function getTables (apiUrl) {
  let postOptions = {
    'table_name': '',
    'options': {}
  };
  let response = await axios.post(`${apiUrl}/show/table`, postOptions);

  try {
    let tableInfo = window.JSON.parse(response.data.data_str);
    return tableInfo.table_names;
  } catch (err) {
    console.error(err);
    return BbPromise.reject(err);
  }
}

/**
 * Gets a list of tables/collections and all contained tables.
 * @param {String} apiUrl - The api URL you wish to query
 * @returns {Array} - An array of table names.
 */
async function getTablesRecursive(apiUrl) {
  // TODO: Return something like this
  // [{tableName: 'my table name', children: []}]
}

/**
 * Parses the Kinetica data_str and returns a JSON object.
 * @param {String} dataStr - The data_str from the Kinetica response.
 * @param {Boolean} raw - Whether to return raw results, or the json_encoded_response.
 *                        Defaults to JSON encoded response.
 * @returns {Object} - The unserialized object or an error
 */
function parseData (dataStr, raw) {
  var data;
  try {
    let jsonData = JSON.parse(dataStr);
    if (raw) {
      data = jsonData;
    } else {
      data = JSON.parse(jsonData.json_encoded_response);
    }
  } catch (err) {
    console.error(err);
  }

  return data;
}

/**
 * Takes a parsed data object and returns a parsed
 * array of records.
 * @param {data} - The parsed data_str object with a records_json property on it.
 * @returns {Array} - The parased array of records as native objects.
 */
function parseJsonRecords(data) {
  let records = [];
  for (var i = 0; i < data.records_json.length; i++) {
    try {
      let record = JSON.parse(data.records_json[i]);
      records.push(record);
    } catch (err) { console.error(err); return; }
  }
  return records;
}

function parseJsonEncodedResponseWithSchema(data) {
  let parsed;
  try {
    parsed = JSON.parse(data.json_encoded_response);
  } catch (err) { console.error(err); return; }

  let results = [];
  for (var i = 0; i < parsed.column_1.length; i++) {
    let scaffold = {};
    for (var h = 0; h < parsed.column_headers.length; h++) {
      scaffold[parsed.column_headers[h]] = parsed[`column_${h + 1}`][i];
    }
    results.push(scaffold);
  }

  return results;
}

/**
 * Posts data to the passed url with the passed options
 * @param {String} api - The URL of the API to call
 * @param {Object} params - The post params
 * @returns {Promise} - The promise of the completed post
 */
async function post(url, api, params) {
  let results;
  try {
    results = await axios.post(`${url}${api}`, params);
  } catch (err) { console.error(err); return; }

  return results;
}

/**
 * Convenience function that automatically targets the data_str property
 * of the response object from Kinetica.
 * @param {Object} data - The reponse object from a Kinetica call
 */
function parseDataStr(data, raw) {
  if (!data.data_str) {
    console.error('No data_str property provided on the data.');
    return;
  }
  return parseData(data.data_str, raw);
}

/**
 * Executes a proc, inspects its status, then returns the results from the proc.
 * @param {String} url - The Kinetica base URL
 * @param {String} procName - The name of a proc to run
 * @param {String} inputTableNames - The names of the tables to use as input
 * @param {String} outputTableNames - The names of the tables to use as output
 * @param {Number} waitTime - The time to wait between executing the proc and checking its status. Prevents
 * spamming the server when the average query time is already known.
 * @returns {Array} - An array of results from the output table.
 */
async function execProcAndGet(url, procName, inputTableNames, outputTableNames, waitTime) {
  console.group('execProcAndGet');
  // Execute the UDF
  await execProc(url, procName, inputTableNames, outputTableNames, waitTime);

  // If proc has completed successfully, fetch the results from the output table
  let results = _getAll(url, outputTableNames);
  console.groupEnd();
  return results;
}

/**
 * Executes a proc, inspects its status, then returns the status of the proc.
 * @param {String} url - The Kinetica base URL
 * @param {String} procName - The name of a proc to run
 * @param {String} inputTableNames - The names of the tables to use as input
 * @param {String} outputTableNames - The names of the tables to use as output
 * @param {Object} params - An object with parameters to be sent to the proc
 * @param {Number} waitTime - The time to wait between executing the proc and checking its status. Prevents
 * spamming the server when the average query time is already known.
 * @returns {Array} - An array of results from the output table.
 */
async function execProc(url, procName, inputTableNames, outputTableNames, params, waitTime) {
  console.debug(`Starting proc ${procName}`, {url, procName, inputTableNames, outputTableNames, params});
  // Execute the proc and get the id
  let procId = await _startProc(url, procName, inputTableNames, outputTableNames, params);
  let result = {id: procId};

  // Delay the checking of the proc if it takes a while to run.
  // Defaults to 0 if no wait time is passed.
  waitTime = waitTime || 0;
  await _sleep(waitTime);

  // Wait for the proc to finish or error out
  let finalStatus = await _awaitProcResult(url, procId);
  result.status = finalStatus;

  // If errored-out, issue a warning and return
  let errors = ['error', 'killed', 'failed'];
  if (lodash.includes(errors, finalStatus)) {
    console.warn(`Proc did not finish properly. Finished with a final status of: ${finalStatus}`);
    return result;
  }

  // await _sleep(500); // TODO: Wait for the table to populate?

  // Return the status of the proc
  return result;
}

/**
 * Deletes (NOT DROPS) all records from the given table name
 * @param {String} url - The Kinetica base url including the port number
 * @param {String} name - The name of the table from which to remove all records
 * @returns {Promise} - The promise of all records being deleted from the passed table.
 */
async function deleteRecords(url, name) {
  let deleteResult;
  try {
    console.debug(`Deleting all records from ${name}...`);
    let options = {
      'table_name': name,
      'expressions': ['1=1'],
      'options': {}
    };
    deleteResult = await axios.post(`${url}/delete/records`, options);
  } catch (err) { console.error(err); return 'error'; }

  return deleteResult;
}

/**
 * Runs a group by in Kinetica and retrieves the results
 * @param {String} url - The kinetica base url
 * @param {String} table - The table name
 * @param {Array<String>} - The group and aggregate columns to retrieve
 * @param {Number} offset - The offset
 * @param {Number} limit - The limit
 * @param {Object} options - The options to run with
 * @returns {Promise} - The promise of the group by results
 */
async function groupBy(url, table, columns, offset, limit, options) {
  options = options || {'sort_order': 'descending'};
  let params = {
    'table_name': table,
    'column_names': columns,
    'offset': offset,
    'limit': limit,
    'encoding': 'json',
    'options': options
  };
  let results;
  try {
    results = await axios.post(`${url}/aggregate/groupby`, params);
  } catch (err) { console.error(err); return; }
  let parsed = JSON.parse(results.data.data_str);
  let retVal = parseJsonEncodedResponseWithSchema(parsed);
  return retVal;
}

/**
 * Builds a lookup map from the array passed based on the key property name
 * @param {Array<Object>} arr - The array of objects to map
 * @param {String} key - The key property to use in the map
 * @param {String} val - The value property to use in the map
 * @returns {Object} - The resulting map
 */
function buildMap(arr, key, val) {
  let retVal = {};
  lodash.forEach(arr, (obj) => {
    let keyVal = obj[key];
    let valVal = obj[val];
    retVal[keyVal] = valVal;
  });
  return retVal;
}

/**
 * Builds a select list from a given array. Returns a list of objects with text and value properties.
 * @param {Array<Object>} arr - The array of objects
 * @param {String} key - The key to be used as the value parameter
 * @param {String} val - The key to be used as the text parameter
 * @returns {Array<Object>} - An array of objects with text and value properties.
 */
function buildSelectList(arr, key, val, defaultMsg) {
  let retVal = [];
  if (defaultMsg) {
    retVal.push({value: '', text: defaultMsg});
  }
  lodash.forEach(arr, (obj) => {
    retVal.push({value: obj[key], text: obj[val]});
  });
  return retVal;
}

//////////////////////////////
// Exported Module
//////////////////////////////

export default {
  buildMap,
  buildSelectList,
  deleteCollection,
  deleteRecords,
  execProc,
  execProcAndGet,
  get,
  getTables,
  getTablesRecursive,
  groupBy,
  parseData,
  parseDataStr,
  parseJsonRecords,
  parseJsonEncodedResponseWithSchema,
  post,
  _sleep
};
