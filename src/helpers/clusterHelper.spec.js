/* global describe it beforeEach */
let should = require('chai').should(); // eslint-disable-line no-unused-vars

let subject;

describe('clusterHelper', () => {
  beforeEach(() => {
    subject = require('./clusterHelper');
  });

  describe('#getClusterLayerDef', () => {
    it('should return a valid object', () => {
      subject.getClusterLayerDef('mikey', 'mouth')
        .should.be.a('object');
    });
    it('should set the layer id', () => {
      subject.getClusterLayerDef('sloth', 'fratellis')
        .should.have.a.property('id')
        .and.equal('sloth');
    });
    it('should set the source id', () => {
    });
  });
});
