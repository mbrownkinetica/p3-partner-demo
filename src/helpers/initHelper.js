/**
 * A module that helps the application
 * initialize itself into the required state
 */

// Helpers
import lsHelper from '@/helpers/localStorageHelper';
import store from '@/store/store';

// Modules
import * as lodash from 'lodash';

// Config
let config = require('@/config/config');

//////////////////////////////
// Data Structures
//////////////////////////////

// The local storage keys
let lsKeys = {
  viewerSettings: 'kinetica-wms-viewer-settings',
  layerSettings: 'kinetica-layer-settings'
};

let defaults = {
  color: '#FF0000',
  pointShapes: 'circle',
  renderPoints: true,
  renderShapes: true,
  renderSymbology: true,
  visible: false,
  wkt: 'wkt',
  xAttr: 'x',
  yAttr: 'y'
};

//////////////////////////////
// Methods
//////////////////////////////

/**
 * Retrieves the viewer settings from local storage
 */
function initViewerSettings () {
  let settings = lsHelper.getObject(lsKeys.viewerSettings, {});
  store.commit('UPDATE_VIEWER_SETTINGS', settings);
}

/**
 * Initializes layer properties
 * @param {Map} webmap - The webmap instance
 * @param {String} layerId - The layer id
 * @param {Layer} layer - The layer
 */
function _initLayer (webmap, layerId, layer) {
  let settings = lsHelper.getObject(lsKeys.layerSettings, {});
  let layerSettings = settings[layerId];

  // Set Visibility
  let visible = lodash.get(layerSettings, 'visible', false);
  if (visible) {
    layer.show();
  }

  // Set shape drawing
  let doShapes = lodash.get(layerSettings, 'renderShapes', config.layerDefaults.renderShapes);
  if (doShapes) {
    lodash.set(layer, 'customLayerParameters.DOSHAPES', defaults.renderShapes);
  }

  // Set the x attribute
  let xAttr = lodash.get(layerSettings, 'xAttr', config.layerDefaults.xAttr);
  if (xAttr) {
    lodash.set(layer, 'customLayerParameters.X_ATTR', xAttr);
  }

  // Set the y attribute
  let yAttr = lodash.get(layerSettings, 'yAttr', config.layerDefaults.yAttr);
  if (xAttr) {
    lodash.set(layer, 'customLayerParameters.Y_ATTR', yAttr);
  }

  // Set the wkt attribute
  let wktAttr = lodash.get(layerSettings, 'wktAttr', config.layerDefaults.wktAttr);
  if (wktAttr) {
    lodash.set(layer, 'wktAttr', wktAttr);
  }

  // Set the wkt attribute
  let pointColors = lodash.get(layerSettings, 'pointColors.hex', config.layerDefaults.color);
  if (pointColors) {
    let strippedHash = pointColors.substr(1, pointColors.length);
    lodash.set(layer, 'customLayerParameters.POINTCOLORS', strippedHash);
  }

  // Set the pointSizes attribute
  let pointSizes = lodash.get(layerSettings, 'pointSizes', config.layerDefaults.pointSizes);
  if (pointSizes) {
    lodash.set(layer, 'customLayerParameters.POINTSIZES', pointSizes);
  }

  // Set the pointShapes attribute
  let pointShapes = lodash.get(layerSettings, 'pointShapes', config.layerDefaults.pointShapes);
  if (pointShapes) {
    lodash.set(layer, 'customLayerParameters.POINTSHAPES', pointShapes);
  }

  // Finally, refresh the layer to cement these settings
  layer.refresh();

  return layer;
}

export default {
  initViewerSettings,
  _initLayer
};
